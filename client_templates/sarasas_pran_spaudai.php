<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_in_create.php';?>

<div class="page news_page">
	<?php include '../partials/global_warning.php';?>
	<section class="filter">
		<label class="simple_checkbox auto_checkbox">
			<input type="checkbox" name="auto" checked>
			<span class="name">
				<span>Automatinis naujienų atsinaujinimas</span>
			</span>
		</label>
		<?php include '../partials/search_client_press_releases.php';?>
		<div class="button underlined detail_search">Detali paieška</div>
		<div class="opened_filter_buttons">
			<div class="button underlined close_search">Suskleisti paiešką</div>
			<div class="clear"></div>
			<div class="button underlined clear_filters">Išvalyti filtrą</div>
		</div>
		<div class="hidden_calendar">
			<div class="simple_input inline icon_dates">
				<input class="datepicker" type="text" name="calendar_date">
			</div>
			<div class="day_button today active">Šiandien</div>
			<div class="day_button tomorrow">Ryt</div>
		</div>
		<div class="right_box">
			<div class="label">Atvaizdavimas</div>
			<div class="view current" data-view="columns"></div>
			<div class="view" data-view="info"></div>
		</div>
	</section>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_client_press_release.php';?>
			</div>
		</div>
		<div class="right layout info columns">
			<div class="photos_holder">
				<div class="scroller_holder">
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.

								<!-- Sitas daiktas reiskia, kad yra nuotrauku, ir kiek -->
								<span class="has_photos">(4)</span>
								<!-- ... -->
								<!-- Sitas daiktas reiskia, kad yra failu prikelta -->
								<span class="has_downloadable"></span>
								<!-- ... -->
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_downloadable"></span>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="show_more_button">Rodyti daugiau</div>
				</div>
			</div>
		</div>
		<div class="side_pop">
			<div class="news_high scroller_holder">
				<div class="breadcrumbs">
					<a href="#">Naujienos</a>
					<span>&nbsp;/&nbsp;</span>
					<a href="#">Sportas</a>
					<span>&nbsp;/&nbsp;</span>
					<a href="#">Lietuvos naujienos</a>
					<span>&nbsp;/&nbsp;</span>
					<a href="#">Ledo ritulys</a>
				</div>
				<a href="#" class="edit top_button">Redaguoti</a>
				<div class="copy_text" data-target="for_copy">Kopijuoti tekstą</div>
				<div class="extra_info_block">
					Valstybinė mokesčių inspekcija <div class="info extra_tooltip">
						<div class="tooltip_holder">
							<div class="tooltip bigger_tooltip">
								<p>Vardenis Pavardenis</p>
								<p>Pareigos</p>
								<a href="mailto:vardenis.pavardenis@adresas.lt" class="email" target="_top">vardenis.pavardenis@adresas.lt</a>
								<p>+370 000 000 00</p>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="copy_holder" id="for_copy">
					<h2>Lietuvos ledo ritulininkės iš Latvijos parsivežė unikalią patirtį</h2>
					<div class="time_stamp">
						<div class="date">2017-08-07</div>
						<div class="time">11:41</div>
					</div>
					<div class="heading">Ryga, kovo 12 d. (ELTA)</div>
					<div class="simple_text">
						Savaitgalį dienomis Tukumse vyko trečiasis Latvijos moterų ledo ritulio čempionato turas. Paskutiniąsias ketverias čempionato rungtynes jame sužaidė ir lietuvių „Hockey Girls“ ekipa<br/><br/>
						Trečiojoje išvykoje lietuvėms teko pripažinti labiau patyrusių ir ne pirmus metus besitreniruojančių kaimyninės šalies merginų pranašumą. Abejos rungtynės su „L&L/JLSS“ ekipą baigėsi rezultatais 0:5 ir 0:6, o kitos dienos rungtynės su „Laima Juniors“ baigėsi pralaimėjimais 0:3 bei 0:7.<br/><br/>
						Pirmą kartą istorijoje atvirame Latvijos moterų ledo ritulio čempionate dalyvaujanti „Hockey Girls“ ekipa iš viso sužaidė 12 rungtynių tarp keturių dalyvaujančių komandų užėmė paskutinę vietą.
					</div>
					<div class="author">Dominykas Genevičius (ELTA)</div>
					<div class="event_info">
						<div class="event_info_name">Renginio pradžia:</div>
						<div class="event_info_text">2018-06-14 (ketvirtadienis) 12:30</div>
					</div>
					<div class="event_info">
						<div class="event_info_name">Renginio pabaiga:</div>
						<div class="event_info_text">2018-06-14 (ketvirtadienis) 12:30</div>
					</div>
					<div class="event_info">
						<div class="event_info_name">Renginio vieta:</div>
						<div class="event_info_text">Nacionalinė dailės galerija</div>
					</div>
				</div>
				<div class="iframe_holder">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/AOoT2xOSoEs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="files_area">
					<div class="file image">
						<a href="#" class="edit">Atsisiųsti</a>
						<div class="img_holder">
							<img src="../media/images/hockey.jpg" alt="">
						</div>
					</div>
					<div class="file image">
						<a href="#" class="edit">Atsisiųsti</a>
						<div class="img_holder">
							<img src="../media/images/hockey.jpg" alt="">
						</div>
					</div>
					<div class="file">
						<a href="#" class="edit">Atsisiųsti</a>
						<div class="file_holder">example.xlsx</div>
					</div>
				</div>
				<div class="warning">Dėmesio! Už šią informaciją ELTA neatsako. Už tai atsako ją publikavęs klientas.</div>
				<div class="button fb"><span>Dalintis</span></div>
			</div>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>