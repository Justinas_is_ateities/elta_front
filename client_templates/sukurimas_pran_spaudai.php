<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_in.php';?>

<div class="page press_release_page sidebar_layout no_filter no_sidebar">
	<?php include '../partials/global_warning.php';?>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_client_press_release.php';?>
			</div>
		</div>
		<div class="right layout">
			<form id="press_release_form">
				<a href="#" class="go_back_to_list">atgal į sąrašą</a>
				<div class="simple_input">
					<div class="label">Pavadinimas</div>
					<input type="text" spellcheck="true" name="name">
				</div>
				<div class="line"></div>
				<div class="row full">
					<div class="label">Tipas</div>
					<label class="simple_radio">
						<input type="radio" data-type="press" name="type" checked>
						<span class="name">
							<span>Pranešimas spaudai</span>
						</span>
					</label>
					<label class="simple_radio">
						<input type="radio" data-type="calendar" name="type">
						<span class="name">
							<span>Kalendorius (renginys)</span>
						</span>
					</label>
				</div>
				<div class="calendar_hideable">
					<div class="row">
						<div class="col">
							<div class="label">Renginio pradžia</div>
							<div class="simple_input inline icon_dates">
								<input class="datepicker" type="text" name="calendar_start_date">
							</div>
							<div class="simple_input inline hours timer">
								<input type="number" name="calendar_start_hours" value="12" min="0" max="24">
								<span>val.</span>
							</div>
							<div class="simple_input inline minutes timer">
								<input type="number" name="calendar_start_minutes" value="00" min="0" max="59">
								<span>min.</span>
							</div>
							<div class="simple_input">
								<div class="label">Renginio vieta</div>
								<input type="text" name="event_place">
							</div>
						</div>
						<div class="col">
							<div class="label">Renginio pabaiga</div>
							<div class="simple_input inline icon_dates">
								<input class="datepicker" type="text" name="calendar_finish_date">
							</div>
							<div class="simple_input inline hours timer">
								<input type="number" name="calendar_finish_hours" value="12" min="0" max="24">
								<span>val.</span>
							</div>
							<div class="simple_input inline minutes timer">
								<input type="number" name="calendar_finish_minutes" value="00" min="0" max="59">
								<span>min.</span>
							</div>
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="label">Data ir laikas</div>
				<label class="simple_checkbox inline postpone">
					<input type="checkbox" name="postpone">
					<span class="name">
						<span>Atidėti publikavimą</span>
					</span>
				</label>
				<div class="time_pickers">
					<div>
						<div class="simple_input inline icon_dates">
							<input class="datepicker" type="text" name="postpone_date">
						</div>
						<div class="simple_input inline hours timer">
							<input type="number" name="postpone_hours" value="12" min="0" max="24">
							<span>val.</span>
						</div>
						<div class="simple_input inline minutes timer">
							<input type="number" name="postpone_minutes" value="00" min="0" max="59">
							<span>min.</span>
						</div>
					</div>
				</div>
				<div class="line"></div>
<!-- 				<div class="row">
					<div class="checkboxes_box">
						<div class="label">Šalis:</div>
						<label class="simple_radio">
							<input type="radio" name="country" checked>
							<span class="name">
								<span>Lietuva</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="country">
							<span class="name">
								<span>Užsienis</span>
							</span>
						</label>
					</div>
				</div>
				<div class="line"></div> -->
				<div class="row">
					<div class="col">
						<div class="label">Tema</div>
						<div class="simple_dropdown bigger">
							<input type="text" name="topic" disabled="" placeholder="Pasirinkite temą" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="label">Potemė</div>
						<div class="simple_dropdown bigger">
							<input type="text" name="subtopic" disabled="" placeholder="Pasirinkite potemę" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="add_another">Pridėti</div>
				<div class="line"></div>
				<div class="editor"></div>
				<div class="line"></div>
				<div class="drag_and_drop">
					<label class="area">
						<input multiple name="file" type="file" accept=".gif, .svg, .jpg, .png, .doc, .docx, .xls, .xlsx, .pptx, .ppt, .pdf |audio/*|video/*|image/*|media_type">
						<span class="texts">
							<span class="big">Drag&Drop</span>
							<span class="small">Norėdami įkelti failus tempkite juos pele į šią vietą arba <span class="blue">įkelkite</span> juos tiesiai iš kompiuterio</span>
						</span>
					</label>
					<div class="uploaded_area"></div>
				</div>
				<div class="line"></div>
				<div class="simple_input">
					<div class="label">Youtube nuoroda</div>
					<input type="text" name="youtube" placeholder="Pvz. https://www.youtube.com/watch?v=Pk5vXZJvWSU">
				</div>
				<div class="add_another">Pridėti nuorodą</div>
				<div class="line"></div>
				<div class="simple_input">
					<div class="label">Raktažodžiai</div>
					<input spellcheck="true" type="text" name="keywords" placeholder="Pvz. elta, seimas, vyriausybe, skvernelis, karbauskis">
				</div>
				<div class="line"></div>
				<div class="label">Gavėjai</div>
				<label class="simple_checkbox block">
					<input type="checkbox" name="sent_to_all">
					<span class="name">
						<span>Siųsti visiems</span>
					</span>
				</label>
				<label class="simple_checkbox block smaller_bottom">
					<input type="checkbox" class="group_check" name="press">
					<span class="name">
						<span>Spauda</span>
					</span>
				</label>
				<div class="multicheckboxes">
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="all_press" class="all">
						<span class="name">
							<span>Pažymėti viską</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
				</div>
				<label class="simple_checkbox block smaller_bottom">
					<input type="checkbox" class="group_check" name="radio_and_tv">
					<span class="name">
						<span>Radijas ir TV</span>
					</span>
				</label>
				<div class="multicheckboxes">
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="all_radio_tv" class="all">
						<span class="name">
							<span>Pažymėti viską</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
				</div>
				<label class="simple_checkbox block smaller_bottom">
					<input type="checkbox" class="group_check" name="web">
					<span class="name">
						<span>Interneto tinklapiai</span>
					</span>
				</label>
				<div class="multicheckboxes">
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" class="all" name="all_web">
						<span class="name">
							<span>Pažymėti viską</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
				</div>
				<label class="simple_checkbox block">
					<input type="checkbox" class="group_check" name="test_new">
					<span class="name">
						<span>Test new</span>
					</span>
				</label>
				<div class="multicheckboxes">
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" class="all" name="all_test_new">
						<span class="name">
							<span>Pažymėti viską</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="balsas">
						<span class="name">
							<span>Balsas</span>
						</span>
					</label>
					<label class="simple_checkbox smaller_bottom">
						<input type="checkbox" name="delfi">
						<span class="name">
							<span>Delfi</span>
						</span>
					</label>
				</div>
				<div class="line"></div>
				<div class="label naming">Kontaktinė informacija</div>
				
				<!-- Šitą rodom tik vadybininkui -->

				<label class="simple_checkbox minus_margin">
					<input type="checkbox" name="show_author" checked>
					<span class="name">
						<span>Rodyti autorių</span>
					</span>
				</label>

				<!-- ... -->

				<div class="row">
					<div class="col">
						<div class="simple_input">
							<div class="label">Pareigybė</div>
							<input type="text" name="position">
						</div>
					</div>
					<div class="col"></div>
				</div>
				<div class="row">
					<div class="col">
						<div class="simple_input">
							<div class="label">Vardas</div>
							<input type="text" name="name">
						</div>
						<div class="simple_input">
							<div class="label">Telefonas</div>
							<input type="text" name="phone">
						</div>
					</div>
					<div class="col">
						<div class="simple_input">
							<div class="label">Pavardė</div>
							<input type="text" name="surname">
						</div>
						<div class="simple_input">
							<div class="label">El. paštas</div>
							<input type="email" name="email">
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="form_buttons">
					<div class="error_message">Something went wrong!</div>
					<div class="delete">Trinti</div>
					<button class="button white big" type="submit">Išsaugoti tarp ruošiamų</button>
					<button class="button blue" type="submit">Išsaugoti ir publikuoti</button>
				</div>
				<a href="#" class="go_back_to_list bottom">atgal į sąrašą</a>
			</form>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>