<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_in.php';?>

<div class="page subscribe_page">
	<?php include '../partials/global_warning.php';?>
	<div class="wrapper smaller">
		<h1>Prenumeruoti naujienas</h1>
		<div class="simple_text grey">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
		</div>
		<form>
			<div class="row">
				<div class="col">
					<div class="simple_dropdown">
						<input type="text" name="theme" disabled="" value="Siųsti viską" placeholder="Pasirinkite temą">
						<div class="content">
							<div class="scroller_holder">
								<div class="option current">Siųsti viską</div>
								<div class="option">Politika</div>
								<div class="option">Ekonomika</div>
								<div class="option">Teisėtvarka</div>
								<div class="option">Soprtas</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="simple_input">
						<input type="email" name="theme_email" placeholder="El. paštas">
					</div>
				</div>
			</div>
			<div class="add_another">Pridėti</div>
			<div class="line"></div>
			<div class="row bottom">
				<div class="col">
					<label class="simple_checkbox">
						<input type="checkbox" name="subscribe_by_keywords">
						<span class="name">
							<span>Prenumerata pagal raktažodžius</span>
						</span>
					</label>
				</div>
			</div>
			<div class="keywords_hider">
				<div class="row bottom">
					<div class="col">
						<div class="simple_input">
							<input type="text" name="keywords" placeholder="Pvz. elta, seimas, vyriausybė">
						</div>
					</div>
					<div class="col">
						<div class="simple_input">
							<input type="email" name="keywords_email" placeholder="El. paštas">
						</div>
					</div>
				</div>
				<div class="add_another">Pridėti</div>
			</div>
			<div class="line"></div>
			<div class="submit_holder">
				<button type="submit" class="button blue">Išsaugoti</button>
			</div>
		</form>
	</div>
</div>

<?php include '../partials/footer.php';?>