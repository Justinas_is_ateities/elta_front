<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_off.php';?>

<div class="page text_page past_conference_page">
	<?php include '../partials/global_warning.php';?>
	<div class="shadow"></div>
	<div class="wrapper smaller">
		<h1>Apie Elta</h1>
		<div class="list_holder">
			<div class="conference">
				<div class="img" style="background-image: url('../media/images/thumb_photo_1.jpg');"></div>
				<div class="info">
					<div class="date">2018-03-12<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>20:40</div>
					<div class="name">Veiksmingiausia pagalba susirgus - Gydytojai klounai dalijosi išbandytais receptais<span class="has_photos">(4)</span></div>
				</div>
			</div>
			<div class="conference">
				<div class="img" style="background-image: url('../media/images/thumb_photo_1.jpg');"></div>
				<div class="info">
					<div class="date">2018-03-12<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>20:40</div>
					<div class="name">Veiksmingiausia<span class="has_photos">(4)</span></div>
				</div>
			</div>
			<div class="conference">
				<div class="img" style="background-image: url('../media/images/thumb_photo_1.jpg');"></div>
				<div class="info">
					<div class="date">2018-03-12<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>20:40</div>
					<div class="name">Veiksmingiausia pagalba susirgus - Gydytojai klounai dalijosi išbandytais receptais<span class="has_photos">(4)</span></div>
				</div>
			</div>
			<div class="conference">
				<div class="img" style="background-image: url('../media/images/thumb_photo_1.jpg');"></div>
				<div class="info">
					<div class="date">2018-03-12<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>20:40</div>
					<div class="name">Veiksmingiausia pagalba susirgus - Gydytojai klounai dalijosi išbandytais receptais<span class="has_photos">(4)</span></div>
				</div>
			</div>
			<div class="conference">
				<div class="img" style="background-image: url('../media/images/thumb_photo_1.jpg');"></div>
				<div class="info">
					<div class="date">2018-03-12<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>20:40</div>
					<div class="name">Veiksmingiausia pagalba susirgus - Gydytojai klounai dalijosi išbandytais receptais<span class="has_photos">(4)</span></div>
				</div>
			</div>
			<div class="conference">
				<div class="img" style="background-image: url('../media/images/thumb_photo_1.jpg');"></div>
				<div class="info">
					<div class="date">2018-03-12<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>20:40</div>
					<div class="name">Veiksmingiausia pagalba susirgus - Gydytojai klounai dalijosi išbandytais receptais<span class="has_photos">(4)</span></div>
				</div>
			</div>
			<div class="conference">
				<div class="img" style="background-image: url('../media/images/thumb_photo_1.jpg');"></div>
				<div class="info">
					<div class="date">2018-03-12<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>20:40</div>
					<div class="name">Veiksmingiausia pagalba susirgus - Gydytojai klounai dalijosi išbandytais receptais<span class="has_photos">(4)</span></div>
				</div>
			</div>
		</div>
		<div class="pager_holder">
			<div class="simple_pager">
				<a href="#" class="page_link">1</a>
				<a href="#" class="page_link current">2</a>
				<a href="#" class="page_link">3</a>
				<span>...</span>
				<a href="#" class="page_link">7</a>
			</div>
		</div>
	</div>
</div>

<?php include '../partials/footer.php';?>