<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_in.php';?>

<div class="page password_page">
	<?php include '../partials/global_warning.php';?>
	<div class="wrapper">
		<div class="center">
			<form id="password_form">
				<h1>Slaptažodžio keitimas</h1>
				<div class="simple_input">
					<input type="password" name="current_password" placeholder="Įveskite dabartinį slaptažodį">
				</div>
				<div class="simple_input">
					<input type="password" name="new_password" placeholder="Įveskite naują slaptažodį">
				</div>
				<div class="simple_input">
					<input type="password" name="repeat_new_password" placeholder="Pakartokite naują slaptažodį">
				</div>
				<button type="submit" class="button blue">Keisti slaptažodį</button>
			</form>
		</div>
	</div>
</div>

<?php include '../partials/footer.php';?>