<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_off.php';?>

<div class="page text_page conference_page">
	<?php include '../partials/global_warning.php';?>
	<div class="wrapper smaller">
		<h1>Konferencijų salė</h1>
		<div class="simple_text">Naujienų agentūra ELTA kviečia rengti spaudos konferencijas naujoje ir patogioje spaudos konferencijų salėje verslo centre VERTAS, Gynėjų g. 16, 11 aukšte, iš kurio atsiveria įspūdinga sostinės panorama. Salėje įrengtos šiuolaikiška įgarsinimo, apšvietimo, vėdinimo sistemos, veikia multimedijos projektorius.</div>
		<div class="swiper-container gallery-top">
			<div class="swiper-wrapper">
				<div class="swiper-slide" style="background-image: url('../media/images/main_photo.jpg');"></div>
				<div class="swiper-slide" style="background-image: url('../media/images/news.jpg');"></div>
				<div class="swiper-slide" style="background-image: url('../media/images/main_photo.jpg');"></div>
				<div class="swiper-slide" style="background-image: url('../media/images/news.jpg');"></div>
				<div class="swiper-slide" style="background-image: url('../media/images/main_photo.jpg');"></div>
			</div>
			<div class="swiper-button-next swiper-button-white"></div>
			<div class="swiper-button-prev swiper-button-white"></div>
		</div>
		<div class="swiper-container gallery-thumbs">
			<div class="swiper-wrapper">
				<div class="swiper-slide" style="background-image: url('../media/images/main_photo.jpg');"></div>
				<div class="swiper-slide" style="background-image: url('../media/images/news.jpg');"></div>
				<div class="swiper-slide" style="background-image: url('../media/images/main_photo.jpg');"></div>
				<div class="swiper-slide" style="background-image: url('../media/images/news.jpg');"></div>
				<div class="swiper-slide" style="background-image: url('../media/images/main_photo.jpg');"></div>
			</div>
		</div>
		<div class="simple_text">
			ELTA išplatina anonsą apie numatomą spaudos konferenciją ir rengėjų pranešimą spaudai bei sukviečia spaudos konferencijų dalyvius.<br/><br/>
			Po spaudos konferencijos ELTA savo užsakovams paskelbia informaciją ir išplatina Eltos fotografo nuotraukas. Tris spaudos konferencijas Eltoje surengęs klientas ketvirtą galės rengti nemokamai. Pagal atskirą susitarimą galima sudaryti metinę spaudos konferencijų salės nuomos sutartį su atitinkamomis nuolaidomis.<br/><br/>
			Salė gali būti nuomojama ir klientų renginiams - susitikimams, susirinkimams, posėdžiams.<br/><br/>
			Dėl salės nuomos prašome kreiptis: tel. <a href="tel:+37052616140">+370 5 2616140</a>, el. paštu: <a href="mailto:zinios@elta.lt" target="_top">zinios@elta.lt</a>
		</div>
		<h3>Kaina</h3>
		<table>
			<tr>
				<td>Spaudos konferencija (~ 1,5 val)</td>
				<td>KONFERENCIJAI</td>
				<td>DIENAI</td>
			</tr>
			<tr>
				<td>Salės nuoma</td>
				<td>109 Eur + PVM</td>
				<td>250 Eur + PVM</td>
			</tr>
		</table>
		<h3>Papildoma informacija</h3>
		<ul>
			<li>
				ELTA pateikia anonsą apie numatomą konferenciją ir rengėjų pranešimą spaudai
			</li>
			<li>Multimedia projektorius</li>
			<li>
				ELTA pateikia anonsą apie numatomą konferenciją ir rengėjų pranešimą spaudai
			</li>
			<li>Multimedia projektorius</li>
			<li>
				ELTA pateikia anonsą apie numatomą konferenciją ir rengėjų pranešimą spaudai
			</li>
			<li>Multimedia projektorius</li>
			<li>
				ELTA pateikia anonsą apie numatomą konferenciją ir rengėjų pranešimą spaudai
			</li>
		</ul>
	</div>
</div>

<?php include '../partials/footer.php';?>