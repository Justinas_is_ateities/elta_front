<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_off.php';?>

<div class="page title_page">
	<?php include '../partials/global_warning.php';?>
	<section class="hero">
		<div class="swiper-container visuals">
			<div class="swiper-wrapper">
				<div class="swiper-slide" style="background-image: url(../media/images/title_1.jpg)"></div>
				<div class="swiper-slide" style="background-image: url(../media/images/title_2.jpg)"></div>
			</div>	
		</div>
		<div class="swiper-container texts">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="text_holder">
						<div class="name">Newsfeed</div>
						<div class="simple_text">
							Give your audience real-time, reliable news that keeps them reading, watching & coming back for more.
						</div>
						<a class="button underlined" href="#">read more</a>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="text_holder">
						<div class="name">Lorem Ipsum labai</div>
						<div class="simple_text">
							Give your audience real-time, reliable news that keeps them reading, watching & coming back for more.
						</div>
						<a class="button underlined" href="#">read more</a>
					</div>
				</div>
			</div>	
			<div class="swiper-pagination swiper-pagination-white"></div>
		</div>
	</section>
	<section class="important">
		<div class="background"></div>
		<div class="wrapper">
			<div class="name">Besiveržiančio ugnikalnio lava pasiekė vandenyną: gyventojams gresia naujas pavojus<span class="badge">svarbu</span></div>
			<div class="simple_text">
				Valdžios atstovai Havajuose įspėjo nesiartinti prie lavos srautų, tekančių į vandenyną. Kai labai karšta lava pasiekia vėsų vandenį, susidaro vandenilio chlorido garai (angl. dar vadinami „laze“), kurie patenka į orą kartu su mažytėmis stiklo dalelėmis.
			</div>
			<a href="" class="button underlined">skaityti daugiau</a>
		</div>
	</section>
	<section class="numbers">
		<div class="wrapper">
			<div class="number_block icon_article">
				<div class="number">37 950</div>
				<div class="label">Articles and news per year</div>
			</div>
			<div class="number_block icon_photo">
				<div class="number">28 550</div>
				<div class="label">Articles and news per year</div>
			</div>
			<div class="number_block icon_play">
				<div class="number">47 547</div>
				<div class="label">Articles and news per year</div>
			</div>
		</div>
	</section>
	<section class="subscription">
		<div class="outer_box">
			<div class="left">
				<img src="../media/images/banner_1.png" alt="">
			</div>
			<div class="right"></div>
		</div>
		<div class="wrapper">
			<div class="left">
				<img src="../media/images/banner_1.png" alt="">
				<div class="text_box">
					<div class="name">Naujienų prenumerata</div>
					<div class="simple_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
					<a href="#" class="button white big">IŠBAnDYTI NEMOKAMAI</a>
				</div>
			</div>
			<div class="right">
				<div class="name">Pranešimai spaudai</div>
				<a href="#" class="read_more button underlined">ŽIŪRĖTI VIsKĄ</a>
				<div class="news">
					<a href="#" class="new">
						<div class="date_box">
							<div class="time">14:52</div>
							<div class="before">2 minutes ago</div>
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
						</div>
					</a>
					<a href="#" class="new">
						<div class="date_box">
							<div class="time">14:52</div>
							<div class="before">2 minutes ago</div>
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
						</div>
					</a>
<!-- 					<a href="#" class="new">
						<div class="date_box">
							<div class="time">14:52</div>
							<div class="before">2 minutes ago</div>
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
						</div>
					</a>
					<a href="#" class="new">
						<div class="date_box">
							<div class="time">14:52</div>
							<div class="before">2 minutes ago</div>
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
						</div>
					</a> -->
				</div>
			</div>
		</div>
	</section>
	<section class="themes" style="background-image: url('../media/images/themes.jpg');">
		<div class="wrapper">
			<h2>Temos</h2>
			<div class="themes_holder">
				<div class="theme">
					<div class="img_holder">
						<img src="../media/images/politika.svg" alt="">
					</div>
					<div class="text_holder">
						<div class="name">Politika</div>
						<div class="simple_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
					</div>
				</div>
				<div class="theme">
					<div class="img_holder">
						<img src="../media/images/ekonomika.svg" alt="">
					</div>
					<div class="text_holder">
						<div class="name">Politika</div>
						<div class="simple_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
					</div>
				</div>
				<div class="theme">
					<div class="img_holder">
						<img src="../media/images/politika.svg" alt="">
					</div>
					<div class="text_holder">
						<div class="name">Politika</div>
						<div class="simple_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
					</div>
				</div>
				<div class="theme">
					<div class="img_holder">
						<img src="../media/images/ekonomika.svg" alt="">
					</div>
					<div class="text_holder">
						<div class="name">Politika</div>
						<div class="simple_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
					</div>
				</div>
				<div class="theme">
					<div class="img_holder">
						<img src="../media/images/ekonomika.svg" alt="">
					</div>
					<div class="text_holder">
						<div class="name">Politika</div>
						<div class="simple_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="add full_add">
		<div class="wrapper">
			<img class="desktop_only" src="../media/images/banner_1.jpg" alt="">
			<img class="mobile_only" src="../media/images/banner_1_mobile.jpg" alt="">
		</div>
	</section>
	<section class="video">
		<div class="wrapper">
			<div class="poster">
				<div class="img" style="background-image: url('../media/images/poster.jpg');"></div>
			</div>
			<h2><span>Naujienų agentūra ELTA</span></h2>
		</div>
	</section>
	<section class="hot_news">
		<div class="wrapper">
			<h2 class="name_after_video">Karštos naujienos</h2>
			<a href="#" class="left">
				<span class="new" style="background-image: url('../media/images/hot_new.jpg');">
					<span class="tag" data-color="#3574d1">Politika</span>
					<span class="content">
						<span class="date">2017-05-24</span>
						<span class="name">Rusijos kariniame biudžete – akivaizdūs pokyčiai: ką tai reiškia</span>
					</span>
				</span>
				<span class="new" style="background-image: url('../media/images/hot_new.jpg');">
					<span class="tag" data-color="#dc851d">Teisėtvarka</span>
					<span class="content">
						<span class="date">2017-05-24</span>
						<span class="name">Rusijos kariniame biudžete – akivaizdūs pokyčiai: ką tai reiškia</span>
					</span>
				</span>
				<span class="new" style="background-image: url('../media/images/hot_new.jpg');">
					<span class="tag" data-color="#3574d1">Politika</span>
					<span class="content">
						<span class="date">2017-05-24</span>
						<span class="name">Rusijos kariniame biudžete – akivaizdūs pokyčiai: ką tai reiškia</span>
					</span>
				</span>
				<span class="new" style="background-image: url('../media/images/hot_new.jpg');">
					<span class="tag" data-color="#1c9400">Sportas</span>
					<span class="content">
						<span class="date">2017-05-24</span>
						<span class="name">Rusijos kariniame biudžete – akivaizdūs pokyčiai: ką tai reiškia</span>
					</span>
				</span>
			</a>
			<div class="right">
				<div class="news">
					<a href="#" class="new">
						<div class="date_box">
							<div class="time">14:52</div>
							<div class="before">2 minutes ago</div>
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
						</div>
					</a>
					<a href="#" class="new">
						<div class="date_box">
							<div class="time">14:52</div>
							<div class="before">2 minutes ago</div>
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
						</div>
					</a>
					<a href="#" class="new">
						<div class="date_box">
							<div class="time">14:52</div>
							<div class="before">2 minutes ago</div>
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
						</div>
					</a>
					<a href="#" class="new">
						<div class="date_box">
							<div class="time">14:52</div>
							<div class="before">2 minutes ago</div>
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="photos">
		<div class="wrapper">
			<h2>Dienos nuotrauka</h2>
			<a href="#" class="left main_photo" style="background-image: url('../media/images/main_photo.jpg');">
				<span class="content">
					<span class="date">2017-05-24</span>
					<span class="name">Rusijos kariniame biudžete – akivaizdūs pokyčiai: ką tai reiškia</span>
				</span>
			</a>
			<div class="right">
				<a href="#" class="new" style="background-image: url('../media/images/hot_new.jpg');">
					<span class="content">
						<span class="date">2017-05-24</span>
						<span class="name">Rusijos kariniame biudžete – akivaizdūs pokyčiai: ką tai reiškia</span>
					</span>
				</a>
				<a href="#" class="new" style="background-image: url('../media/images/hot_new.jpg');">
					<span class="content">
						<span class="date">2017-05-24</span>
						<span class="name">Rusijos kariniame biudžete – akivaizdūs pokyčiai: ką tai reiškia</span>
					</span>
				</a>
				<a href="#" class="new" style="background-image: url('../media/images/hot_new.jpg');">
					<span class="content">
						<span class="date">2017-05-24</span>
						<span class="name">Rusijos kariniame biudžete – akivaizdūs pokyčiai: ką tai reiškia</span>
					</span>
				</a>
				<a href="#" class="new" style="background-image: url('../media/images/hot_new.jpg');">
					<span class="content">
						<span class="date">2017-05-24</span>
						<span class="name">Rusijos kariniame biudžete – akivaizdūs pokyčiai: ką tai reiškia</span>
					</span>
				</a>
			</div>
			<div class="add desktop_only" style="background-image: url('../media/images/banner_2.jpg');"></div>
			<div class="add mobile_only">
				<img src="../media/images/banner_1.png" alt="">
				<div class="text_box">
					<div class="name">Naujienų prenumerata</div>
					<div class="simple_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</div>
					<a href="#" class="button white big">IŠBAnDYTI NEMOKAMAI</a>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>
				
