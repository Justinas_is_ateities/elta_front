<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_off.php';?>

<div class="page contacts_page">
	<?php include '../partials/global_warning.php';?>
	<div class="wrapper">
		<h1>Kontaktai</h1>
		<div class="director_card">
			<div class="left" style="background-image: url('../media/images/contact.jpg');"></div>
			<div class="right">
				<div class="contact">
					<div class="position">Direktorė</div>
					<div class="name">Gitana Markovičienė</div>
					<div class="phone">Telefonas <a href="tel:+37052071993">+370 5 207 19 93</a></div>
					<div class="email">El. adresas <a href="mailto:eleonora@elta.lt" target="_top">eleonora@elta.lt</a></div>
				</div>
			</div>
		</div>
		<div class="contacts_grid">
			<div class="contact">
				<div class="position">Direktorė</div>
				<div class="name">Gitana Markovičienė</div>
				<div class="phone">Telefonas <a href="tel:+37052071993">+370 5 207 19 93</a></div>
				<div class="email">El. adresas <a href="mailto:eleonora@elta.lt" target="_top">eleonora@elta.lt</a></div>
			</div>
			<div class="contact">
				<div class="position">Direktorė</div>
				<div class="name">Gitana Markovičienė</div>
				<div class="phone">Telefonas <a href="tel:+37052071993">+370 5 207 19 93</a></div>
				<div class="email">El. adresas <a href="mailto:eleonora@elta.lt" target="_top">eleonora@elta.lt</a></div>
			</div>
			<div class="contact">
				<div class="position">Direktorė</div>
				<div class="name">Gitana Markovičienė</div>
				<div class="phone">Telefonas <a href="tel:+37052071993">+370 5 207 19 93</a></div>
				<div class="email">El. adresas <a href="mailto:eleonora@elta.lt" target="_top">eleonora@elta.lt</a></div>
			</div>
			<div class="contact">
				<div class="position">Direktorė</div>
				<div class="name">Gitana Markovičienė</div>
				<div class="phone">Telefonas <a href="tel:+37052071993">+370 5 207 19 93</a></div>
				<div class="email">El. adresas <a href="mailto:eleonora@elta.lt" target="_top">eleonora@elta.lt</a></div>
			</div>
			<div class="contact">
				<div class="position">Direktorė</div>
				<div class="name">Gitana Markovičienė</div>
				<div class="phone">Telefonas <a href="tel:+37052071993">+370 5 207 19 93</a></div>
				<div class="email">El. adresas <a href="mailto:eleonora@elta.lt" target="_top">eleonora@elta.lt</a></div>
			</div>
			<div class="contact">
				<div class="position">Direktorė</div>
				<div class="name">Gitana Markovičienė</div>
				<div class="phone">Telefonas <a href="tel:+37052071993">+370 5 207 19 93</a></div>
				<div class="email">El. adresas <a href="mailto:eleonora@elta.lt" target="_top">eleonora@elta.lt</a></div>
			</div>
		</div>
		<div class="map_holder">
			<div id="map"></div>
			<div class="etiquette">
				<div class="logo"></div>
				<div class="name">UAB Lietuvos naujienų agentūra ELTA</div>
				<div class="address">
					Gynėjų g. 16<br/>
					Vilnius, LT-01109, Lietuva<br/>
					Telefonas <a href="tel:+37052071993">+370 5 207 19 93</a><br/>
					El. adresas <a href="mailto:zinios@elta.lt" target="_top">zinios@elta.lt</a>
				</div>
				<div class="social_links">
					<a href="#" class="fb"></a>
					<a href="#" class="yt"></a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include '../partials/footer.php';?>