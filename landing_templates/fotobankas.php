<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_in.php';?>

<div class="page photos_page">
	<?php include '../partials/global_warning.php';?>
	<section class="filter another_dropdown">
		<label class="simple_checkbox auto_checkbox">
			<input type="checkbox" name="auto" checked>
			<span class="name">
				<span>Automatinis naujienų atsinaujinimas</span>
			</span>
		</label>
		<div class="head_tabs">
			<label class="head_tab">
				<input type="radio" name="lang_filter" value="lang_filter_all" checked>
				<span class="name"><span>Rodyti viską</span></span>
			</label>
			<label class="head_tab">
				<input type="radio" name="lang_filter" value="lang_filter_lt">
				<span class="name"><span>Lietuva</span></span>
			</label>
			<label class="head_tab">
				<input type="radio" name="lang_filter" value="lang_filter_en">
				<span class="name"><span>Užsienis</span></span>
			</label>
		</div>
		<?php include '../partials/search_client.php';?>
		<div class="button underlined detail_search">Detali paieška</div>
		<div class="opened_filter_buttons">
			<div class="button underlined close_search">Suskleisti paiešką</div>
			<div class="clear"></div>
			<div class="button underlined clear_filters">Išvalyti filtrą</div>
		</div>
		<div class="hidden_calendar">
			<div class="simple_input inline icon_dates">
				<input class="datepicker" type="text" name="calendar_date">
			</div>
			<div class="day_button today active">Šiandien</div>
			<div class="day_button tomorrow">Ryt</div>
		</div>
		<div class="right_box">
			<div class="label">Atvaizdavimas</div>
			<div class="view current" data-view="columns"></div>
			<div class="view" data-view="info"></div>
		</div>
	</section>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="mobile_only extra_filters">
			<label class="simple_checkbox auto_checkbox">
				<input type="checkbox" name="auto" checked>
				<span class="name">
					<span>Automatinis naujienų atsinaujinimas</span>
				</span>
			</label>
			<div class="simple_dropdown main_dropdown">
				<input type="text" disabled value="Visos naujienos">
				<div class="content">
					<div class="scroller_holder">
						<div class="option current">Visos naujienos</div>
						<div class="option">Lietuva</div>
						<div class="option">Užsienis</div>
					</div>
				</div>
			</div>
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_client_news.php';?>
			</div>
		</div>
		<div class="right layout">
			<div class="photos_holder">
				<div class="scroller_holder">
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
										<span class="tooltip">
											Liko parsisiųsti 74 vnt.
										</span>
									</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
										<span class="tooltip">
											Liko parsisiųsti 74 vnt.
										</span>
									</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit open_edit_pop">Redaguoti</span>
							<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
							<div class="info extra_tooltip">
								<div class="tooltip">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								</div>
							</div>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<div class="buttons info">
									<span class="edit open_edit_pop">Redaguoti</span>
									<a href="#" class="download simple_tooltip">
								<span class="tooltip">
									Liko parsisiųsti 74 vnt.
								</span>
							</a>
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="pager_holder">
						<div class="simple_pager">
							<a href="#" class="page_link">1</a>
							<a href="#" class="page_link current">2</a>
							<a href="#" class="page_link">3</a>
							<span>...</span>
							<a href="#" class="page_link">7</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>