<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_off.php';?>

<div class="page password_page">
	<?php include '../partials/global_warning.php';?>
	<div class="wrapper">
		<div class="center">
			<form id="password_form">
				<h1>Slaptažodžio priminimas</h1>
				<div class="simple_input">
					<input type="email" name="email" placeholder="Įveskite savo el.paštą">
				</div>
				<button type="submit" class="button blue">Priminti slaptažodį</button>
			</form>
		</div>
	</div>
</div>

<?php include '../partials/footer.php';?>