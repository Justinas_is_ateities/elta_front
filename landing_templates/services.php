<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_off.php';?>

<div class="page services_page">
	<?php include '../partials/global_warning.php';?>
	<div class="wrapper">
		<h1>Paslaugos</h1>
		<div class="service img_right">
			<div class="left">
				<div class="text_box">
					<h3>Naujienų rašymas</h3>
					<div class="simple_text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
					</div>
				</div>
			</div>
			<div class="right" style="background-image: url('../media/images/news.jpg');"></div>
		</div>
		<div class="service img_left" data-shadow>
			<div class="left" style="background-image: url('../media/images/news.jpg');"></div>
			<div class="right">
				<div class="text_box">
					<h3>Video ir audio paslaugos</h3>
					<div class="simple_text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
					</div>
				</div>
			</div>
		</div>
		<div class="service img_right">
			<div class="left">
				<div class="text_box">
					<h3>Konferencijų salė</h3>
					<div class="simple_text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et incididunt ut labore et <div class="simple_tooltip">
							<div class="content">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
							</div>
						</div>
						<div class="clear"></div>
						<a href="http://elta.devprojects.lt/landing_templates/conference.php" class="button underlined">Skaityti daugiau</a>
						<!-- <div class="price_list">
							<div class="price">
								<div class="label">Spaudos konferencija (~ 1,5 val)</div>
								<div class="value">Salės nuoma</div>
							</div>
							<div class="price">
								<div class="label">KONFERENCIJAI</div>
								<div class="value">109 Eur + PVM</div>
							</div>
							<div class="price">
								<div class="label">DIENAI</div>
								<div class="value">250 Eur + PVM</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
			<div class="right" style="background-image: url('../media/images/news.jpg');"></div>
		</div>
		<div class="service img_left">
			<div class="left" style="background-image: url('../media/images/news.jpg');"></div>
			<div class="right">
				<div class="text_box">
					<h3>Video ir audio paslaugos</h3>
					<div class="simple_text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include '../partials/footer.php';?>