<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_in.php';?>

<div class="page success_page">
	<?php include '../partials/global_warning.php';?>
	<div class="wrapper">
		<div class="center">
			<div class="heading">Slaptažodis išsiųstas sėkmingai</div>
			<div class="simple_text grey">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliqui
			</div>
			<a href="http://elta.devprojects.lt/landing_templates/title.php" class="button underlined">Grįžti į titulinį</a>
		</div>
	</div>
</div>

<?php include '../partials/footer.php';?>