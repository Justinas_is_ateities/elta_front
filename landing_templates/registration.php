<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_off.php';?>

<div class="page registration_page">
	<?php include '../partials/global_warning.php';?>
	<div class="wrapper smaller">
		<h1>Registracija</h1>
		<div class="simple_text grey">
			Teisė išbandyti ELTOS naujienas bus gali būti suteikta tik pilnai ir teisingai užpildžiusiems formą:
		</div>
		<form>
			<div class="radio_boxes">
				<div class="label">Vartotojo tipas</div>
				<label class="simple_radio">
					<input type="radio" id="user_type_company" name="person_company" checked>
					<span class="name">
						<span>Juridinis</span>
					</span>
				</label>
				<label class="simple_radio">
					<input type="radio" id="user_type_person" name="person_company">
					<span class="name">
						<span>Fizinis</span>
					</span>
				</label>
			</div>
			<div class="clear"></div>
			<div class="simple_input first">
				<div class="label">Vardas</div>
				<input type="text" name="name">
			</div>
			<div class="simple_input">
				<div class="label">Pavardė</div>
				<input type="text" name="surname">
			</div>
			<div class="company_hide">
				<div class="simple_input first">
					<div class="label">Pareigos</div>
					<input type="text" name="ocupation">
				</div>
				<div class="simple_input">
					<div class="label">Įmonės pavadinimas</div>
					<input type="text" name="company_name">
				</div>
			</div>
			<div class="simple_input first">
				<div class="label">Adresas:</div>
				<input type="text" name="address" placeholder="Gatvė, Nr., Miestas, Indeksas">
			</div>
			<div class="simple_input">
				<div class="label">Kontaktinis telefonas</div>
				<input type="number" name="phone">
			</div>
			<div class="simple_input first">
				<div class="label">El.paštas</div>
				<input type="email" name="email">
			</div>
			<div class="line"></div>
			<div class="checkboxes_box left">
				<div class="label">Dominančios ELTA naujienų, nuotraukų sritys</div>
				<label class="simple_checkbox">
					<input type="checkbox" name="politics">
					<span class="name">
						<span>Politika</span>
						<span class="tooltip">
							<ul>
								<li>Krepšinis</li>
								<li>Futbolas</li>
								<li>Bliardas</li>
								<li>Krepšinis</li>
								<li>Boksas</li>
								<li>Ledo ritulys</li>
								<li>Kriketas</li>
							</ul>
						</span>
					</span>
				</label>
				<label class="simple_checkbox">
					<input type="checkbox" name="culture">
					<span class="name">
						<span>Kultūra</span>
						<span class="tooltip">
							<ul>
								<li>Krepšinis</li>
								<li>Futbolas</li>
								<li>Bliardas</li>
								<li>Krepšinis</li>
								<li>Boksas</li>
								<li>Ledo ritulys</li>
								<li>Kriketas</li>
							</ul>
						</span>
					</span>
				</label>
				<label class="simple_checkbox">
					<input type="checkbox" name="economy">
					<span class="name">
						<span>Ūkis</span>
						<span class="tooltip">
							<ul>
								<li>Krepšinis</li>
								<li>Futbolas</li>
								<li>Bliardas</li>
								<li>Krepšinis</li>
								<li>Boksas</li>
								<li>Ledo ritulys</li>
								<li>Kriketas</li>
							</ul>
						</span>
					</span>
				</label>
				<label class="simple_checkbox">
					<input type="checkbox" name="sport">
					<span class="name">
						<span>Sportas</span>
						<span class="tooltip">
							<ul>
								<li>Krepšinis</li>
								<li>Futbolas</li>
								<li>Bliardas</li>
								<li>Krepšinis</li>
								<li>Boksas</li>
								<li>Ledo ritulys</li>
								<li>Kriketas</li>
							</ul>
						</span>
					</span>
				</label>
				<label class="simple_checkbox">
					<input type="checkbox" name="law">
					<span class="name">
						<span>Teisėtvarka</span>
						<span class="tooltip">
							<ul>
								<li>Krepšinis</li>
								<li>Futbolas</li>
								<li>Bliardas</li>
								<li>Krepšinis</li>
								<li>Boksas</li>
								<li>Ledo ritulys</li>
								<li>Kriketas</li>
							</ul>
						</span>
					</span>
				</label>
				<label class="simple_checkbox">
					<input type="checkbox" name="elta_guide">
					<span class="name">
						<span>Eltos gidas</span>
						<span class="tooltip">
							<ul>
								<li>Krepšinis</li>
								<li>Futbolas</li>
								<li>Bliardas</li>
								<li>Krepšinis</li>
								<li>Boksas</li>
								<li>Ledo ritulys</li>
								<li>Kriketas</li>
							</ul>
						</span>
					</span>
				</label>
			</div>
			<div class="checkboxes_box">
				<div class="label">Dominančios ELTA naujienų sritys</div>
				<label class="simple_checkbox">
					<input type="checkbox" name="lithuania">
					<span class="name">
						<span>Lietuvos</span>
						<span class="tooltip">
							<ul>
								<li>Krepšinis</li>
								<li>Futbolas</li>
								<li>Bliardas</li>
								<li>Krepšinis</li>
								<li>Boksas</li>
								<li>Ledo ritulys</li>
								<li>Kriketas</li>
							</ul>
						</span>
					</span>
				</label>
				<label class="simple_checkbox">
					<input type="checkbox" name="foreign">
					<span class="name">
						<span>Užsienio</span>
						<span class="tooltip">
							<ul>
								<li>Krepšinis</li>
								<li>Futbolas</li>
								<li>Bliardas</li>
								<li>Krepšinis</li>
								<li>Boksas</li>
								<li>Ledo ritulys</li>
								<li>Kriketas</li>
							</ul>
						</span>
					</span>
				</label>
			</div>
			<div class="line"></div>
			<div class="simple_textarea">
				<div class="label">Pastabos ir pageidavimai</div>
				<textarea name="comments"></textarea>
			</div>
			<div class="line"></div>
			<div class="rules">
				<div class="label">Registracijos taisyklės</div>
				<div class="simple_text grey">
						1. Patvirtinu, kad registracijai pateikiau teisingą informaciją.<br/>
						2. Gavęs teisę matyti Eltos naujienas, įsipareigoju be išankstinio raštiško ELTA sutikimo:<br/>
						2. 1. Neperduoti jokiai kitai šaliai suteikto vartotojo vardo ir slaptažodžio;<br/>
						2. 2. Nekaupti ir nesaugoti bet kokiu būdu ir pavidalu ilgiau kaip 7 dienas ir nesudaryti sąlygų tai padaryti kitiems jokių ELTA tekstų ir/ar vaizdų;<br/>
						2. 3. Neplatinti, neperleisti, neperduoti bet kokiu būdu ir pavidalu jokiai kitai šaliai ar į kitas duomenų bazes ELTA tekstų ir/ar vaizdų;
				</div>
				<label class="simple_checkbox">
					<input type="checkbox" name="rules">
					<span class="name">
						<span>Su taisyklėmis susipažinau ir sutinku</span>
					</span>
				</label>
			</div>
			<div class="line"></div>
			<div class="submit_holder">
				<button type="submit" class="button blue">Registruotis</button>
			</div>
		</form>
	</div>
</div>

<?php include '../partials/footer.php';?>