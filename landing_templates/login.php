<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_off.php';?>

<div class="page login_page">
	<?php include '../partials/global_warning.php';?>
	<div class="flex_holder">
		<div class="wrapper">
			<div class="left">
				<form id="login_form">
					<h2>Prisijungimas</h2>
					<div class="simple_input">
						<input type="text" name="name" placeholder="VARTOTOJO VARDAS">
					</div>
					<div class="simple_input">
						<input type="password" name="password" placeholder="SLAPATAŽODIS">
					</div>
					<button type="submit" class="button blue">prisijungti</button>
					<a href="http://elta.devprojects.lt/landing_templates/frogot_password.php" class="forgot button underlined">Pamiršote slaptažodį?</a>
				</form>
			</div>
			<div class="mobile_right">
				<a href="http://elta.devprojects.lt/landing_templates/registration.php" class="button white">Sukurti naują paskyrą</a>
			</div>
			<div class="right">
				<form id="register_form">
					<h2>Registracija</h2>
					<div class="simple_input">
						<input type="email" name="name" placeholder="JŪSŲ EL.PAŠTAS">
					</div>
					<button type="submit" class="button blue">Sukurti naują paskyrą</button>
				</form>
			</div>
		</div>
	</div>
</div>

<?php include '../partials/footer.php';?>