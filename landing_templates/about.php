<?php include '../partials/head.php';?>
<?php include '../partials/header_logged_off.php';?>

<div class="page text_page about_page">
	<?php include '../partials/global_warning.php';?>
	<div class="shadow"></div>
	<div class="wrapper smaller">
		<h1>Apie Elta</h1>
		<section class="video">
			<div class="poster">
				<div class="img" style="background-image: url('../media/images/poster.jpg');"></div>
				<div class="play"></div>
			</div>
		</section>
		<div class="simple_text">
			ELTA - pirmaujanti žinių agentūra Baltijos šalyse. ELTA - ant ilgametės patirties pamato išaugusi dinamiška ir moderni nacionalinė naujienų agentūra, teikianti aktualią, patikimą, subalansuotą informaciją.<br/><br/>
			ELTA - svarbiausios Lietuvos ir pasaulio naujienos, pristatomos patogiai ir laiku. Eltos svetainėje www.elta.lt visą parą nuolat atnaujinama originali ir verstinė informacija apie politiką, verslą, sportą, kultūrą, teisėsaugą, pramogas.<br/><br/>
			Eltos naujienų agentūroje dirba patyrę žurnalistai, fotokorespondentai, vertėjai, redaktoriai. Eltos fotobanke sukaupta per 100 tūkstančių archyvinių nuotraukų nuo 1985 m. 
		</div>
		<img src="../media/images/news.jpg" alt="">
		<div class="qoute">
			<span>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
			</span>
		</div>
		<div class="simple_text">
			Kiekvieną rytą ELTA praneša, kokie įvykiai tą dieną numatomi Lietuvoje, kuo prieš keletą metų ar šimtmečių konkreti diena buvo svarbi Lietuvai ir pasauliui, ką rašo šalies ir pasaulio spauda. ELTA skelbia visuomenės nuomonės apklausas, politologų komentarus, interviu su žinomais ir įtakingais žmonėmis. Verslo sraute - šalies įmonių naujienos, reguliariai pateikiamos Lietuvos ir pasaulio akcijų biržų žinios, valiutų kursai, tarptautinės verslo naujienos. Visas Eltos žinių srautas nuo 1996 m. archyvuojamas klientams prieinamoje duomenų bazėje.<br/><br/>
			ELTA Jums siūlo ir naują modernią spaudos konferencijų salę, kurioje galima rengti konferencijas, seminarus, pasitarimus, mokymus.
		</div>
		<h2>
			Eltos žmonės
		</h2>
		<div class="simple_text">
			Eltos informaciją kuria geriausi žurnalistai-profesionalai, renkantys žinias tik etiškais ir teisėtais būdais. Eltos komanda yra operatyvi, bet ne paviršutiniška; aštri, bet ne tendencinga; konkurencinga, bet ne įžūli; principinga, bet ne tiesmuka.
		</div>
		<img src="../media/images/news.jpg" data-shadow-start alt="">
		<h2>Eltos klientai</h2>
		<div class="simple_text">
			Eltos informaciją kuria geriausi žurnalistai-profesionalai, renkantys žinias tik etiškais ir teisėtais būdais. Eltos komanda yra operatyvi, bet ne paviršutiniška; aštri, bet ne tendencinga; konkurencinga, bet ne įžūli; principinga, bet ne tiesmuka.
		</div>
		<div class="clients_grid">
			<div class="img_holder">
				<img src="../media/images/client_1.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_2.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_1.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_2.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_1.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_2.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_1.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_2.png" alt="">
			</div>
		</div>
		<div class="simple_text">
			Eltos informaciją kuria geriausi žurnalistai-profesionalai, renkantys žinias tik etiškais ir teisėtais būdais. Eltos komanda yra operatyvi, bet ne paviršutiniška; aštri, bet ne tendencinga; konkurencinga, bet ne įžūli; principinga, bet ne tiesmuka.
		</div>
		<h2>Eltos partneriai</h2>
		<div class="simple_text">
			Eltos informaciją kuria geriausi žurnalistai-profesionalai, renkantys žinias tik etiškais ir teisėtais būdais. Eltos komanda yra operatyvi, bet ne paviršutiniška; aštri, bet ne tendencinga; konkurencinga, bet ne įžūli; principinga, bet ne tiesmuka.
		</div>
		<div class="clients_grid">
			<div class="img_holder">
				<img src="../media/images/client_1.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_2.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_1.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_2.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_1.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_2.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_1.png" alt="">
			</div>
			<div class="img_holder">
				<img src="../media/images/client_2.png" alt="">
			</div>
		</div>
		<div class="simple_text">
			Eltos informaciją kuria geriausi žurnalistai-profesionalai, renkantys žinias tik etiškais ir teisėtais būdais. Eltos komanda yra operatyvi, bet ne paviršutiniška; aštri, bet ne tendencinga; konkurencinga, bet ne įžūli; principinga, bet ne tiesmuka.
		</div>
		<img src="../media/images/news.jpg" data-shadow-finish alt="">
		<h2>Mūsų istorija</h2>
		<div class="history_grid">
			<div class="history">
				<div class="years">1920</div>
				<div class="simple_text">
					<span>
						ELTA įkurta 1920 metais. Pirmasis agentūros direktorius, šveicarų kilmės literatūros profesorius, publicistas, visuomenės veikėjas, filosofijos daktaras Juozas Eretas siekė, jog Eltos žinių srautas būtų operatyvus ir patikimas kaip šveicariškas laikrodis.
					</span>
				</div>
			</div>
			<div class="history">
				<div class="years">1920</div>
				<div class="simple_text">
					<span>
						ELTA įkurta 1920 metais.
					</span>
				</div>
			</div>
			<div class="history">
				<div class="years">1920</div>
				<div class="simple_text">
					<span>
						ELTA įkurta 1920 metais. Pirmasis agentūros direktorius, šveicarų kilmės literatūros profesorius, publicistas,
					</span>
				</div>
			</div>
			<div class="history">
				<div class="years">1920</div>
				<div class="simple_text">
					<span>
						ELTA įkurta 1920 metais.
					</span>
				</div>
			</div>
			<div class="history">
				<div class="years">1920</div>
				<div class="simple_text">
					<span>
						ELTA įkurta 1920 metais. Pirmasis agentūros direktorius, šveicarų kilmės literatūros profesorius, publicistas, visuomenės veikėjas, filosofijos daktaras Juozas Eretas siekė, jog Eltos žinių srautas būtų operatyvus ir patikimas kaip šveicariškas laikrodis.
					</span>
				</div>
			</div>
			<div class="history">
				<div class="years">1920</div>
				<div class="simple_text">
					<span>
						ELTA įkurta 1920 metais.
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include '../partials/footer.php';?>