<header style="opacity: 0;">
	<a href="http://elta.devprojects.lt/landing_templates/title.php" id="logo"></a>
	<div class="right_block">
		<ul class="main_menu">
			<li><a href="#">Naujienos</a></li>
			<li><a href="#">Pranešimai spaudai</a></li>
			<li><a class="active" href="http://elta.devprojects.lt/landing_templates/conference.php">Įvykusios konferencijos</a></li>
			<li><a href="http://elta.devprojects.lt/landing_templates/photos.php">fotobankas</a></li>
			<li class="toggler">
				<span>Eltos gidas</span>
				<div class="submenu">
					<a class="active" href="#">Section 1</a>
					<a href="#">Section 2</a>
					<a href="#">Section 3</a>
				</div>
			</li>
		</ul>
		<div class="header_toggler langs">
			<span>LT</span>
			<div class="submenu">
				<a class="active" href="#">LT</a>
				<a href="#">EN</a>
				<a href="#">LV</a>
			</div>
		</div>
		<div class="header_toggler person">
			<span>Jonas Jonaitis</span>
			<div class="submenu">
				<a class="active" href="#">Keisti slaptažodį</a>
				<a href="#">Prenumeruoti naujienas</a>
				<a href="#">Atsijungti</a>
			</div>
		</div>
	</div>
	<div class="mobile_header">
		<div class="mobile_search_button"></div>
		<div class="login mobile_login mobile_person_opener"></div>
		<div class="burger">
			<div class="plank"></div>
			<div class="plank"></div>
			<div class="plank"></div>
		</div>
	</div>
</header>

<div class="mobile_menu">
	<ul>
		<li><a href="#">Naujienos</a></li>
		<li><a href="#">Pranešimai spaudai</a></li>
		<li><a class="active" href="http://elta.devprojects.lt/landing_templates/conference.php">Įvykusios konferencijos</a></li>
		<li><a href="http://elta.devprojects.lt/landing_templates/photos.php">fotobankas</a></li>
		<li class="mobile_toggler">
			<span>Eltos gidas</span>
		</li>
	</ul>
	<div class="mobile_langs">
		<a href="#">EN</a>
		<a class="active" href="#">LT</a>
		<a href="#">RU</a>
	</div>
</div>
<div class="mobile_submenu">
	<ul>
		<li><a href="#">Section-1</a></li>
		<li><a class="active" href="#">Section-2</a></li>
		<li><a href="#">Section-3</a></li>
	</ul>
</div>
<div class="mobile_person_menu">
	<ul>
		<li><a href="#" class="active icon_person">Jonas Jonaitis</a></li>
		<li><a href="#" class="icon_news">Prenumeruoti naujienas</a></li>
		<li><a href="#" class="icon_key">Slaptažodžio keitimas</a></li>
		<li><a href="#" class="icon_logout">Atsijungti</a></li>
	</ul>
</div>
<div class="mobile_search">
	<div class="search_button"></div>
	<input type="text" name="search" placeholder="Search">
</div>

<main>
	<div id="scroller">