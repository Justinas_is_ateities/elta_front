<header style="opacity: 0;">
	<a href="http://elta.devprojects.lt/landing_templates/title.php" id="logo"></a>
	<div class="right_block">
		<ul class="main_menu">
			<li><a href="#">Pranešimai spaudai</a></li>
			<li><a class="active" href="http://elta.devprojects.lt/landing_templates/conference.php">Įvykusios konferencijos</a></li>
			<li><a href="http://elta.devprojects.lt/landing_templates/photos.php">fotobankas</a></li>
			<li class="toggler">
				<span>Eltos gidas</span>
				<div class="submenu">
					<a class="active" href="#">Section 1</a>
					<a href="#">Section 2</a>
					<a href="#">Section 3</a>
				</div>
			</li>
			<li><a href="http://elta.devprojects.lt/landing_templates/services.php">paslaugos</a></li>
			<li><a href="http://elta.devprojects.lt/landing_templates/contacts.php">kontaktai</a></li>
		</ul>
		<div class="search">
			<div class="search_button"></div>
			<input type="text" name="search" placeholder="Search">
		</div>
		<a href="http://elta.devprojects.lt/landing_templates/login.php" class="login button underlined">Prisijungti</a>
	</div>
	<div class="mobile_header">
		<div class="mobile_search_button"></div>
		<a href="http://elta.devprojects.lt/landing_templates/login.php" class="login mobile_login"></a>
		<div class="burger">
			<div class="plank"></div>
			<div class="plank"></div>
			<div class="plank"></div>
		</div>
	</div>
</header>

<div class="mobile_menu">
	<ul>
		<li><a href="#">Pranešimai spaudai</a></li>
		<li><a class="active" href="http://elta.devprojects.lt/landing_templates/conference.php">Įvykusios konferencijos</a></li>
		<li><a href="http://elta.devprojects.lt/landing_templates/photos.php">fotobankas</a></li>
		<li class="mobile_toggler">
			<span>Eltos gidas</span>
		</li>
		<li><a href="http://elta.devprojects.lt/landing_templates/services.php">paslaugos</a></li>
		<li><a href="http://elta.devprojects.lt/landing_templates/contacts.php">kontaktai</a></li>
	</ul>
</div>

<div class="mobile_submenu">
	<ul>
		<li><a href="#">Section-1</a></li>
		<li><a class="active" href="#">Section-2</a></li>
		<li><a href="#">Section-3</a></li>
	</ul>
</div>

<div class="mobile_search">
	<div class="search_button"></div>
	<input type="text" name="search" placeholder="Search">
</div>

<main>
	<div id="scroller">
	