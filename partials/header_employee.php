<header style="opacity: 0;">
	<a href="http://elta.devprojects.lt/landing_templates/title.php" id="logo"></a>
	<div class="right_block">
		<div class="create_menu">
			<span class="name">sukurti</span>
			<div class="content">
				<div class="scroller_holder">
					<a href="#">Naujieną</a>
					<a href="#">Pranešimą spaudai</a>
					<a href="#">Numatomą įvykį</a>
					<a href="#">Gidas</a>
					<a href="#">Nuotrauką</a>
					<a href="#">Įvykusią konf.</a>
					<a href="#">Naujieną</a>
					<a href="#">Įvykusią konf.</a>
					<a href="#">Naujieną</a>
					<a href="#">Įvykusią konf.</a>
				</div>
			</div>
		</div>
		<div class="header_toggler langs">
			<span>LT</span>
			<div class="submenu">
				<a class="active" href="#">LT</a>
				<a href="#">EN</a>
				<a href="#">LV</a>
			</div>
		</div>
		<div class="header_toggler person">
			<span>Jonas Jonaitis</span>
			<div class="submenu">
				<a class="active" href="#">Keisti slaptažodį</a>
				<a href="#">Atsijungti</a>
			</div>
		</div>
	</div>
	<div class="mobile_header">
		<div class="mobile_search_button"></div>
		<div class="login mobile_login mobile_person_opener"></div>
		<div class="burger_plus">
			<div class="plank"></div>
			<div class="plank"></div>
		</div>
	</div>
</header>

<div class="mobile_menu">
	<ul>
		<li><a href="#">Naujieną</a></li>
		<li><a href="#">Pranešimą spaudai</a></li>
		<li><a href="#">Numatomą įvykį</a></li>
		<li><a href="#">Gidas</a></li>
		<li><a href="#">Nuotrauką</a></li>
		<li><a href="#">Įvykusią konf.</a></li>
	</ul>
	<div class="mobile_langs">
		<a href="#">EN</a>
		<a class="active" href="#">LT</a>
		<a href="#">RU</a>
	</div>
</div>
<div class="mobile_submenu">
	<ul>
		<li><a href="#">Section-1</a></li>
		<li><a class="active" href="#">Section-2</a></li>
		<li><a href="#">Section-3</a></li>
	</ul>
</div>
<div class="mobile_person_menu">
	<ul>
		<li><a href="#" class="active icon_person">Jonas Jonaitis</a></li>
		<li><a href="#" class="icon_key">Slaptažodžio keitimas</a></li>
		<li><a href="#" class="icon_logout">Atsijungti</a></li>
	</ul>
</div>
<div class="mobile_search">
	<div class="search_button"></div>
	<input type="text" name="search" placeholder="Search">
</div>

<main>
	<div id="scroller">