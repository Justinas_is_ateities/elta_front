<!-- Čia universalus pranesimo blokelis, kuris yra visuose templeituose sudetas, kad bet kada bet kuo butu galima 'informuoti' vartotoja - kad kas nors istrinta ar sukurta ir pan. Atidaromas globalia funkcija openWarning(), uzdaromas darsyk iskvietus ta pacia openWarning() . Turini pagal situacija teks pakeisti dinamiskai pries saukiant openWarning() -->

<div class="global_warning">
	Įrašas sėkmingai sukurtas "Ruošiamos naujienos/Eltos gidas/Numatoma lietuvoje". <a href="#">Įrašą peržiūrėti galite čia.</a>
</div>