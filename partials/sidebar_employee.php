<div class="toggler">
	<div class="toggler_head" data-sector="">
		<div class="side_color" style="background: #d17c3d;"></div>
		<span>
			<span>Priskirta man</span>
			<span class="has_messages">2</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>ELTOS gidas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Biuleteniai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatoma Lietuvoje
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatomos vaizdo informacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Lietuvos istorija mena
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Užsienio istorinių sukakčių kalendorius
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Astrologinė prognozė kitai savaitei
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Astrologija, orai, geri patarimai, juokai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				TV programa
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įvairybės
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Pranešimai spaudai</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Politika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisėtvarka
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kultūra
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Sportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Sveikata
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kalendorius
			</div>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<div class="side_color" style="background: #3574d1;"></div>
		<span>
			<span>Ruošiamos naujienos</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>ELTOS gidas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Biuleteniai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatoma Lietuvoje
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatomos vaizdo informacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Lietuvos istorija mena
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Užsienio istorinių sukakčių kalendorius
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Astrologinė prognozė kitai savaitei
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Astrologija, orai, geri patarimai, juokai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				TV programa
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įvairybės
			</div>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Užsienio naujienos</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>AFP</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>DPA</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>TASS</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Atidėtas publikavimas</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Eltos gidas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Biuleteniai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatoma Lietuvoje
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatomos vaizdo informacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Lietuvos istorija mena
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Užsienio istorinių sukakčių kalendorius
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Astrologinė prognozė kitai savaitei
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Astrologija, orai, geri patarimai, juokai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				TV programa
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įvairybės
			</div>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Publikuotos naujienos</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Eltos gidas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Biuleteniai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatoma Lietuvoje
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatomos vaizdo informacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Lietuvos istorija mena
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Užsienio istorinių sukakčių kalendorius
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Astrologinė prognozė kitai savaitei
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Astrologija, orai, geri patarimai, juokai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				TV programa
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įvairybės
			</div>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Ruošiamos nuotraukos</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Užsienio nuotraukos</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>EPA</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Publikuotos nuotraukos</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Ruošiami pranešimai spaudai</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Atidėtas publikavimas pranešimai spaudai</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Ruošiami pranešimai spaudai</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Publikuoti pranešimai spaudai</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Diplomatija
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Organizacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Parlamentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prezidentas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Saugumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Seimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Valdymas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vyriausybė
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Ekonomika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Energetika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Finansai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Paslaugos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Prekyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Ryšiai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Statyba
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Transportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įmonės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žemė
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Susisiekimas
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Institucijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nelaimės
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Nusikaltimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisingumas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Tvarkdara
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Bažnyčia
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Medicina
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Menas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Mokslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pramogos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Įstaigos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Švietimas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiniasklaida
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Atletinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kamuolinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kovinis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Struktūros
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Techninis
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Vandens
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žaidimai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Žiemos
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Įvairūs</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Įvykusios konferencijos</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Svarbi naujiena tituliniame</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Šiukšliadėžė</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Naujienos</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Politika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisėtvarka
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kultūra
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Sportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Sveikata
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Pranešimai spaudai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Biuleteniai
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatoma Lietuvoje
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Numatomos vaizdo informacijos
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Lietuvos istorija mena
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Užsienio istorinių sukakčių kalendorius
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Astrologinė prognozė kitai savaitei
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				TV programa
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kas kur kada?
			</div>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Nuotraukos</span>
			</span>
			<div class="sub_opener"><span></span></div>
		</div>
		<div class="toggler_sub_body">
			<div class="sub_subsector" data-sub-subsector="">
				Politika
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Verslas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Teisėtvarka
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Kultūra
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Sportas
			</div>
			<div class="sub_subsector" data-sub-subsector="">
				Sveikata
			</div>
		</div>
	</div>
</div>