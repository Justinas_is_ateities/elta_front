<div class="toggler">
	<div class="toggler_head active" data-sector="all">
		<span>
			<span>Viskas</span>
		</span>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Biuleteniai</span>
		</span>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Numatoma Lietuvoje</span>
		</span>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Numatomos vaizdo informacijos</span>
		</span>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Lietuvos istorija mena</span>
		</span>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Užsienio istorinių sukakčių kalendorius</span>
		</span>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Astrologinė prognozė kitai savaitei</span>
		</span>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>TV programa</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Lietuva</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Užsienis</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Kas kur kada?</span>
		</span>
	</div>
</div>
