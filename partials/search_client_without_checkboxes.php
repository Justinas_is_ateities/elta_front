<div class="search">
	<input type="text" class="search_main_input" placeholder="Įveskite, ko ieškote...">
	<button class="icon_search"><span>Ieškoti</span></button>
	<div class="search_info_warning">
		Paieškos rezultatuose pateikiama dviejų paskutinių savaičių informacija, norėdami ieškoti ilgesnio periodo naujienų naudokite detalią paiešką.
	</div>
	<div class="full_search_holder">
		<div class="left_search_side">
			<div class="simple_input icon_dates">
				<input type="text" name="dates" placeholder="Pasirinkite laikotarpį" value="">
			</div>
			<div class="simple_dropdown">
				<input type="text" name="category" disabled="" placeholder="Pasirinkite temą" value="">
				<div class="content">
					<div class="scroller_holder">
						<div class="option">Visos naujienos</div>
						<div class="option">Politika</div>
						<div class="option">Ekonomika</div>
						<div class="option">Teisėtvarka</div>
						<div class="option">Soprtas</div>
					</div>
				</div>
			</div>
			<div class="simple_dropdown">
				<input type="text" name="subcategory" disabled="" placeholder="Pasirinkite potemę" value="">
				<div class="content">
					<div class="scroller_holder">
						<div class="option">Visos naujienos</div>
						<div class="option">Politika</div>
						<div class="option">Ekonomika</div>
						<div class="option">Teisėtvarka</div>
						<div class="option">Soprtas</div>
					</div>
				</div>
			</div>
		</div>
		<div class="right_search_side">
			<div class="autocomplete_dropdown">
				<div class="planks">
					<div class="plank"></div>
					<div class="plank"></div>
					<div class="plank"></div>
				</div>
				<select class="for_desktop" name="author" data-placeholder="Pasirinkite autorių" tabindex="1">
					<option value=""></option>
					<option value="Visos naujienos">Visos naujienos</option>
					<option value="Politika">Politika</option>
					<option value="Soprtas">Soprtas</option>
					<option value="Visos naujienos">Visos naujienos</option>
					<option value="Politika">Politika</option>
					<option value="Soprtas">Soprtas</option>
					<option value="Visos naujienos">Visos naujienos</option>
					<option value="Politika">Politika</option>
					<option value="Soprtas">Soprtas</option>
				</select>
				<!-- Cia dublikatas del mobile -->
				<select name="author_2" class="for_mobile not_touched">
					<option value="Visos naujienos">Visos naujienos</option>
					<option value="Politika">Politika</option>
					<option value="Soprtas">Soprtas</option>
					<option value="Visos naujienos">Visos naujienos</option>
					<option value="Politika">Politika</option>
					<option value="Soprtas">Soprtas</option>
					<option value="Visos naujienos">Visos naujienos</option>
					<option value="Politika">Politika</option>
					<option value="Soprtas">Soprtas</option>
				</select>
			</div>
			<div class="simple_input">
				<input type="text" name="keywords" spellcheck="true" placeholder="Raktažodžiai" value="">
			</div>
		</div>
		<div class="buttons_for_mobile_filter">
			<button class="icon_search"><span>Ieškoti</span></button>
			<div class="button underlined clear_filters">Išvalyti filtrą</div>
		</div>
	</div>
</div>