<div id="overlay" class="overlay">
	
	<div class="pop mobile_detail_filter">
		<div class="close"></div>
		<?php include '../partials/search_client.php';?>
		<div class="clear"></div>
	</div>

	<div class="pop photos">
		<div class="photos_pop_head">
			<div class="name"></div>
			<div class="time_stamp">
				<span class="date"></span>
				<span class="time"></span>
			</div>
			<div class="right_box">
				<span class="edit open_edit_pop">Redaguoti</span>
				<a href="#" class="delete">ištrinti</a>
				<div class="close_box"></div>
			</div>
		</div>
		<div class="slider_box">
			<div class="swiper-container pop_photo_box">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="img_holder">
							<img src="../media/images/main_photo.jpg" alt="">
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						</div>
						<div class="name">Vilniaus „Lietuvos rytas“ Lorem ipsum Irmanto Gelūno nuotr.</div>
						<div class="time_stamp">
							<span class="date">2017-01-11</span>
							<span class="time">21:45</span>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_holder">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						</div>
						<div class="name">Vilniaus „Lietuvos rytas“ - Klaipėdos „Neptūnas“ / Irmanto Gelūno nuotr.</div>
						<div class="time_stamp">
							<span class="date">2017-03-21</span>
							<span class="time">21:45</span>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_holder">
							<img src="../media/images/main_photo.jpg" alt="">
						</div>
						<div class="simple_text">
							Lorem 
						</div>
						<div class="name">Vilniaus „Lietuvos rytas“ - Klaipėdos „Neptūnas“ / Irmanto Gelūno nuotr.</div>
						<div class="time_stamp">
							<span class="date">2017-03-21</span>
							<span class="time">21:45</span>
						</div>
					</div>
				</div>
				<div class="swiper-button-next"></div>
				<div class="swiper-button-prev"></div>
			</div>
		</div>
	</div>

	<div class="pop photos_download">
		<div class="photos_pop_head">
			<div class="name"></div>
			<div class="time_stamp">
				<span class="date"></span>
				<span class="time"></span>
			</div>
			<div class="right_box">
				<span class="edit open_edit_pop">Redaguoti</span>
				<a href="#" class="download"></a>
				<div class="close_box"></div>
			</div>
		</div>
		<div class="slider_box">
			<div class="swiper-container pop_photo_box_2">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="img_holder">
							<img src="../media/images/main_photo.jpg" alt="">
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						</div>
						<div class="name">Vilniaus „Lietuvos rytas“ Lorem ipsum Irmanto Gelūno nuotr.</div>
						<div class="time_stamp">
							<span class="date">2017-01-11</span>
							<span class="time">21:45</span>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_holder">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="simple_text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						</div>
						<div class="name">Vilniaus „Lietuvos rytas“ - Klaipėdos „Neptūnas“ / Irmanto Gelūno nuotr.</div>
						<div class="time_stamp">
							<span class="date">2017-03-21</span>
							<span class="time">21:45</span>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_holder">
							<img src="../media/images/main_photo.jpg" alt="">
						</div>
						<div class="simple_text">
							Lorem 
						</div>
						<div class="name">Vilniaus „Lietuvos rytas“ - Klaipėdos „Neptūnas“ / Irmanto Gelūno nuotr.</div>
						<div class="time_stamp">
							<span class="date">2017-03-21</span>
							<span class="time">21:45</span>
						</div>
					</div>
				</div>
				<div class="swiper-button-next"></div>
				<div class="swiper-button-prev"></div>
			</div>
		</div>
	</div>

	<!-- Šitas pats layout popupo yra naudojamas tiek naujienose, tiek įvykusioms konferencijoms atvaizduoti -->

	<div class="pop new">
		<div class="close"></div>
		<a href="#" class="edit top_button">Redaguoti</a>
		<a href="#" class="edit top_button">Versti</a>
		<div class="copy_text_pop" data-target="for_copy_pop">Kopijuoti tekstą</div>
		<div class="copy_holder_pop" id="for_copy_pop">
			<h2>Lietuvos ledo ritulininkės iš Latvijos parsivežė unikalią patirtį</h2>
			<div class="dates">
				<div class="date_line">Publikavimo data: <span class="date">2017-08-07</span><span class="time">11:41</span></div>
				<div class="date_line">Atnaujinimo data: <span class="date">2017-08-07</span><span class="time">11:41</span></div>
			</div>
			<div class="keywords_holder">
				<div class="key">Latvija</div>
				<div class="key">Ritulys</div>
				<div class="key">Brazauskas</div>
				<div class="key">Šuo</div>
			</div>
			<div class="heading">Ryga, kovo 12 d. (ELTA)</div>
			<div class="simple_text">
				<p>Savaitgalį dienomis Tukumse vyko trečiasis Latvijos moterų ledo ritulio čempionato turas. Paskutiniąsias ketverias čempionato rungtynes jame sužaidė ir lietuvių „Hockey Girls“ ekipa</p>
				<div>Trečiojoje išvykoje lietuvėms teko pripažinti labiau patyrusių ir ne pirmus metus besitreniruojančių kaimyninės šalies merginų pranašumą. Abejos rungtynės su „L&L/JLSS“ ekipą baigėsi rezultatais 0:5 ir 0:6, o kitos dienos rungtynės su „Laima Juniors“ baigėsi pralaimėjimais 0:3 bei 0:7.</div>
				Pirmą kartą istorijoje atvirame Latvijos moterų ledo ritulio čempionate dalyvaujanti „Hockey Girls“ ekipa iš viso sužaidė 12 rungtynių tarp keturių dalyvaujančių komandų užėmė paskutinę vietą.<br/><br/>
				„Į Latviją vykome nusiteikusios kovoti iki galo, tačiau įsitikinome, kad varžovės kur kas stipresnės ir labiau patyrusios. Atidavėme visas jėgas, bet to neužteko ir išsvajotų pergalių nepasiekėme. Vis dėlto manau, kad dalyvavimas Latvijos čempionate davė neįkainojamos patirties. Galimybė žaisti su stipresnėmis varžovėmis skatina tobulėti bei žengti pirmyn. Tikiuosi, kad tokią galimybę turėsime ir kitą sezoną, o po kelerių metų galėsime realizuoti svajones bei su moterų ledo ritulio rinktine dalyvauti pasaulio čempionate“, - sakė Lietuvos komandos Ramunė Maleckienė.	
			</div>
			<div class="author">Dominykas Genevičius (ELTA)</div>
			<div class="event_info">
				<div class="event_info_name">Renginio pradžia:</div>
				<div class="event_info_text">2018-06-14 (ketvirtadienis) 12:30</div>
			</div>
			<div class="event_info">
				<div class="event_info_name">Renginio pabaiga:</div>
				<div class="event_info_text">2018-06-14 (ketvirtadienis) 12:30</div>
			</div>
			<div class="event_info">
				<div class="event_info_name">Renginio vieta:</div>
				<div class="event_info_text">Nacionalinė dailės galerija</div>
			</div>
		</div>
		<div class="additionals">
			<div class="side_photo_holder">
				<img src="../media/images/hockey.jpg" alt="">
				<div class="buttons absolute">
					<a href="#" class="edit">Atsisiųsti</a>
					<div class="info extra_tooltip">
						<div class="tooltip_holder">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
				</div>
				<div class="photo_name">Nuotraukos pavadinimas</div>
			</div>
			<div class="clear"></div>
			<div class="side_photo_holder">
				<img src="../media/images/hockey.jpg" alt="">
				<div class="buttons absolute">
					<a href="#" class="edit">Atsisiųsti</a>
					<div class="info extra_tooltip">
						<div class="tooltip_holder">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
				</div>
				<div class="photo_name">Nuotraukos pavadinimas</div>
			</div>
			<div class="clear"></div>
			<div class="side_photo_holder">
				<img src="../media/images/hockey.jpg" alt="">
				<div class="buttons absolute">
					<a href="#" class="edit">Atsisiųsti</a>
					<div class="info extra_tooltip">
						<div class="tooltip_holder">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
				</div>
				<div class="photo_name">Nuotraukos pavadinimas</div>
			</div>
			<div class="clear"></div>
			<div class="files_area">
				<div class="file image">
					<a href="#" class="edit">Atsisiųsti</a>
					<div class="img_holder">
						<img src="../media/images/hockey.jpg" alt="">
					</div>
				</div>
				<div class="file image">
					<a href="#" class="edit">Atsisiųsti</a>
					<div class="img_holder">
						<img src="../media/images/hockey.jpg" alt="">
					</div>
				</div>
				<div class="file">
					<a href="#" class="edit">Atsisiųsti</a>
					<div class="file_holder">example.xlsx</div>
				</div>
			</div>
			<div class="warning">Dėmesio! Už šią informaciją ELTA neatsako. Už tai atsako ją publikavęs klientas.</div>
			<div class="button fb"><span>Dalintis</span></div>
		</div>
	</div>

	<div class="pop history">
		<div class="close"></div>
		<div class="row head_row">
			<div class="col">Laikas</div>
			<div class="col">Vartotojas</div>
			<div class="col">Veiksmas</div>
			<div class="col">IP adresas</div>
		</div>
		<div class="row">
			<div class="col">2018-03-23&nbsp;&nbsp;03:12</div>
			<div class="col">Vardenis Pavardenis</div>
			<div class="col">Vartotojo Petras Petraitis priskyrimas</div>
			<div class="col">8524455644</div>
		</div>
		<div class="row">
			<div class="col">2018-03-23&nbsp;&nbsp;03:12</div>
			<div class="col">Vardenis Pavardenis</div>
			<div class="col">Vartotojo Petras Petraitis priskyrimas</div>
			<div class="col">8524455644</div>
		</div>
		<div class="row">
			<div class="col">2018-03-23&nbsp;&nbsp;03:12</div>
			<div class="col">Vardenis Pavardenis</div>
			<div class="col">Vartotojo Petras Petraitis priskyrimas</div>
			<div class="col">8524455644</div>
		</div>
		<div class="row">
			<div class="col">2018-03-23&nbsp;&nbsp;03:12</div>
			<div class="col">Vardenis Pavardenis</div>
			<div class="col">Vartotojo Petras Petraitis priskyrimas</div>
			<div class="col">8524455644</div>
		</div>
		<div class="row">
			<div class="col">2018-03-23&nbsp;&nbsp;03:12</div>
			<div class="col">Vardenis Pavardenis</div>
			<div class="col">Vartotojo Petras Petraitis priskyrimas</div>
			<div class="col">8524455644</div>
		</div>
	</div>

	<div class="pop add_manager">
		<div class="close"></div>
		<div class="label">Skyrius</div>
		<div class="simple_dropdown bigger">
			<input type="text" name="section" disabled="" placeholder="Pasirinkite skyrių" value="">
			<div class="content">
				<div class="scroller_holder">
					<div class="option">Visos naujienos</div>
					<div class="option">Politika</div>
					<div class="option">Ekonomika</div>
					<div class="option">Teisėtvarka</div>
					<div class="option">Soprtas</div>
				</div>
			</div>
		</div>
		<div class="label">Darbuotojas</div>
		<div class="autocomplete_dropdown bigger not_active">
			<div class="planks">
				<div class="plank"></div>
				<div class="plank"></div>
				<div class="plank"></div>
			</div>
			<select class="for_desktop" name="employee" data-placeholder="Pasirinkite darbuotoją" tabindex="1">
				<option value=""></option>
				<option value="Visos naujienos">Visos naujienos</option>
				<option value="Politika">Politika</option>
				<option value="Soprtas">Soprtas</option>
				<option value="Visos naujienos">Visos naujienos</option>
				<option value="Politika">Politika</option>
				<option value="Soprtas">Soprtas</option>
				<option value="Visos naujienos">Visos naujienos</option>
				<option value="Politika">Politika</option>
				<option value="Soprtas">Soprtas</option>
			</select>
			<!-- Cia dublikatas del mobile -->
			<select name="employee_2" class="for_mobile not_touched">
				<option value="" selected disabled>Pasirinkite darbuotoją</option>
				<option value="Visos naujienos">Visos naujienos</option>
				<option value="Politika">Politika</option>
				<option value="Soprtas">Soprtas</option>
				<option value="Visos naujienos">Visos naujienos</option>
				<option value="Politika">Politika</option>
				<option value="Soprtas">Soprtas</option>
				<option value="Visos naujienos">Visos naujienos</option>
				<option value="Politika">Politika</option>
				<option value="Soprtas">Soprtas</option>
			</select>
		</div>
		<div class="button blue">Patvirtinti</div>
		<div class="cancel">Atšaukti</div>
	</div>

	<div class="pop select_photo">
		<div class="close"></div>
		<section class="filter">
			<div class="search">
				<div class="flex_holder">
					<input type="text" class="search_main_input" placeholder="Įveskite, ko ieškote...">
					<label class="simple_checkbox">
						<input type="checkbox" name="local" checked="">
						<span class="name">
							<span>Lietuva</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="global" checked="">
						<span class="name">
							<span>Užsienis</span>
						</span>
					</label>
					<button class="icon_search"><span>Ieškoti</span></button>
				</div>
				<div class="search_info_warning">
					Paieškos rezultatuose pateikiama dviejų paskutinių savaičių informacija, norėdami ieškoti ilgesnio periodo naujienų naudokite detalią paiešką.
				</div>
				<div class="full_search_holder">
					<div class="left_search_side">
						<div class="simple_input icon_dates">
							<input type="text" name="dates" placeholder="Pasirinkite laikotarpį" value="">
						</div>
						<div class="simple_dropdown">
							<input type="text" name="users_group" disabled="" placeholder="Vartotojų grupė" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
					<div class="right_search_side">
						<div class="simple_dropdown">
							<input type="text" name="status" disabled="" placeholder="Būsena" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide_buttons">
				<div class="button underlined detail_search">Detali paieška</div>
				<div class="opened_filter_buttons">
					<div class="button underlined close_search">Suskleisti paiešką</div>
					<div class="clear"></div>
					<div class="button underlined clear_filters">Išvalyti filtrą</div>
				</div>
			</div>
		</section>
		<div class="added_photos">
			<div class="photo_block">
				<div class="photo_holder">
					<div class="checked"></div>
					<div class="buttons">
						<div class="info extra_tooltip">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
					<div class="img_holder">
						<img src="../media/images/thumb_photo_1.jpg" alt="">
					</div>
				</div>
				<div class="name">S. Skvernelis / Irmanto Gelūno nuotr.</div>
			</div>
			<div class="photo_block">
				<div class="photo_holder">
					<div class="checked"></div>
					<div class="buttons">
						<div class="info extra_tooltip">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
					<div class="img_holder">
						<img src="../media/images/thumb_photo_2.jpg" alt="">
					</div>
				</div>
				<div class="name">R. Karbauskis / Irmanto Gelūno nuotr.</div>
			</div>
			<div class="photo_block">
				<div class="photo_holder">
					<div class="checked"></div>
					<div class="buttons">
						<div class="info extra_tooltip">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
					<div class="img_holder">
						<img src="../media/images/thumb_photo_1.jpg" alt="">
					</div>
				</div>
				<div class="name">S. Skvernelis / Irmanto Gelūno nuotr.</div>
			</div>
			<div class="photo_block">
				<div class="photo_holder">
					<div class="checked"></div>
					<div class="buttons">
						<div class="info extra_tooltip">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
					<div class="img_holder">
						<img src="../media/images/thumb_photo_2.jpg" alt="">
					</div>
				</div>
				<div class="name">R. Karbauskis / Irmanto Gelūno nuotr.</div>
			</div>
			<div class="photo_block">
				<div class="photo_holder">
					<div class="checked"></div>
					<div class="buttons">
						<div class="info extra_tooltip">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
					<div class="img_holder">
						<img src="../media/images/thumb_photo_1.jpg" alt="">
					</div>
				</div>
				<div class="name">S. Skvernelis / Irmanto Gelūno nuotr.</div>
			</div>
			<div class="photo_block">
				<div class="photo_holder">
					<div class="checked"></div>
					<div class="buttons">
						<div class="info extra_tooltip">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
					<div class="img_holder">
						<img src="../media/images/thumb_photo_2.jpg" alt="">
					</div>
				</div>
				<div class="name">R. Karbauskis / Irmanto Gelūno nuotr.</div>
			</div>
			<div class="photo_block">
				<div class="photo_holder">
					<div class="checked"></div>
					<div class="buttons">
						<div class="info extra_tooltip">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
					<div class="img_holder">
						<img src="../media/images/thumb_photo_1.jpg" alt="">
					</div>
				</div>
				<div class="name">S. Skvernelis / Irmanto Gelūno nuotr.</div>
			</div>
			<div class="photo_block">
				<div class="photo_holder">
					<div class="checked"></div>
					<div class="buttons">
						<div class="info extra_tooltip">
							<div class="tooltip">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
					</div>
					<div class="img_holder">
						<img src="../media/images/thumb_photo_2.jpg" alt="">
					</div>
				</div>
				<div class="name">R. Karbauskis / Irmanto Gelūno nuotr.</div>
			</div>
		</div>
		<div class="simple_pager">
			<a href="#" class="page_link">1</a>
			<a href="#" class="page_link current">2</a>
			<a href="#" class="page_link">3</a>
			<span>...</span>
			<a href="#" class="page_link">7</a>
		</div>
		<div class="line"></div>
		<div class="functional_buttons">
			<div class="button white close_button">Uždaryti</div>
			<div class="button blue">Pridėti</div>
		</div>
	</div>

	<div class="pop confirm">
		<div class="close"></div>
		<div class="heading">Ar tikrai norite trinti?</div>
		<div class="functional_buttons">
			<div class="button white">Taip</div>
			<div class="button blue">Ne</div>
		</div>
	</div>

	<div class="pop confirm_download">
		<div class="close"></div>
		<div class="heading">Ar tikrai norite parsisiųsti šią nuotrauką?</div>
		<div class="simple_text">Liko parsisiųsti 74 vnt.</div>
		<div class="functional_buttons">
			<div class="button white">Taip</div>
			<div class="button blue">Ne</div>
		</div>
	</div>

	<div class="pop one_photo">
		<div class="photos_pop_head">
			<div class="right_box">
				<div class="close_box"></div>
			</div>
		</div>
		<div class="slider_box">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="img_holder">
							<img src="../media/images/main_photo.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="pop one_video">
		<div class="photos_pop_head">
			<div class="right_box">
				<div class="close_box"></div>
			</div>
		</div>
		<div class="slider_box">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">

<!-- 						Atkreipti dėmesį į keistą struktūrą - iframe linką statom ne į src, o holderio data, čia yra hackas, kuris leidžia trigerint 'play', nes kitaip tam reikėtų arba paspaust atskirai darsyk play arba naudoti youtube api, kas yra vargas -->

						<div class="img_holder" data-iframe="https://www.youtube.com/embed/toYULkssPZ8?autoplay=1">
							<iframe preload="none" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="pop info">
		<div class="close"></div>
		<div class="heading">Atsiprašome, šiuo metu pranešimas redaguojamas</div>
		<div class="comment">Redaguoja <span class="blue">Jonas Jonaitis</span></div>
	</div>

	<div class="pop add_news">
		<div class="close"></div>
		<div class="heading">Žinių sąrašas</div>
		<section class="filter">
			<div class="search">
				<input type="text" class="search_main_input" placeholder="Įveskite, ko ieškote...">
				<button class="icon_search"><span>Ieškoti</span></button>
				<div class="search_info_warning">
					Paieškos rezultatuose pateikiama dviejų paskutinių savaičių informacija, norėdami ieškoti ilgesnio periodo naujienų naudokite detalią paiešką.
				</div>
				<div class="full_search_holder">
					<div class="left_search_side">
						<div class="simple_input icon_dates">
							<input type="text" name="dates" placeholder="Pasirinkite laikotarpį" value="">
						</div>
						<div class="simple_dropdown">
							<input type="text" name="users_group" disabled="" placeholder="Vartotojų grupė" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
					<div class="right_search_side">
						<div class="simple_dropdown">
							<input type="text" name="status" disabled="" placeholder="Būsena" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide_buttons">
				<div class="button underlined detail_search">Detali paieška</div>
				<div class="opened_filter_buttons">
					<div class="button underlined close_search">Suskleisti paiešką</div>
					<div class="clear"></div>
					<div class="button underlined clear_filters">Išvalyti filtrą</div>
				</div>
			</div>
		</section>
		<section>
			<div class="news_holder popup_news_list">
				<div class="scroller_holder">
					<div class="new" data-index="0">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="1">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="2">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="3">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="4">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="5">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="6">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="7">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="8">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="9">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="10">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="11">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="12">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="13">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="14">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="15">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="16">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="17">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="18">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="19">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="20">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="21">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="22">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
					</div>
					<div class="new" data-index="23">
						<div class="time_stamp">
							<span>
								<span class="time">21:45</span>
								<span class="date">2017-03-21</span>
							</span>
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
					</div>
					<div class="show_more_button">Rodyti daugiau</div>
				</div>
			</div>
			<div class="text_container">
				<div class="scroller_holder">
					<h2>Lietuvos ledo ritulininkės iš Latvijos parsivežė unikalią patirtį</h2>
					<div class="dates">
						<div class="date_line">Publikavimo data: <span class="date">2017-08-07</span><span class="time">11:41</span></div>
						<div class="date_line">Atnaujinimo data: <span class="date">2017-08-07</span><span class="time">11:41</span></div>
					</div>
					<div class="heading">Ryga, kovo 12 d. (ELTA)</div>
					<div class="simple_text">
						Savaitgalį dienomis Tukumse vyko trečiasis Latvijos moterų ledo ritulio čempionato turas. Paskutiniąsias ketverias čempionato rungtynes jame sužaidė ir lietuvių „Hockey Girls“ ekipa<br/><br/>
						Trečiojoje išvykoje lietuvėms teko pripažinti labiau patyrusių ir ne pirmus metus besitreniruojančių kaimyninės šalies merginų pranašumą. Abejos rungtynės su „L&L/JLSS“ ekipą baigėsi rezultatais 0:5 ir 0:6, o kitos dienos rungtynės su „Laima Juniors“ baigėsi pralaimėjimais 0:3 bei 0:7.<br/><br/>
						Pirmą kartą istorijoje atvirame Latvijos moterų ledo ritulio čempionate dalyvaujanti „Hockey Girls“ ekipa iš viso sužaidė 12 rungtynių tarp keturių dalyvaujančių komandų užėmė paskutinę vietą.<br/><br/>
						„Į Latviją vykome nusiteikusios kovoti iki galo, tačiau įsitikinome, kad varžovės kur kas stipresnės ir labiau patyrusios. Atidavėme visas jėgas, bet to neužteko ir išsvajotų pergalių nepasiekėme. Vis dėlto manau, kad dalyvavimas Latvijos čempionate davė neįkainojamos patirties. Galimybė žaisti su stipresnėmis varžovėmis skatina tobulėti bei žengti pirmyn. Tikiuosi, kad tokią galimybę turėsime ir kitą sezoną, o po kelerių metų galėsime realizuoti svajones bei su moterų ledo ritulio rinktine dalyvauti pasaulio čempionate“, - sakė Lietuvos komandos Ramunė Maleckienė.	
					</div>
					<div class="author">Dominykas Genevičius (ELTA)</div>
					<div class="additionals">
						<div class="side_photo_holder">
							<img src="../media/images/hockey.jpg" alt="">
							<div class="buttons absolute">
								<a href="#" class="edit">Atsisiųsti</a>
								<div class="info extra_tooltip">
									<div class="tooltip_holder">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
							<div class="photo_name">Nuotraukos pavadinimas</div>
						</div>
						<div class="clear"></div>
						<div class="side_photo_holder">
							<img src="../media/images/hockey.jpg" alt="">
							<div class="buttons absolute">
								<a href="#" class="edit">Atsisiųsti</a>
								<div class="info extra_tooltip">
									<div class="tooltip_holder">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
							<div class="photo_name">Nuotraukos pavadinimas</div>
						</div>
						<div class="clear"></div>
						<div class="side_photo_holder">
							<img src="../media/images/hockey.jpg" alt="">
							<div class="buttons absolute">
								<a href="#" class="edit">Atsisiųsti</a>
								<div class="info extra_tooltip">
									<div class="tooltip_holder">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
								</div>
							</div>
							<div class="photo_name">Nuotraukos pavadinimas</div>
						</div>
						<div class="clear"></div>
						<div class="files_area">
							<div class="file image">
								<a href="#" class="edit">Atsisiųsti</a>
								<div class="img_holder">
									<img src="../media/images/hockey.jpg" alt="">
								</div>
							</div>
							<div class="file image">
								<a href="#" class="edit">Atsisiųsti</a>
								<div class="img_holder">
									<img src="../media/images/hockey.jpg" alt="">
								</div>
							</div>
							<div class="file">
								<a href="#" class="edit">Atsisiųsti</a>
								<div class="file_holder">example.xlsx</div>
							</div>
						</div>
						<div class="warning">Dėmesio! Už šią informaciją ELTA neatsako. Už tai atsako ją publikavęs klientas.</div>
						<div class="button fb"><span>Dalintis</span></div>
					</div>
				</div>
			</div>
		</section>
		<div class="buttons_block">
			<div class="button blue">Patvirtinti</div>
			<div class="clear"></div>
			<div class="cancel_button">Atšaukti</div>
		</div>
	</div>

	<div class="pop edit_photo_photographer_upload">
		<div class="close"></div>
		<div class="added_photos">
			<div class="photo_block">
				<div class="photo_holder">
					<div class="img_holder">
						<img src="../media/images/thumb_photo_1.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="form">
			<div class="simple_input">
				<div class="label">Pavadinimas</div>
				<input type="text" spellcheck="true" name="name">
			</div>
			<div class="line"></div>
			<div class="simple_textarea">
				<textarea name="comments" placeholder="Komentaras"></textarea>
			</div>
			<div class="line"></div>
			<div class="row full">
				<div class="simple_input">
					<div class="label">Raktažodžiai</div>
					<input type="text" spellcheck="true" name="keywords" placeholder="Pvz. elta, seimas, vyriausybe, skvernelis, karbauskis">
				</div>
			</div>
			<div class="line"></div>
			<div class="label">Dienos nuotrauka</div>
			<label class="simple_checkbox">
				<input type="checkbox" name="day_photo">
				<span class="name">
					<span>Dienos nuotrauka</span>
				</span>
			</label>
			<div class="simple_input inline icon_dates">
				<input class="datepicker" type="text" name="photo_of_the_day_date">
			</div>
			<div class="line"></div>
			<div class="form_buttons">
				<div class="error_message">Something went wrong!</div>
				<button class="button white">Atšaukti</button>
				<button class="button blue">Patvirtinti</button>
			</div>
		</div>
	</div>

	<div class="pop edit_news_photo">
		<div class="close"></div>
		<div class="added_photos">
			<div class="photo_block">
				<div class="photo_holder">
					<div class="img_holder">
						<img src="" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="form">
			<div class="simple_input">
				<div class="label">Pavadinimas</div>
				<input type="text" spellcheck="true" name="name">
			</div>
			<div class="simple_textarea">
				<textarea name="comments" placeholder="Komentaras"></textarea>
			</div>
			<div class="form_buttons">
				<div class="error_message">Something went wrong!</div>
				<button class="button white">Atšaukti</button>
				<button class="button blue">Patvirtinti</button>
			</div>
		</div>
	</div>

	<div class="pop edit_photo">
		<div class="close"></div>
		<div class="added_photos">
			<div class="photo_block">
				<div class="photo_holder">
					<div class="img_holder">
						<img src="../media/images/thumb_photo_1.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
		<form>
			<div class="simple_input">
				<div class="label">Pavadinimas</div>
				<input type="text" spellcheck="true" name="name">
			</div>
			<div class="line"></div>
			<div class="row">
				<div class="col">
					<div class="label">Tema</div>
					<div class="simple_dropdown bigger">
						<input type="text" name="topic" disabled="" placeholder="Pasirinkite temą" value="">
						<div class="content">
							<div class="scroller_holder">
								<div class="option">Visos naujienos</div>
								<div class="option">Politika</div>
								<div class="option">Ekonomika</div>
								<div class="option">Teisėtvarka</div>
								<div class="option">Soprtas</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="label">Potemė</div>
					<div class="simple_dropdown bigger">
						<input type="text" name="subtopic" disabled="" placeholder="Pasirinkite potemę" value="">
						<div class="content">
							<div class="scroller_holder">
								<div class="option">Visos naujienos</div>
								<div class="option">Politika</div>
								<div class="option">Ekonomika</div>
								<div class="option">Teisėtvarka</div>
								<div class="option">Soprtas</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="add_another">Pridėti</div>
			<div class="line"></div>
			<div class="simple_textarea">
				<textarea name="comments" placeholder="Komentaras"></textarea>
			</div>
			<div class="line"></div>
			<div class="row">
				<div class="col">
					<div class="label">Autorius</div>
					<div class="autocomplete_dropdown">
						<div class="planks">
							<div class="plank"></div>
							<div class="plank"></div>
							<div class="plank"></div>
						</div>
						<select class="for_desktop" name="author" data-placeholder="Pasirinkite autorių" tabindex="1">
							<option value=""></option>
							<option value="Visos naujienos">Visos naujienos</option>
							<option value="Politika">Politika</option>
							<option value="Soprtas">Soprtas</option>
							<option value="Visos naujienos">Visos naujienos</option>
							<option value="Politika">Politika</option>
							<option value="Soprtas">Soprtas</option>
							<option value="Visos naujienos">Visos naujienos</option>
							<option value="Politika">Politika</option>
							<option value="Soprtas">Soprtas</option>
						</select>
						<!-- Cia dublikatas del mobile -->
						<select name="author_2" class="for_mobile not_touched">
							<option value="Visos naujienos">Visos naujienos</option>
							<option value="Politika">Politika</option>
							<option value="Soprtas">Soprtas</option>
							<option value="Visos naujienos">Visos naujienos</option>
							<option value="Politika">Politika</option>
							<option value="Soprtas">Soprtas</option>
							<option value="Visos naujienos">Visos naujienos</option>
							<option value="Politika">Politika</option>
							<option value="Soprtas">Soprtas</option>
						</select>
					</div>
				</div>
			</div>
			<div class="line"></div>
			<div class="row full">
				<div class="simple_input">
					<div class="label">Raktažodžiai</div>
					<input type="text" spellcheck="true" name="keywords" placeholder="Pvz. elta, seimas, vyriausybe, skvernelis, karbauskis">
				</div>
			</div>
			<div class="line"></div>
			<div class="label">Dienos nuotrauka</div>
			<label class="simple_checkbox">
				<input type="checkbox" name="day_photo">
				<span class="name">
					<span>Dienos nuotrauka</span>
				</span>
			</label>
			<div class="simple_input inline icon_dates">
				<input class="datepicker" type="text" name="photo_of_the_day_date">
			</div>
			<div class="line"></div>
			<div class="form_buttons">
				<div class="error_message">Something went wrong!</div>
				<div class="delete">Trinti</div>
				<button class="button blue" type="submit">Išsaugoti</button>
			</div>
		</form>
	</div>

</div>