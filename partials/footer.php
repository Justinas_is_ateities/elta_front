				
				<footer>
					<div class="copy">© 2016, Lietuvos naujienų agentūra ELTA. Visos teisės saugomos. Platinti kopijuoti be Eltos sutikimo draudžiama.</div>
				</footer>

			</div>
		</main>

		<?php include '../partials/popups.php';?>
		
		<!-- Rodom, tik jei žmogus dar nėra sutikęs su cookiais anksčiau -->

		<div class="cookies_pop">
			<div class="wrapper">
				<div class="privacy_text">
					Vykdydami ES direktyvos dėl privatumo ir elektroninių ryšių reikalavimus, pranešame, jog svetainėje naudojame slapukus norėdami suasmeninti turinį ir skelbimus, 
					kad teiktume visuomeninės medijos funkcijas ir galėtume analizuoti savo srautą. Informaciją apie tai, kaip naudojatės svetaine, bendriname su visuomeninės medijos, 
					reklamavimo ir analizės partneriais.
					<div class="button_holder">
						<div class="button blue agree">Sutinku</div>
					</div>
				</div>
			</div>
		</div>

		<!-- ... -->

		<div id="loader">
			<div class="center">
				<img src="../media/images/loader.gif" alt="">
				<span>Kraunasi, prašome palaukti</span>
			</div>
		</div>

	<script type="text/javascript" src="../js/libs.js"></script>
    <script type="text/javascript" src="../js/script.js"></script>



<!-- 		Auselė fronto peržiūrai - ištrinti! -->

<style type="text/css">
			#mmm {
				position: fixed;
				width: 260px;
				right: -260px;
				top: 0;
				background: #0c0c0c;
				z-index: 200;
				padding: 15px 15px 15px 30px;
				top: 0;
				height: 100%;
				box-sizing: border-box;
			}
			#mmm:hover {
				right:0;
			}
			#iii {
				overflow-y: scroll;
				position: absolute;
				width: 100%;
				top: 30px;
				height: 85%;
			}
			#mmm a {
				text-decoration: none;
				color: #fff;
				font-size: 14px;
				display: block;
				color: #666;
				margin: 5px 0;
				transition: color 100ms ease-in-out;
			}
			#mmm a:hover {
				color: #fff;
			}
			#ooo {
				width: 30px;
				height: 29px;
				position: absolute;
				top: 0;
				left: -30px;
				background: #0c0c0c;
				border-bottom-left-radius: 2px;
				cursor: pointer;
			}
			#ooo:before {
				content: "";
				position: absolute;
				right: 7px;
				display: block;
				width: 16px;
				top: -1px;
				height: 0;
				box-shadow: 0 10px 0 1px #fff,0 16px 0 1px #fff,0 22px 0 1px #fff;
			}
			.subb {
				opacity: .5;
				color: #fff;
				font-weight: bold;
				margin: 18px 0;
				font-size: 70%;
			}
		</style>
		<?php
		echo '<div id="mmm"><div id="ooo"></div><div id="iii">';
		$url = 'http://'.$_SERVER['HTTP_HOST'];
		echo '<div class="subb">LANDING</div>';
		$files=scandir(getcwd().'/../landing_templates');
		foreach($files as $k => $file)
		    if (strpos($file,'.php') !== false)
				if ($_SERVER['HTTP_HOST'] == 'localhost:82') {
		    		echo '<a href="'.$url.'/elta_front/landing_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	} else {
		    		echo '<a href="'.$url.'/landing_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	}
    	echo '<div class="subb">CLIENT</div>';
    	$files=scandir(getcwd().'/../client_templates');
		foreach($files as $k => $file)
		    if (strpos($file,'.php') !== false)
				if ($_SERVER['HTTP_HOST'] == 'localhost:82') {
		    		echo '<a href="'.$url.'/elta_front/client_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	} else {
		    		echo '<a href="'.$url.'/client_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	}
    	echo '<div class="subb">MANAGER</div>';
    	$files=scandir(getcwd().'/../manager_templates');
		foreach($files as $k => $file)
		    if (strpos($file,'.php') !== false)
				if ($_SERVER['HTTP_HOST'] == 'localhost:82') {
		    		echo '<a href="'.$url.'/elta_front/manager_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	} else {
		    		echo '<a href="'.$url.'/manager_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	}


		// echo '<div class="subb">KLIENTAI</div>';
		// $files=scandir(getcwd().'/../client_templates');
		// foreach($files as $k => $file)
		//     if (strpos($file,'.php') !== false)
		//         echo '<a href="/igehub-fe/client_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		// echo '<div class="subb">ANALITIKAI</div>';
		// $files=scandir(getcwd().'/../analytic_templates');
		// foreach($files as $k => $file)
		//     if (strpos($file,'.php') !== false)
		//         echo '<a href="/igehub-fe/analytic_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';

		?>

<!-- 		Auselės pabaiga -->



	</body>
</html>