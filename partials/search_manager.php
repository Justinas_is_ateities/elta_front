<div class="search">
	<input type="text" class="search_main_input" placeholder="Įveskite, ko ieškote...">
	<button class="icon_search"><span>Ieškoti</span></button>
	<div class="search_info_warning">
		Paieškos rezultatuose pateikiama dviejų paskutinių savaičių informacija, norėdami ieškoti ilgesnio periodo naujienų naudokite detalią paiešką.
	</div>
	<div class="full_search_holder">
		<div class="left_search_side">
			<div class="simple_input icon_dates">
				<input type="text" name="dates" placeholder="Pasirinkite laikotarpį" value="">
			</div>
			<div class="simple_dropdown">
				<input type="text" name="users_group" disabled="" placeholder="Vartotojų grupė" value="">
				<div class="content">
					<div class="scroller_holder">
						<div class="option">Visos naujienos</div>
						<div class="option">Politika</div>
						<div class="option">Ekonomika</div>
						<div class="option">Teisėtvarka</div>
						<div class="option">Soprtas</div>
					</div>
				</div>
			</div>
		</div>
		<div class="right_search_side">
			<div class="simple_dropdown">
				<input type="text" name="status" disabled="" placeholder="Būsena" value="">
				<div class="content">
					<div class="scroller_holder">
						<div class="option">Visos naujienos</div>
						<div class="option">Politika</div>
						<div class="option">Ekonomika</div>
						<div class="option">Teisėtvarka</div>
						<div class="option">Soprtas</div>
					</div>
				</div>
			</div>
		</div>
		<div class="buttons_for_mobile_filter">
			<button class="icon_search"><span>Ieškoti</span></button>
			<div class="button underlined clear_filters">Išvalyti filtrą</div>
		</div>
	</div>
</div>
