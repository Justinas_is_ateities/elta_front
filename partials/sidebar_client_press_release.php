<div class="toggler">
	<div class="toggler_head active" data-sector="all">
		<span>
			<span>Viskas</span>
		</span>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Politika</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Diplomatija</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Organizacijos</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Parlamentas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Prezidentas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Saugumas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Seimas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Valdymas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Vyriausybė</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Verslas</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Ekonomika</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Energetika</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Finansai</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Paslaugos</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Prekyba</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Ryšiai</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Statyba</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Transportas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Įmonės</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Žemė</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Susisiekimas</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Teisėtvarka</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Institucijos</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Nelaimės</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Nusikaltimai</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisingumas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Tvarkdara</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Kultūra</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Bažnyčia</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Medicina</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Menas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Mokslas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Pramogos</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Įstaigos</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Švietimas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Žiniasklaida</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Sportas</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Atletinis</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kamuolinis</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kovinis</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Struktūros</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Techninis</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Vandens</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Žaidimai</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Žiemos</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Sveikata</span>
		</span>
	</div>
	<div class="toggler_head" data-sector="calendar">
		<span>
			<span>Kalendorius</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Prezidentūra</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Seimas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Ministerijos</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Savivalda</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Organizacijos</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas (įvairiems verslo renginiams, kavinių pristatymams ir t.t.)</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Spaudos konferencijos</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Mano ruošiami</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kalendorius</span>
			</span>
		</div>
	</div>
	<div class="toggler_head" data-sector="">
		<span>
			<span>Mano publikuoti</span>
		</span>
		<div class="opener"><span></span></div>
	</div>
	<div class="toggler_body">
		<div class="subsector" data-subsector="">
			<span>
				<span>Politika</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Verslas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Teisėtvarka</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kultūra</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sportas</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Sveikata</span>
			</span>
		</div>
		<div class="subsector" data-subsector="">
			<span>
				<span>Kalendorius</span>
			</span>
		</div>
	</div>
</div>