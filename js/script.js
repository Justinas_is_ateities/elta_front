var header_white = false;
var pop_opened = null;
var calendar_mode = false;
var popup_debounce = false;
var opened_photo_edit_element;
var opened_news_edit_element;
var stop_jump = false;
var stop_y = 0;
var isMobile = false;

window.stopJump = function() {
  stop_y = $("main").scrollTop();
  stop_jump = true;
};

window.startJump = function() {
  stop_jump = false;
};

$("main").on("scroll", function() {
  if (stop_jump) {
    $(this).scrollTop(stop_y);
  }
});

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, "g"), replacement);
};

function CopyToClipboard(element) {
  var doc = document,
    text = doc.getElementById(element),
    range,
    selection;

  if (doc.body.createTextRange) {
    range = doc.body.createTextRange();
    range.moveToElementText(text);
    range.select();
    document.execCommand("copy");
    window.getSelection().removeAllRanges();
  } else if (window.getSelection) {
    selection = window.getSelection();
    range = doc.createRange();
    range.selectNodeContents(text);
    selection.removeAllRanges();
    selection.addRange(range);

    var editable = selection.toString();
    editable = editable.replaceAll("\n", "\n\n");
    var textarea = document.createElement("textarea");
    textarea.textContent = editable;
    textarea.style.position = "fixed";
    document.body.appendChild(textarea);
    textarea.select();
    try {
      return document.execCommand("copy");
    } catch (ex) {
      console.warn("Copy to clipboard failed.", ex);
      return false;
    } finally {
      window.getSelection().removeAllRanges();
      document.body.removeChild(textarea);
    }
  }
}

$(document).ready(function() {
  readyFunction();
});

function readyFunction() {
  globals();
  if ($(".title_page").is(":visible")) {
    titlePage();
  }
  if ($(".registration_page").is(":visible")) {
    registrationPage();
  }
  if ($(".subscribe_page").is(":visible")) {
    subscribePage();
  }
  if ($(".login_page").is(":visible")) {
    loginPage();
  }
  if ($(".text_page").is(":visible")) {
    textPage();
  }
  if ($(".services_page").is(":visible")) {
    servicesPage();
  }
  if ($(".password_page").is(":visible")) {
    passwordPage();
  }
  if ($(".expected_page ").is(":visible")) {
    expectedPage();
  }
  if ($(".manager_settings_page ").is(":visible")) {
    managerSettingsPage();
  }
  if ($(".redactor_page ").is(":visible")) {
    redactorPage();
  }
  if ($(".sidebar_layout").is(":visible")) {
    sidebarLayout();
  }
  if ($(".guide_page ").is(":visible")) {
    guidePage();
  }

  if ($("header .create_menu").is(":visible")) {
    createMenuScroll();
  }

  if ($(".photos_page").is(":visible") || $(".news_page").is(":visible")) {
    photosPage();
  }
  if ($(".past_conference_page ").is(":visible")) {
    pastConferencePage();
  }
  if ($(".recycle_bin").is(":visible")) {
    recycleBin();
  }
  adjustScrollbar();
}

function createMenuScroll() {
  var content = $("header .create_menu .content");
  var ps = new SimpleBar(
    document.querySelectorAll("header .create_menu .content")[0],
    { autoHide: false }
  );
  setTimeout(function() {
    var sum_height = 0;
    for (var i = 0, j = content.find("a").length; i < j; i++) {
      var link = content.find("a").eq(i);
      sum_height += link.outerHeight(true, true);
    }
    if (sum_height < 420) {
      $("header .create_menu").addClass("no_margin");
    }
  });
}

function recycleBin() {
  var right_buttons = $(".control_area .right_buttons");

  $(document).on("click", ".recycle_bin .photo", function() {
    if ($(this).hasClass("checked")) {
      $(this).removeClass("checked");
      $(this)
        .find('input[type="checkbox"]')
        .prop("checked", false);
      if ($('.recycle_bin .photo input[type="checkbox"]:checked').length < 1) {
        right_buttons.addClass("not_active");
      }
    } else {
      $(this).addClass("checked");
      $(this)
        .find('input[type="checkbox"]')
        .prop("checked", true);
      if ($('.recycle_bin .photo input[type="checkbox"]:checked').length == 1) {
        right_buttons.removeClass("not_active");
      }
    }
  });
  $(document).on("change", '.recycle_bin input[name="check_all"]', function() {
    if ($(this).is(":checked")) {
      $(".recycle_bin .photo").addClass("checked");
      $('.recycle_bin .photo input[type="checkbox"]').prop("checked", true);
      right_buttons.removeClass("not_active");
    } else {
      $('.recycle_bin .photo input[type="checkbox"]').prop("checked", false);
      right_buttons.addClass("not_active");
      $(".recycle_bin .photo").removeClass("checked");
    }
  });
}

function redactorPage() {
  var shift_pressed = false;
  $(document).on("click", ".popup_news_list .new", function() {
    if (shift_pressed) {
      var that = $(this);
      var current_index = that.attr("data-index");
      var list = $(".popup_news_list .new");
      var upper_new;
      for (var i = 0, j = list.length; i < j; i++) {
        var element = list.eq(i);
        var element_index = element.attr("data-index");
        if (element_index == current_index) {
          break;
        } else if (element.hasClass("selected")) {
          upper_new = element_index;
        }
      }
      that.addClass("selected");
      if (typeof upper_new != "undefined") {
        for (var k = parseInt(upper_new), l = list.length; k < l; k++) {
          var element = list.eq(k);
          var element_index = element.attr("data-index");
          if (element_index == current_index) {
            break;
          } else {
            element.addClass("selected");
          }
        }
      }
    } else {
      $(this).toggleClass("selected");
    }
  });
  $(document).on("keydown", function(e) {
    var which = e.which;
    if (which == 16) {
      if (shift_pressed) {
        return false;
      }
      shift_pressed = true;
    }
  });
  $(document).on("keyup", function(e) {
    var which = e.which;
    if (which == 16) {
      shift_pressed = false;
    }
  });
  var ps = new SimpleBar(document.querySelectorAll(".popup_news_list")[0], {
    autoHide: false
  });
  var ps_2 = new SimpleBar(
    document.querySelectorAll(".pop.add_news .text_container")[0],
    { autoHide: false }
  );
  $(".cancel_button").on("click", function() {
    $(".popup_news_list .new").removeClass("selected");
    closePop();
  });
  $(document).on("click", ".edit_news_photo.pop .button.white", function() {
    closePop();
  });
  $(document).on("click", ".edit_news_photo.pop .button.blue", function() {
    var context = $(opened_news_edit_element).closest(".photo_block");
    var name = $('.pop.edit_news_photo input[name="name"]').val();
    var comments = $('.pop.edit_news_photo textarea[name="comments"]').val();
    context.attr("data-comment", comments);
    context.find(".name").text(name);
    context.find(".tooltip").text(comments);
    closePop();
  });
}

function guidePage() {
  redactorPage();
  var add_photos_hiddeable = $(".add_photos_hiddeable");
  var button_space = $(".button_space");
  var tv_hiddeable = $(".tv_hiddeable");

  $(".option[data-tv]").on("click", function() {
    add_photos_hiddeable.slideUp(150);
    button_space.slideUp(150);
    tv_hiddeable.slideDown(150);
  });
  $(".option[data-multi]").on("click", function() {
    add_photos_hiddeable.slideDown(150);
    button_space.slideUp(150);
    tv_hiddeable.slideUp(150);
  });
  $(".theme_dropdown .option")
    .not(".option[data-tv], .option[data-multi]")
    .on("click", function() {
      add_photos_hiddeable.slideDown(150);
      button_space.slideDown(150);
      tv_hiddeable.slideUp(150);
    });
}

function servicesPage() {
  var shadows = $("*[data-shadow]");
  var page = $(".services_page");
  for (var i = 0, j = shadows.length; i < j; i++) {
    var shadow_element = shadows.eq(i);
    var height = shadow_element.innerHeight() + 280;
    var top = shadow_element.offset().top - 140;
    var html =
      '<div class="shadow" style="height: ' +
      height +
      "px; top: " +
      top +
      'px"></div>';
    page.prepend(html);
  }
}

function textPage() {
  if ($(".gallery-top").is(":visible")) {
    var galleryTop = new Swiper(".gallery-top", {
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      }
    });
    if ($(".gallery-thumbs .swiper-slide").length < 2) {
      $(".gallery-thumbs").remove();
      $(".gallery-top").css({ "margin-bottom": "40px" });
    } else {
      var galleryThumbs = new Swiper(".gallery-thumbs", {
        spaceBetween: 12,
        centeredSlides: true,
        slidesPerView: "auto",
        touchRatio: 0.2,
        slideToClickedSlide: true
      });
      galleryTop.controller.control = galleryThumbs;
      galleryThumbs.controller.control = galleryTop;
    }
  }
  if ($(".about_page").is(":visible")) {
    var shadow = $(".shadow");
    var start_element = $("*[data-shadow-start]");
    var finish_element = $("*[data-shadow-finish]");
    if (
      typeof start_element != "undefined" ||
      typeof finish_element != "undefined"
    ) {
      var start = start_element.offset().top + +start_element.innerHeight() / 2;
      var finish =
        finish_element.offset().top + finish_element.innerHeight() / 2;
      shadow.css({
        height: String(finish - start) + "px",
        top: String(start) + "px"
      });
    }
  }
  $("section.video .poster").on("click", function() {
    openPop("one_video");
  });
}

function pastConferencePage() {
  $(".copy_text_pop").hide();
  $(document).on("click", ".conference", function() {
    if (typeof eltaFront != "undefined") {
      eltaFront.initConferencePage(
        $(this).attr("data-id"),
        $(this).attr("data-slug")
      );
    }
    openPop("new");
  });

  $(document).on("click", ".edit_news_photo.pop .button.white", function() {
    closePop();
  });
  $(document).on("click", ".edit_news_photo.pop .button.blue", function() {
    var context = $(opened_news_edit_element).closest(".photo_block");
    var name = $('.pop.edit_news_photo input[name="name"]').val();
    var comments = $('.pop.edit_news_photo textarea[name="comments"]').val();
    context.attr("data-comment", comments);
    context.find(".name").text(name);
    context.find(".tooltip").text(comments);
    closePop();
  });
}

function reloader(scope) {
  if (typeof scope == "undefined") scope = "";
  (function() {
    var tags = $(scope + ".photo .badge, " + scope + ".new .badge");
    for (var i = 0, j = tags.length; i < j; i++) {
      var tag = tags.eq(i);
      var color = tag.attr("data-color");
      tag.css({ background: color });
    }
  })();
  (function() {
    var download_buttons = $(scope + ".side_photo_holder .edit");
    for (var i = 0, j = download_buttons.length; i < j; i++) {
      var button = download_buttons.eq(i);
      var href = button
        .closest(".side_photo_holder")
        .find("img")
        .attr("src");
      button.attr("download", "photo.jpg");
      button.attr("href", href);
    }
  })();

  var toolbarOptions = [
    ["bold", "italic", "underline"],
    ["blockquote"],
    [{ list: "ordered" }, { list: "bullet" }],
    [{ script: "sub" }, { script: "super" }],
    [{ indent: "-1" }, { indent: "+1" }],
    [{ size: ["small", false, "large", "huge"] }],
    [{ header: [1, 2, 3, 4, 5, 6, false] }],
    [{ align: [] }],
    ["link", "image", "video"],
    ["clean"]
  ];
  var options = {
    theme: "snow",
    modules: {
      toolbar: toolbarOptions
    }
  };
  // if ($('.editor_iframe').is(':visible')) {
  // 	var editor_iframe = $('.editor_iframe');
  // 	var iframe = editor_iframe.find('iframe');
  // 	iframe.on('load', function() {
  // 		var string = editor_iframe.find('input').val();
  // 		var inside = iframe.contents().find('body');
  // 		inside.attr('data-html', string);
  // 	});
  // }
  // $(scope + '.editor_iframe:visible').each(function($k, $elem) {
  // 	var editor_iframe = $(this);
  // 	var iframe = editor_iframe.find('iframe');
  // 	iframe.on('load', function() {
  //      		iframeGetData(iframe);
  // 	});
  // });
}

function iframeGetData($iframe) {
  // if (typeof $iframe.get(0).contentWindow.getData != 'undefined') {
  // 	var string = $iframe.closest('.editor_iframe').find('input').val();
  // 	var error = $iframe.closest('.editor_iframe').find('input').attr('data-oversize');
  // 	var texts = $iframe.closest('.editor_iframe').find('input').attr('data-texts');
  // 	var inside = $iframe.contents().find('body');
  // 	inside.attr('data-html', string);
  // 	inside.attr('data-oversize', error);
  // 	inside.attr('data-texts', texts);
  // 	$iframe.get(0).contentWindow.getData();
  // }
}

function photosPage() {
  var news_page = $(".news_page").is(":visible");
  var sidebar_layout = $(".sidebar_layout").is(":visible");

  function changeNewsView(real, node) {
    $(".filter .view").removeClass("current");
    node.addClass("current");
    if (node.attr("data-view") == "info") {
      if (localStorage && real) {
        localStorage.setItem("news_layout", "info");
      }
      $(".buttons").css({
        transition: "none"
      });
      $(".layout").removeClass("columns");
      $(".photos_holder .photo").removeClass("current");
      $(".side_pop").css({ display: "none" });
      if (typeof eltaBack != "undefined") eltaBack.setLayout(2);
    } else {
      if (localStorage && real) {
        localStorage.setItem("news_layout", "columns");
      }
      $(".layout").addClass("columns");
      $(".photos_holder .photo").removeClass("current");
      $(".photos_holder .photo")
        .eq(0)
        .addClass("current");
      $(".side_pop").css({ display: "inline-block" });
      setTimeout(function() {
        $(".buttons").css({
          transition: "transform 250ms, opacity 250ms"
        });
      }, 200);
      if (typeof eltaBack != "undefined") eltaBack.setLayout(1);
    }
  }
  function changePhotosView(real, node) {
    $(".filter .view").removeClass("current");
    node.addClass("current");
    if (node.attr("data-view") == "info") {
      if (localStorage && real) {
        localStorage.setItem("photos_layout", "info");
      }
      $(".buttons").css({
        transition: "none"
      });
      $(".layout").addClass("info");
      if (typeof eltaBack != "undefined") eltaBack.setLayout(2);
    } else {
      if (localStorage && real) {
        localStorage.setItem("photos_layout", "columns");
      }
      $(".layout").removeClass("info");
      if (typeof eltaBack != "undefined") eltaBack.setLayout(1);
      setTimeout(function() {
        $(".buttons").css({
          transition: "transform 250ms, opacity 250ms"
        });
      }, 200);
    }
  }

  if (news_page) {
    if (localStorage) {
      if (
        localStorage.getItem("news_layout") == "info" &&
        window.innerWidth > 1000
      ) {
        changeNewsView(false, $('.view[data-view="info"]'));
      }
    }
    $("main").addClass("hidden");
    $(".filter .view").on("click", function() {
      changeNewsView(true, $(this));
    });
    $(".photos_holder .photo")
      .eq(0)
      .addClass("current");
  } else {
    if (localStorage) {
      if (
        localStorage.getItem("photos_layout") == "info" &&
        window.innerWidth > 1000
      ) {
        changePhotosView(false, $('.view[data-view="info"]'));
      }
    }
    $(".filter .view").on("click", function() {
      changePhotosView(true, $(this));
    });
  }
  if (news_page) {
    if (window.innerWidth <= 1000) {
      changeNewsView(false, $('.view[data-view="info"]'));
    }
  } else {
    if (window.innerWidth < 768) {
      changePhotosView(false, $('.view[data-view="info"]'));
    } else if (window.innerWidth <= 1000) {
      changePhotosView(false, $('.view[data-view="columns"]'));
    }
  }

  $(".toggler_head > span").on("click", function() {
    var toggler_head = $(this).closest(".toggler_head");
    var all_toggler_heads = $(".toggler_head");
    var toggler_body = toggler_head.next(".toggler_body");
    var all_toggler_bodys = $(".toggler_body");
    var all_subsectors = $(".subsector");
    all_toggler_heads.removeClass("active");
    toggler_head.addClass("active");
    toggler_head.addClass("open");
    all_subsectors.removeClass("active");
    all_toggler_bodys
      .not(toggler_body)
      .stop(true, true)
      .slideUp(250);
    toggler_body.stop(true, true).slideDown(250);
    all_toggler_heads.not(toggler_head).removeClass("open");

    if (toggler_head.attr("data-sector") == "calendar") {
      calendar_mode = true;
      $("section.filter").addClass("calendar");
    } else {
      calendar_mode = false;
      $("section.filter").removeClass("calendar");
    }
  });
  $(".toggler_head .opener").on("click", function() {
    var toggler_head = $(this).closest(".toggler_head");
    var all_toggler_heads = $(".toggler_head");
    var toggler_body = toggler_head.next(".toggler_body");
    var all_toggler_bodys = $(".toggler_body");
    var all_subsectors = $(".subsector");
    if (toggler_head.hasClass("open")) {
      toggler_head.removeClass("open");
      toggler_body.stop(true, true).slideUp(250);
    } else {
      all_toggler_heads.removeClass("open");
      toggler_head.addClass("open");
      all_toggler_bodys
        .not(toggler_body)
        .stop(true, true)
        .slideUp(250);
      toggler_body.stop(true, true).slideDown(250);
    }
  });
  $(".toggler_body .subsector > span").on("click", function() {
    var toggler_head = $(this)
      .closest(".toggler_body")
      .prev(".toggler_head");
    var all_toggler_heads = $(".toggler_head");
    var toggler_body = $(this).closest(".toggler_body");
    var all_toggler_bodys = $(".toggler_body");
    var all_subsectors = $(".subsector");
    all_subsectors.removeClass("active");
    all_subsectors.removeClass("open");
    all_toggler_heads.removeClass("active");
    $(this)
      .closest(".subsector")
      .addClass("active");
    toggler_head.addClass("active");
    $(".sub_subsector ").removeClass("active");
    if (
      !$(this)
        .closest(".subsector")
        .hasClass("open")
    ) {
      $(".toggler_sub_body").slideUp(250);
    }
    if (toggler_head.attr("data-sector") == "calendar") {
      calendar_mode = true;
      $("section.filter").addClass("calendar");
    } else {
      calendar_mode = false;
      $("section.filter").removeClass("calendar");
    }
    $(".toggler_body .subsector").removeClass("open");
    $(this)
      .closest(".subsector")
      .addClass("open");
    $(".toggler_sub_body")
      .not(
        $(this)
          .closest(".subsector")
          .next(".toggler_sub_body")
      )
      .stop(true, true)
      .slideUp(250);
    $(this)
      .closest(".subsector")
      .next(".toggler_sub_body")
      .stop(true, true)
      .slideDown(250);
  });
  $(".toggler_body .subsector .sub_opener").on("click", function() {
    var toggler_head = $(this).closest(".subsector");
    var all_toggler_heads = $(".toggler_body .subsector");
    var toggler_body = toggler_head.next(".toggler_sub_body");
    var all_toggler_bodys = $(".toggler_sub_body");
    var all_subsectors = $(".subsector");
    if (toggler_head.hasClass("open")) {
      toggler_head.removeClass("open");
      toggler_body.stop(true, true).slideUp(250);
    } else {
      all_toggler_heads.removeClass("open");
      toggler_head.addClass("open");
      all_toggler_bodys
        .not(toggler_body)
        .stop(true, true)
        .slideUp(250);
      toggler_body.stop(true, true).slideDown(250);
    }
  });
  $(".toggler_body .sub_subsector").on("click", function() {
    var toggler_head = $(this)
      .closest(".toggler_sub_body")
      .prev(".subsector");
    var all_toggler_heads = $(".toggler_body .subsector");
    var toggler_body = $(this).closest(".toggler_sub_body");
    var all_toggler_bodys = $(".toggler_sub_body");
    var all_subsectors = $(".sub_subsector");

    $(".toggler_head").removeClass("active");
    all_subsectors.removeClass("active");
    all_toggler_heads.removeClass("active");
    $(this).addClass("active");
    toggler_head.addClass("active");
    $(this)
      .closest(".toggler_body")
      .prev(".toggler_head")
      .addClass("active");

    if (
      $(this)
        .closest(".toggler_body")
        .prev(".toggler_head")
        .attr("data-sector") == "calendar"
    ) {
      calendar_mode = true;
      $("section.filter").addClass("calendar");
    } else {
      calendar_mode = false;
      $("section.filter").removeClass("calendar");
    }
  });
  $(document).on("click", "section.photos .photo", function(e) {
    var target = $(e.target);
    if (target.hasClass("open_link")) {
      return true;
    }
    if (
      target.hasClass("tag") ||
      target.closest(".buttons").hasClass("buttons") ||
      target.closest(".functional_buttons").hasClass("functional_buttons")
    ) {
      return false;
    }
    if (!$(".recycle_bin").is(":visible")) {
      if (news_page && $(".layout").hasClass("columns")) {
        $(".photos_holder .photo").removeClass("current");
        var photo = target.closest(".photo");
        photo.addClass("current");
        photo.addClass("visited");
        if (typeof eltaFront != "undefined")
          eltaFront.initNewspostPage(
            photo.attr("data-id"),
            photo.attr("data-slug")
          );
      } else if (news_page) {
        var photo = target.closest(".photo").addClass("visited");
        if (typeof eltaFront != "undefined")
          eltaFront.initNewspostPage(
            photo.attr("data-id"),
            photo.attr("data-slug"),
            true
          );
        openPop("new", e.target);
      } else {
        if ($(".client_view").is(":visible")) {
          openPop("photos_download", e.target);
        } else {
          openPop("photos", e.target);
        }
      }
    }
  });
  if (news_page) {
    var news_holder = $(".right.layout.info");
    var new_change_debounce = false;
    var side_pop = $(".side_pop");
    $(document).on("keyup", function(e) {
      var side_pop_hovered = side_pop.is(":hover");
      if (side_pop_hovered) {
        return false;
      }
      if (e.which == 40) {
        if (!news_holder.hasClass("columns") || new_change_debounce) {
          return false;
        }
        new_change_debounce = true;
        setTimeout(function() {
          new_change_debounce = false;
        }, 1000);
        var current = $(".photos_holder .photo.current");
        if (typeof current.next() != "undefined") {
          if (current.next().hasClass("photo")) {
            current.removeClass("current");
            current.next().addClass("current");
            current.next().trigger("click");
          }
        }
      } else if (e.which == 38) {
        if (!news_holder.hasClass("columns") || new_change_debounce) {
          return false;
        }
        new_change_debounce = true;
        setTimeout(function() {
          new_change_debounce = false;
        }, 1000);
        var current = $(".photos_holder .photo.current");
        if (typeof current.prev() != "undefined") {
          if (current.prev().hasClass("photo")) {
            current.removeClass("current");
            current.prev().addClass("current");
            current.prev().trigger("click");
          }
        }
      }
    });
  }
  $('input[name="photo_of_the_day_date"]').daterangepicker(
    {
      singleDatePicker: true,
      locale: {
        format: "YYYY-MM-DD",
        separator: "  -  ",
        applyLabel: "Apply",
        cancelLabel: "Cancel",
        fromLabel: "From",
        toLabel: "To",
        customRangeLabel: "Custom",
        weekLabel: "W",
        daysOfWeek: ["S", "Pr", "A", "T", "K", "Pn", "Š"],
        monthNames: [
          "Sausis",
          "Vasaris",
          "Kovas",
          "Balandis",
          "Gegužė",
          "Birželis",
          "Liepa",
          "Rugpjūtis",
          "Rugsėjis",
          "Spalis",
          "Lapkritis",
          "Gruodis"
        ],
        firstDay: 1
      }
    },
    function(start, end, label) {}
  );
  $('input[name="calendar_date"]').daterangepicker(
    {
      singleDatePicker: true,
      locale: {
        format: "YYYY-MM-DD",
        separator: "  -  ",
        applyLabel: "Apply",
        cancelLabel: "Cancel",
        fromLabel: "From",
        toLabel: "To",
        customRangeLabel: "Custom",
        weekLabel: "W",
        daysOfWeek: ["S", "Pr", "A", "T", "K", "Pn", "Š"],
        monthNames: [
          "Sausis",
          "Vasaris",
          "Kovas",
          "Balandis",
          "Gegužė",
          "Birželis",
          "Liepa",
          "Rugpjūtis",
          "Rugsėjis",
          "Spalis",
          "Lapkritis",
          "Gruodis"
        ],
        firstDay: 1
      }
    },
    function(start, end, label) {}
  );
  $(".hidden_calendar .day_button").on("click", function() {
    $(".hidden_calendar .day_button")
      .not($(this))
      .removeClass("active");
    $(this).addClass("active");
    var picker = $('input[name="calendar_date"]').data("daterangepicker");
    if ($(this).hasClass("today")) {
      picker.setStartDate(moment().format("YYYY-MM-DD"));
      picker.setEndDate(moment().format("YYYY-MM-DD"));
    } else {
      picker.setStartDate(
        moment()
          .add(1, "days")
          .format("YYYY-MM-DD")
      );
      picker.setEndDate(
        moment()
          .add(1, "days")
          .format("YYYY-MM-DD")
      );
    }
    $(".filter .search button.icon_search").click();
  });
  $('input[name="calendar_date"]').on("apply.daterangepicker", function(
    ev,
    picker
  ) {
    var value = $(this).val();
    var now = moment().format("YYYY-MM-DD");
    var tomorrow = moment()
      .add(1, "days")
      .format("YYYY-MM-DD");

    if (value == now) {
      $(".hidden_calendar .day_button").removeClass("active");
      $(".hidden_calendar .day_button.today").addClass("active");
    } else if (value == tomorrow) {
      $(".hidden_calendar .day_button").removeClass("active");
      $(".hidden_calendar .day_button.tomorrow").addClass("active");
    } else {
      $(".hidden_calendar .day_button").removeClass("active");
    }
    $(".filter .search button.icon_search").click();
  });
  $('input[name="dates"]').daterangepicker({
    startDate: moment().subtract(14, "day"),
    endDate: moment(),
    opens: "right",
    locale: {
      format: "YYYY-MM-DD",
      separator: "  -  ",
      applyLabel: "Apply",
      cancelLabel: "Cancel",
      fromLabel: "From",
      toLabel: "To",
      customRangeLabel: "Custom",
      weekLabel: "W",
      daysOfWeek: ["S", "Pr", "A", "T", "K", "Pn", "Š"],
      monthNames: [
        "Sausis",
        "Vasaris",
        "Kovas",
        "Balandis",
        "Gegužė",
        "Birželis",
        "Liepa",
        "Rugpjūtis",
        "Rugsėjis",
        "Spalis",
        "Lapkritis",
        "Gruodis"
      ],
      firstDay: 1
    },
    autoApply: true
  });
  $(window).on("resize", function() {
    if (news_page) {
      if (window.innerWidth <= 1000) {
        changeNewsView(false, $('.view[data-view="info"]'));
      } else if (localStorage) {
        if (localStorage.getItem("news_layout") == "info") {
          changeNewsView(false, $('.view[data-view="info"]'));
        } else if (localStorage.getItem("news_layout") == "columns") {
          changeNewsView(false, $('.view[data-view="columns"]'));
        }
      }
    } else {
      if (window.innerWidth < 768) {
        changePhotosView(false, $('.view[data-view="info"]'));
      } else if (window.innerWidth <= 1000) {
        changePhotosView(false, $('.view[data-view="columns"]'));
      } else if (localStorage) {
        if (localStorage.getItem("photos_layout") == "info") {
          changePhotosView(false, $('.view[data-view="info"]'));
        } else if (localStorage.getItem("photos_layout") == "columns") {
          changePhotosView(false, $('.view[data-view="columns"]'));
        }
      }
    }
  });
  (function() {
    var debounce = false;
    $(".mobile_filter_toggler").on("click", function() {
      if (debounce) {
        return false;
      }
      debounce = true;
      setTimeout(function() {
        debounce = false;
      }, 300);
      $(this).toggleClass("open");
      $(".photos .left")
        .stop(true, true)
        .slideToggle(300);
    });
  })();
  $(document).on("click", ".buttons .info", function() {
    $(".buttons .info")
      .not($(this))
      .removeClass("open");
    $(this).toggleClass("open");
  });
  reloader();
  (function() {
    var scrolls = false;
    var ps;
    var side_ps;
    var left_ps;
    var list = $(".right.layout");
    var list_holder = $(".right.layout .photos_holder");
    var toggler = $(".left .toggler");
    var left = $(".section.photos .left");
    var side_pop = $(".side_pop");
    var left = $(".photos .left");
    var mobile_login = $(".mobile_login");
    var filter = $(".page section.filter");
    var footer = $("footer");
    var recycle_bin = $(".recycle_bin");
    var control_area = $(".control_area");

    function setListHeight() {
      var padding_top;
      if (mobile_login.is(":visible")) {
        padding_top = 73;
      } else {
        padding_top = 93;
      }
      var filter_height = filter.outerHeight(true, true);
      var footer_height = footer.outerHeight(true, true);
      if (typeof filter_height == "undefined") {
        filter_height = 0;
      }
      var sum_height = filter_height + footer_height + padding_top;
      left.css({ height: "calc(100vh - " + sum_height + "px)" });
      side_pop.css({ height: "calc(100vh - " + sum_height + "px)" });
      var control_area_height = 0;
      if (recycle_bin.is(":visible")) {
        control_area_height = control_area.outerHeight(true, true);
      }
      sum_height = control_area_height + sum_height;
      list_holder.css({ height: "calc(100vh - " + sum_height + "px)" });
    }

    try {
      if (news_page && list.hasClass("columns")) {
        setListHeight();
        ps = new SimpleBar(
          document.querySelectorAll(".right.layout .photos_holder")[0],
          { autoHide: false }
        );
        side_ps = new SimpleBar(document.querySelectorAll(".side_pop")[0], {
          autoHide: false
        });
        left_ps = new SimpleBar(document.querySelectorAll(".photos .left")[0], {
          autoHide: false
        });
        scrolls = true;
      } else if (news_page) {
        setListHeight();
        ps = new SimpleBar(
          document.querySelectorAll(".right.layout .photos_holder")[0],
          { autoHide: false }
        );
        left_ps = new SimpleBar(document.querySelectorAll(".photos .left")[0], {
          autoHide: false
        });
        scrolls = true;
      }
    } catch (err) {}

    $(document).on("click", ".view, .photo", function() {
      if (window.innerWidth > 1000) {
        if (news_page && list.hasClass("columns")) {
          setListHeight();
          side_ps = new SimpleBar(document.querySelectorAll(".side_pop")[0], {
            autoHide: false
          });
          scrolls = true;
        } else if (typeof side_ps != "undefined" && side_ps != null) {
          side_ps = null;
        }
      }
    });
    (function() {
      var detail_search = $(".page .detail_search");
      var search = $(".page .filter .search");
      var full_search_holder = $(".page .full_search_holder");
      var opened_filter_buttons = $(".page .opened_filter_buttons");
      var right_box = $(".right_box:not(.pop .right_box)");
      var right = $(".photos .right");
      var photos_holder = right.find(".photos_holder");
      var search_info_warning = $(".page .search_info_warning");
      var search_debounce = false;
      var pop_detail_search = $(".pop .detail_search");
      var pop_filter = $(".pop section.filter");
      var pop_search = $(".pop .filter .search");
      var pop_full_search_holder = $(".pop .full_search_holder");
      var pop_opened_filter_buttons = $(".pop .opened_filter_buttons");
      var pop_search_info_warning = $(".pop .search_info_warning");

      var detail_search_debounce = false;

      detail_search.on("click", function() {
        if (detail_search_debounce) {
          return false;
        }
        detail_search_debounce = true;
        setTimeout(function() {
          detail_search_debounce = false;
        }, 260);
        filter.removeClass("auto");
        search_info_warning.slideUp(250);
        detail_search.hide();
        right_box.hide();
        filter.addClass("full");
        resetScrollHeights();
        full_search_holder.slideDown(250);
        opened_filter_buttons.css({ display: "inline-block" });
        left.addClass("filter_opened");
        right.addClass("filter_opened");
        side_pop.addClass("filter_opened");
      });
      pop_detail_search.on("click", function() {
        pop_filter.removeClass("auto");
        pop_search_info_warning.slideUp(250);
        pop_detail_search.hide();
        pop_filter.addClass("full");
        pop_full_search_holder.slideDown(250);
        pop_opened_filter_buttons.css({ display: "inline-block" });
      });
      $(window).on("resize", function() {
        if (
          (filter.hasClass("auto") && scrolls) ||
          (filter.hasClass("full") && scrolls)
        ) {
          setTimeout(function() {
            resetScrollHeights();
          });
        }
      });
      function interactedScrollContainers() {
        if (pop_opened) {
          return false;
        }
        if (filter.hasClass("auto") || filter.hasClass("full")) {
          filter.removeClass("auto");
          left.removeClass("filter_opened");
          right.removeClass("filter_opened");
          side_pop.removeClass("filter_opened");
          opened_filter_buttons.hide();
          filter.removeClass("full");
          resetScrollHeights();
          full_search_holder.slideUp(250);
          search_info_warning.slideUp(250);
          right_box.show();
          detail_search.show();
        }
      }
      left.on("click", function() {
        interactedScrollContainers();
        $(".button.underlined.clear_filters").trigger("click");
      });
      right.on("click", function() {
        if (!pop_opened) {
          filter.removeClass("auto");
          left.removeClass("filter_opened");
          right.removeClass("filter_opened");
          side_pop.removeClass("filter_opened");
          opened_filter_buttons.hide();
          filter.removeClass("full");
          resetScrollHeights();
          full_search_holder.slideUp(250);
          search_info_warning.slideUp(250);
          right_box.show();
          detail_search.show();
        }
      });
      side_pop.on("click", function() {
        interactedScrollContainers();
      });
      photos_holder.on("scroll", function() {
        interactedScrollContainers();
      });
      side_pop.on("scroll", function() {
        interactedScrollContainers();
      });
      function resetScrollHeights() {
        if (pop_opened) {
          return false;
        }
        var extra_height = 0;
        if (filter.hasClass("auto")) {
          extra_height = search_info_warning.outerHeight(true, true);
        } else if (filter.hasClass("full")) {
          extra_height = full_search_holder.outerHeight(true, true);
        }
        filter.css({ height: String(extra_height + 36) + "px" });
        var padding_top;
        if (mobile_login.is(":visible")) {
          padding_top = 73;
        } else {
          padding_top = 93;
        }
        var footer_height = footer.outerHeight(true, true);
        var sum_height = 51 + footer_height + padding_top + extra_height;
        if (!$(".page.photos_page").is(":visible")) {
          left.css({ height: "calc(100vh - " + sum_height + "px)" });
          side_pop.css({ height: "calc(100vh - " + sum_height + "px)" });
        }
        var control_area_height = 0;
        if (recycle_bin.is(":visible")) {
          control_area_height = control_area.outerHeight(true, true);
        }
        sum_height = control_area_height + sum_height;
        if (!$(".page.photos_page").is(":visible")) {
          list_holder.css({ height: "calc(100vh - " + sum_height + "px)" });
        }
      }
      $(".clear_filters").on("click", function() {
        if (pop_opened) {
          var inputs = pop_search.find("input");
          if ($(".mobile_detail_filter.pop").hasClass("show")) {
            var inputs = $(".mobile_detail_filter.pop").find("input");
          }
          for (var i = 0, j = inputs.length; i < j; i++) {
            var input = inputs.eq(i);
            input.val("");
            input
              .closest(".simple_dropdown")
              .find(".current")
              .removeClass("current");
            input.prop("checked", true);
            if (input.data("daterangepicker")) {
              input
                .data("daterangepicker")
                .setStartDate(moment().subtract(14, "day"));
              input.data("daterangepicker").setEndDate(moment());
            }
          }
          $(
            ".autocomplete_dropdown select.for_desktop, .autocomplete_dropdown select.for_mobile"
          )
            .val("")
            .trigger("chosen:updated");
        } else {
          var inputs = search.find("input");
          for (var i = 0, j = inputs.length; i < j; i++) {
            var input = inputs.eq(i);
            input.val("");
            input
              .closest(".simple_dropdown")
              .find(".current")
              .removeClass("current");
            input.prop("checked", true);
            if (input.data("daterangepicker")) {
              input
                .data("daterangepicker")
                .setStartDate(moment().subtract(14, "day"));
              input.data("daterangepicker").setEndDate(moment());
            }
          }
          $(
            ".autocomplete_dropdown select.for_desktop, .autocomplete_dropdown select.for_mobile"
          )
            .val("")
            .trigger("chosen:updated");
        }
      });
      $(".close_search").on("click", function() {
        if (pop_opened) {
          pop_opened_filter_buttons.hide();
          pop_filter.removeClass("full");
          pop_full_search_holder.slideUp(250);
          pop_detail_search.show();
        } else {
          if (detail_search_debounce) {
            return false;
          }
          detail_search_debounce = true;
          setTimeout(function() {
            detail_search_debounce = false;
          }, 260);
          left.removeClass("filter_opened");
          right.removeClass("filter_opened");
          side_pop.removeClass("filter_opened");
          opened_filter_buttons.hide();
          filter.removeClass("full");
          resetScrollHeights();
          full_search_holder.slideUp(250);
          right_box.show();
          detail_search.show();
        }
      });
      $(".filter .search .search_main_input").on("blur", function() {
        if ($(this).val().length > 0) {
          $(this).addClass("focus");
        } else {
          $(this).removeClass("focus");
        }
      });
      function doSearch() {
        if (
          (isMobile &&
            $(".mobile_detail_filter .search_main_input").val().length < 1) ||
          (!isMobile &&
            $(".filter .search .search_main_input").val().length < 1) ||
          search_debounce
        )
          if (search_debounce) {
            return false;
          }
        search_debounce = true;
        setTimeout(function() {
          search_debounce = false;
        }, 100);

        if (pop_opened) {
          if (typeof eltaBack != "undefined") eltaBack.feedPopup(pop_opened);

          //        if (!pop_filter.hasClass('full')) {
          // 	pop_filter.addClass('auto');
          // 	pop_search_info_warning.slideDown(250);
          // }
        } else {
          if (typeof eltaFront != "undefined") {
            eltaFront.search();
          } else if (typeof eltaBack != "undefined") {
            eltaBack.search();
          }

          // if (!filter.hasClass('full')) {
          // 	filter.addClass('auto');
          // 	resetScrollHeights();
          // 	search_info_warning.slideDown(250);
          // }
        }
      }
      $(".filter .search button.icon_search").on("click", function() {
        doSearch();
      });
      $(".buttons_for_mobile_filter button.icon_search").on(
        "click",
        function() {
          closePop();
          doSearch();
        }
      );
      $(document).keydown(function(e) {
        if (e.which == 13) {
          doSearch();
        }
      });
    })();
  })();
  $(document).on("click", "a.download", function() {
    openPop("confirm_download", this);
  });
}

function managerSettingsPage() {
  $('input[name="export_dates"]').daterangepicker(
    {
      opens: "right",
      timePicker: true,
      timePicker24Hour: true,
      applyButtonClasses: "export_dates",
      locale: {
        format: "YYYY-MM-DD  HH:mm",
        separator: "  -  ",
        applyLabel: "Apply",
        cancelLabel: "Cancel",
        fromLabel: "From",
        toLabel: "To",
        customRangeLabel: "Custom",
        weekLabel: "W",
        daysOfWeek: ["S", "Pr", "A", "T", "K", "Pn", "Š"],
        monthNames: [
          "Sausis",
          "Vasaris",
          "Kovas",
          "Balandis",
          "Gegužė",
          "Birželis",
          "Liepa",
          "Rugpjūtis",
          "Rugsėjis",
          "Spalis",
          "Lapkritis",
          "Gruodis"
        ],
        firstDay: 1
      },
      autoApply: true
    },
    function(start, end, label) {
      console.log(start, end, label);
    }
  );
  $('input[name="subscription_dates"]').daterangepicker(
    {
      opens: "right",
      applyButtonClasses: "subscription_dates",
      locale: {
        format: "YYYY-MM-DD",
        separator: "  -  ",
        applyLabel: "Apply",
        cancelLabel: "Cancel",
        fromLabel: "From",
        toLabel: "To",
        customRangeLabel: "Custom",
        weekLabel: "W",
        daysOfWeek: ["S", "Pr", "A", "T", "K", "Pn", "Š"],
        monthNames: [
          "Sausis",
          "Vasaris",
          "Kovas",
          "Balandis",
          "Gegužė",
          "Birželis",
          "Liepa",
          "Rugpjūtis",
          "Rugsėjis",
          "Spalis",
          "Lapkritis",
          "Gruodis"
        ],
        firstDay: 1
      },
      autoApply: true
    },
    function(start, end, label) {}
  );
  if ($('input[name="subscription_dates"]').attr("data-empty") == "true") {
    $('input[name="subscription_dates"]').val("");
  }
    $('input[name="pressrelease_subscription_dates"]').daterangepicker(
        {
            opens: "right",
            applyButtonClasses: "pressrelease_subscription_dates",
            locale: {
                format: "YYYY-MM-DD",
                separator: "  -  ",
                applyLabel: "Apply",
                cancelLabel: "Cancel",
                fromLabel: "From",
                toLabel: "To",
                customRangeLabel: "Custom",
                weekLabel: "W",
                daysOfWeek: ["S", "Pr", "A", "T", "K", "Pn", "Š"],
                monthNames: [
                    "Sausis",
                    "Vasaris",
                    "Kovas",
                    "Balandis",
                    "Gegužė",
                    "Birželis",
                    "Liepa",
                    "Rugpjūtis",
                    "Rugsėjis",
                    "Spalis",
                    "Lapkritis",
                    "Gruodis"
                ],
                firstDay: 1
            },
            autoApply: true
        },
        function(start, end, label) {}
    );
    if ($('input[name="pressrelease_subscription_dates"]').attr("data-empty") == "true") {
        $('input[name="pressrelease_subscription_dates"]').val("");
    }
  $('input[name="archive_dates"]').daterangepicker(
    {
      opens: "right",
      applyButtonClasses: "archive_dates",
      locale: {
        format: "YYYY-MM-DD",
        separator: "  -  ",
        applyLabel: "Apply",
        cancelLabel: "Cancel",
        fromLabel: "From",
        toLabel: "To",
        customRangeLabel: "Custom",
        weekLabel: "W",
        daysOfWeek: ["S", "Pr", "A", "T", "K", "Pn", "Š"],
        monthNames: [
          "Sausis",
          "Vasaris",
          "Kovas",
          "Balandis",
          "Gegužė",
          "Birželis",
          "Liepa",
          "Rugpjūtis",
          "Rugsėjis",
          "Spalis",
          "Lapkritis",
          "Gruodis"
        ],
        firstDay: 1
      },
      autoApply: true
    },
    function(start, end, label) {}
  );
  if ($('input[name="archive_dates"]').attr("data-empty") == "true") {
    $('input[name="archive_dates"]').val("");
  }
  $(
    'input[name="export_dates"], input[name="subscription_dates"], input[name="archive_dates"]'
  ).on("keydown", function(e) {
    if (e.which == 8) {
      $(this).trigger("blur");
      $(".applyBtn." + String($(this).attr("name")))
        .closest(".daterangepicker")
        .css({ display: "none" });
      $(this).val("");
      $(this).attr("data-empty", "true");
    }
  });
  $(
    'input[name="export_dates"], input[name="subscription_dates"], input[name="archive_dates"]'
  ).on("change", function(e) {
    if ($(this).attr("data-empty") == "true") {
      $(this).val("");
    }
  });
  $(
    'input[name="export_dates"], input[name="subscription_dates"], input[name="archive_dates"]'
  ).on("focus", function(e) {
    if ($(this).attr("data-empty") == "true") {
      $(this).attr("data-empty", "false");
      $(".applyBtn." + String($(this).attr("name")))
        .closest(".daterangepicker")
        .css({ display: "block" });
    }
  });
  $(".add_another_removable").on("click", function() {
    var parent = $(this).closest(".hiddeable");
    if (parent.hasClass("ips")) {
      var new_name = "ip_" + String(parent.find("input").length + 1);
      var html =
        '<div class="simple_input removable"><div class="remove"></div><input type="text" name="' +
        new_name +
        '" placeholder="88.119.140.137"></div>';
      parent.find(".row").append(html);
    } else if (parent.hasClass("ips_ranges")) {
      var new_name = "ip_range_" + String(parent.find("input").length + 1);
      var html =
        '<div class="simple_input removable"><div class="remove"></div><input type="text" name="' +
        new_name +
        '" placeholder="192.168.0.1-192.168.120.255"></div>';
      parent.find(".row").append(html);
    } else if (parent.hasClass("subnet_ips")) {
      var new_name = "subnet_ip_" + String(parent.find("input").length + 1);
      var html =
        '<div class="simple_input removable"><div class="remove"></div><input type="text" name="' +
        new_name +
        '" placeholder="192.168.0.0/24"></div>';
      parent.find(".row").append(html);
    }
    refreshDropdowns();
  });
  $(document).on("click", ".simple_input.removable .remove", function(e) {
    $(this)
      .closest(".simple_input")
      .remove();
  });
  $('input[name="multiple_ip"]').on("change", function() {
    $(".hiddeable.ips").slideToggle(150);
  });
  $('input[name="show_pressrelease_in_news"]').on("change", function() {
    $(".show_press_releases_in_main_list_hiddeable").slideToggle(150);
  });
  $('input[name="subnet"]').on("change", function() {
    $(".hiddeable.subnet_ips").slideToggle(150);
  });
  $('input[name="export_news"]').on("change", function() {
    $(".export_hiddeable").slideToggle(150);
  });
  $('input[name="export_photos"]').on("change", function() {
    $(".export_photos_hiddeable").slideToggle(150);
  });
  $(".export_type .option").on("click", function() {
    if ($(this).attr("data-selection") == "ftp") {
      $(".ftp_hiddeable").slideDown(150);
      $(".email_hiddeable").slideUp(150);
      $(".format_code_hiddeable").slideUp(150);
    } else {
      $(".ftp_hiddeable").slideUp(150);
      $(".email_hiddeable").slideDown(150);
      if (
        $(".export_format_email .current").attr("data-selection") ==
        "email_plain"
      ) {
        $(".format_code_hiddeable").slideDown(150);
      } else {
        $(".format_code_hiddeable").slideUp(150);
      }
    }
  });
  $(".export_format_email .option").on("click", function() {
    if ($(this).attr("data-selection") == "email_plain") {
      $(".format_code_hiddeable").slideDown(150);
    } else {
      $(".format_code_hiddeable").slideUp(150);
    }
  });
}

function expectedPage() {
  var expandable = $(".expandable");
  var local_resize_debounce;
  function resetExpandables() {
    for (var k = 0, l = expandable.length; k < l; k++) {
      var one = expandable.eq(k).find(".inner");
      var height = one.innerHeight();
      if (height < 60) {
        one.closest(".publication_name").addClass("smaller");
      } else {
        one.closest(".publication_name").removeClass("smaller");
      }
    }
  }
  resetExpandables();
  $(window).on("resize", function() {
    clearTimeout(local_resize_debounce);
    local_resize_debounce = setTimeout(function() {
      resetExpandables();
    }, 300);
  });
  $(".expand").on("click", function() {
    $(this)
      .closest(".publication_name")
      .toggleClass("opened");
  });
  $('input[name="select_all"]').on("change", function() {
    var inner_inputs = $(this)
      .closest(".day")
      .find(".day_list input");
    var at_least_one_checked = false;
    var button = $(this)
      .closest(".day_controls")
      .find(".button");
    for (var i = 0, j = inner_inputs.length; i < j; i++) {
      var inner_input = inner_inputs.eq(i);
      if (inner_input.is(":checked")) {
        at_least_one_checked = true;
        break;
      }
    }
    if ($(this).is(":checked")) {
      button.slideDown(150);
      inner_inputs.prop("checked", true);
    } else {
      inner_inputs.prop("checked", false);
      button.slideUp(150);
    }
  });
  $(".day_list input").on("change", function() {
    var inner_inputs = $(this)
      .closest(".day")
      .find(".day_list input");
    var outer_input = $(this)
      .closest(".day")
      .find('input[name="select_all"]');
    var button = $(this)
      .closest(".day")
      .find(".day_controls .button");
    var at_least_one_checked = false;
    for (var i = 0, j = inner_inputs.length; i < j; i++) {
      var inner_input = inner_inputs.eq(i);
      if (inner_input.is(":checked")) {
        at_least_one_checked = true;
        break;
      }
    }
    if (!$(this).is(":checked")) {
      outer_input.prop("checked", false);
      if (!at_least_one_checked) {
        button.slideUp(150);
      }
    } else {
      button.slideDown(150);
    }
  });
}

function sidebarLayout() {
  photosPage();
  $(document).on("click", ".added_photos .photo_block .remove", function() {
    $(this)
      .closest(".photo_block")
      .remove();
  });
  $(".subheader .history").on("click", function() {
    openPop("history");
  });
  $(".subheader .add_manager").on("click", function() {
    openPop("add_manager");
  });
  $(".add_manager.pop .option").on("click", function() {
    $(".add_manager.pop .not_active").removeClass("not_active");
  });
  $(".add_more_news").on("click", function() {
    openPop("add_news");
  });
  $(".pop.add_manager .cancel").on("click", function() {
    var pop = $(this).closest(".pop");
    pop.find(".option").removeClass("current");
    pop.find("input").val("");
    $(".subheader .added_manager").hide();
    closePop();
  });
  $(".pop.add_manager .button.blue").on("click", function() {
    var employeeName = $(
      '.pop.add_manager .autocomplete_dropdown[data-type="employee"] select.' +
        (isMobile ? "for_mobile" : "for_desktop") +
        " option:selected"
    ).html();
    var departmentName = $(
      '.pop.add_manager [data-type="department"] input[type="text"]'
    ).val();
    var value = employeeName ? employeeName : departmentName;
    $(".subheader .added_manager span").text(value);
    if (value) $(".subheader .added_manager").slideDown(150);
    else $(".subheader .added_manager").hide();
    closePop();
  });
  $(document).on("click", ".pop .photo_block", function() {
    $(this).toggleClass("checked");
    if (typeof eltaBack != "undefined") eltaBack.selectPopupPhoto($(this));
  });
  $(".photo_holder.add_photo").on("click", function() {
    openPop("select_photo");
  });
  $(".close_button").on("click", function() {
    closePop();
    $(".photo_block.checked").removeClass("checked");
  });
  $(".form_buttons .delete").on("click", function() {
    openPop("confirm");
  });
  $(
    'input[name="postpone_date"], input[name="calendar_start_date"], input[name="calendar_finish_date"]'
  ).daterangepicker(
    {
      singleDatePicker: true,
      locale: {
        format: "YYYY-MM-DD",
        separator: "  -  ",
        applyLabel: "Apply",
        cancelLabel: "Cancel",
        fromLabel: "From",
        toLabel: "To",
        customRangeLabel: "Custom",
        weekLabel: "W",
        daysOfWeek: ["S", "Pr", "A", "T", "K", "Pn", "Š"],
        monthNames: [
          "Sausis",
          "Vasaris",
          "Kovas",
          "Balandis",
          "Gegužė",
          "Birželis",
          "Liepa",
          "Rugpjūtis",
          "Rugsėjis",
          "Spalis",
          "Lapkritis",
          "Gruodis"
        ],
        firstDay: 1
      }
    },
    function(start, end, label) {}
  );
  var hours = $(
    'input[name="postpone_hours"], input[name="calendar_start_hours"], input[name="calendar_finish_hours"]'
  );
  var minutes = $(
    'input[name="calendar_start_minutes"], input[name="postpone_hours"], input[name="calendar_finish_minutes"]'
  );
  hours.on("blur", function() {
    var value = parseInt($(this).val());
    if (String(value).length < 1 || isNaN(value)) {
      $(this).val(12);
    }
    if (value > 24) {
      $(this).val(24);
    }
    if (value < 0) {
      $(this).val(0);
    }
  });
  minutes.on("blur", function() {
    var value = parseInt($(this).val());
    if (String(value).length < 1 || isNaN(value)) {
      $(this).val(00);
    }
    if (value > 59) {
      $(this).val(59);
    }
    if (value < 0) {
      $(this).val(00);
    }
    if (String(value).length == 1) {
      $(this).val("0" + String(value));
    }
  });
  $('input[name="postpone"]').on("change", function() {
    $(".time_pickers > div").slideToggle(150);
  });
  $('input[name="type"]').on("change", function() {
    $(".calendar_hideable").slideToggle(150);
  });
  var toolbarOptions = [
    ["bold", "italic", "underline"],
    ["blockquote"],
    [{ list: "ordered" }, { list: "bullet" }],
    [{ script: "sub" }, { script: "super" }],
    [{ indent: "-1" }, { indent: "+1" }],
    [{ size: ["small", false, "large", "huge"] }],
    [{ header: [1, 2, 3, 4, 5, 6, false] }],
    [{ align: [] }],
    ["link", "image", "video"],
    ["clean"]
  ];
  var options = {
    theme: "snow",
    modules: {
      toolbar: toolbarOptions
    }
  };
  if ($(".editor_iframe").is(":visible")) {
    var editor_iframes = $(".editor_iframe");
    var length = editor_iframes.length;
    function init(i) {
      var editor_iframe = editor_iframes.eq(i);
      var iframe = editor_iframe.find("iframe");
      var string = editor_iframe.find("input").val();
      var error = editor_iframe.find("input").attr("data-oversize");
      var texts = editor_iframe.find("input").attr("data-texts");
      if (typeof iframe.get(0).contentWindow.getData == "undefined") {
        iframe.on("load", function() {
          var inside = iframe.contents().find("body");
          inside.attr("data-html-input", string);
          inside.attr("data-oversize", error);
          inside.attr("data-texts", texts);
          setTimeout(function() {
            iframe.get(0).contentWindow.getData();
            i++;
            if (i < length) {
              init(i);
            }
          });
        });
      } else {
        var inside = iframe.contents().find("body");
        inside.attr("data-html-input", string);
        inside.attr("data-oversize", error);
        inside.attr("data-texts", texts);
        setTimeout(function() {
          iframe.get(0).contentWindow.getData();
          i++;
          if (i < length) {
            init(i);
          }
        });
      }
    }
    init(0);
  }
  if ($(".drag_and_drop").is(":visible")) {
    var full_files_list = [];
    function photographerUpload() {
      $(document).on(
        "click",
        ".edit_photo_photographer_upload.pop .button.white",
        function() {
          closePop();
        }
      );
      $(document).on(
        "click",
        ".edit_photo_photographer_upload.pop .button.blue",
        function() {
          var context = $(opened_photo_edit_element)
            .closest(".file")
            .find(".remove");
          var id_number = $(opened_photo_edit_element)
            .closest(".file")
            .find(".remove")
            .attr("data-id-number");
          var name = $(
            '.pop.edit_photo_photographer_upload input[name="name"]'
          ).val();
          var keywords = $(
            '.pop.edit_photo_photographer_upload input[name="keywords"]'
          ).val();
          var comments = $(
            '.pop.edit_photo_photographer_upload textarea[name="comments"]'
          ).val();
          var date = "";
          if (
            $('.pop.edit_photo_photographer_upload input[name="day_photo"]').is(
              ":checked"
            )
          ) {
            date = $(
              '.pop.edit_photo_photographer_upload input[name="photo_of_the_day_date"]'
            ).val();
          }
          context.attr("data-keywords", keywords);
          context.attr("data-comment", comments);
          context.attr("data-date", date);
          $(opened_photo_edit_element)
            .closest(".file")
            .find(".name")
            .text(name);
          $(opened_photo_edit_element)
            .closest(".file")
            .find(".tooltip")
            .text(comments);
          closePop();
          for (var inp in full_files_list) {
            for (var i = 0, j = full_files_list[inp].length; i < j; i++) {
              var one_file = full_files_list[inp][i];
              if (one_file.id_number == id_number) {
                one_file.title = name;
                one_file.keywords = keywords;
                one_file.comments = comments;
                one_file.date = date;
                break;
              }
            }
          }
        }
      );
    }
    var photographer = false;
    if ($(".photographer_upload_page").is(":visible")) {
      photographer = true;
      photographerUpload();
    }
    $(document).on("change", 'input[type="file"]', function(e) {
      var target = e.target;
      var input_name = target.getAttribute("name");
      var files = target.files;
      var drag_and_drop = $(this).closest(".drag_and_drop");
      var uploaded_area = drag_and_drop.find(".uploaded_area");

      function filesRecursion(i) {
        if (i >= files.length) {
          var label_area = drag_and_drop.find("label.area");
          var input_area_html = label_area[0].outerHTML;
          label_area.remove();
          drag_and_drop.prepend(input_area_html);
          return false;
        }
        var file = files[i];
        if (
          file.type == "image/jpeg" ||
          file.type == "image/png" ||
          file.type == "image/png"
        ) {
          var reader = new FileReader();
          reader.onload = function(e) {
            var blob = e.target.result;
            var id_number = parseInt(Math.random() * 100000000000);
            if (photographer) {
              var get_name = $('.right.layout input[name="name"]').val();
              var get_comments = $(
                '.right.layout textarea[name="content"]'
              ).val();
              var get_keywords = $(
                '.right.layout input[name="keywords"]'
              ).val();
              var html =
                '<div class="file image"><div class="border"><div class="buttons"><div class="info extra_tooltip"><div class="tooltip">' +
                get_comments +
                '</div></div><span class="edit open_edit_pop">Redaguoti</span></div><div class="remove" data-name="' +
                file.name +
                '" data-keywords="' +
                get_keywords +
                '" data-date="" data-comment="' +
                get_comments +
                '" data-id-number="' +
                id_number +
                '"></div><div class="img_holder"><img src="' +
                blob +
                '" alt=""></div></div><div class="name">' +
                get_name +
                "</div></div>";
            } else {
              var html =
                '<div class="file image"><div class="border"><div class="remove" data-name="' +
                file.name +
                '" data-id-number="' +
                id_number +
                '"></div><div class="img_holder"><img src="' +
                blob +
                '" alt=""></div></div><div class="name">' +
                file.name +
                "</div></div>";
            }
            uploaded_area.prepend(html);
            if (typeof full_files_list[input_name] === "undefined")
              full_files_list[input_name] = [];
            file.id_number = id_number;
            if (photographer) {
              file.keywords = get_keywords;
            }
            full_files_list[input_name].push(file);

            // full_files_list[input_name]
            i++;
            filesRecursion(i);
          };
          reader.readAsDataURL(file);
        } else {
          var id_number = parseInt(Math.random() * 100000000000);
          if (photographer) {
            var get_name = $('.right.layout input[name="name"]').val();
            var get_keywords = $('.right.layout input[name="keywords"]').val();
            var get_comments = $(
              '.right.layout textarea[name="content"]'
            ).val();
            var html =
              '<div class="file"><div class="border"><div class="buttons"><div class="info extra_tooltip"><div class="tooltip">' +
              get_comments +
              '</div></div><span class="edit open_edit_pop">Redaguoti</span></div><div class="remove" data-name="' +
              file.name +
              '" data-keywords="' +
              get_keywords +
              '" data-date="" data-comment="' +
              get_comments +
              '" data-id-number="' +
              id_number +
              '"></div><div class="file_holder">' +
              file.name +
              '</div></div><div class="name">' +
              get_name +
              "</div></div>";
          } else {
            var html =
              '<div class="file"><div class="border"><div class="remove" data-name="' +
              file.name +
              '" data-id-number="' +
              id_number +
              '"></div><div class="file_holder">' +
              file.name +
              '</div></div><div class="name">' +
              file.name +
              "</div></div>";
          }
          uploaded_area.prepend(html);
          if (typeof full_files_list[input_name] === "undefined")
            full_files_list[input_name] = [];
          file.id_number = id_number;
          if (photographer) {
            file.keywords = get_keywords;
          }
          full_files_list[input_name].push(file);
          i++;
          filesRecursion(i);
        }
      }
      filesRecursion(0);
    });
    $(document).on("click", ".drag_and_drop .remove", function() {
      var input_name = $(this)
        .closest(".drag_and_drop")
        .find('input[type="file"]')
        .attr("name");
      $(this)
        .closest(".file")
        .remove();
      if (typeof full_files_list[input_name] === "undefined") return;
      var new_files_list = [];
      var name = $(this).attr("data-name");
      for (var i = 0, j = full_files_list[input_name].length; i < j; i++) {
        var one = full_files_list[input_name][i];
        if (one.name != name) {
          new_files_list.push(one);
        }
      }
      full_files_list[input_name] = new_files_list;
    });
  }
  $("form").on("submit", function(e) {
    // e.preventDefault();
    if (typeof eltaBack != "undefined") eltaBack.setFormFiles(full_files_list);
    return checkData();
  });
  function checkData() {
    // Čia bus formos validacija, surenkant duomenis labai svarbu paimt ir failus, kurie guli sitam kintamajam - full_files_list, nes file inputas išvalomas
    // console.log(full_files_list);
    return true;
  }
  $('input.group_check').on(
    "change",
    function() {
      $(this)
        .closest(".simple_checkbox")
        .next(".multicheckboxes")
        // laikinai neiskleidziam: .slideToggle(150)
        
        // laikinai zymim viska, kol nerodom gaveju:
          .find("input")
          .prop("checked", $(this).is(":checked"))          
        // end laikinai                
        ;

    }
  );
  $('input[name="sent_to_all"]').on("change", function() {
    if ($(this).is(":checked")) {
      $('input.group_check').prop("checked", true)
      // laikinai zymim viska, kol nerodom gaveju:
          .change()       
      ;
      // laikinai neiskleidziam: $(".multicheckboxes").slideDown(150);
    } else {
      $('input.group_check').prop("checked", false)
      // laikinai zymim viska, kol nerodom gaveju:
          .change()      
        // end laikinai                      
      ;
      // laikinai neiskleidziam: $(".multicheckboxes").slideUp(150);
    }
  });
  $(
    'input.all'
  ).on("change", function() {
    if ($(this).is(":checked")) {
      $(this)
        .closest(".multicheckboxes")
        .find("input")
        .not($(this))
        .prop("checked", true);
    } else {
      $(this)
        .closest(".multicheckboxes")
        .find("input")
        .not($(this))
        .prop("checked", false);
    }
  });

  $(".translator_photo_page .photo_block .edit").on("click", function() {
    $(".pop.one_photo img").attr("src", "");
    var src = $(this)
      .closest(".photo_block")
      .find("img")
      .attr("data-published-src");
    $(".pop.one_photo img").attr("src", src);
    openPop("one_photo", this);
  });
}

function passwordPage() {
  var form = $("form#password_form");
  form.on("submit", function(e) {
    return checkFormData();
  });
  function checkFormData() {
    form.find("input").removeClass("error");
    // var simple_inputs = form.find('input[type="text"], input[type="password"], input[type="email"]');
    var email_input = form.find('input[type="text"]');
    var simple_inputs = email_input;

    for (var i = 0, j = simple_inputs.length; i < j; i++) {
      var input = simple_inputs.eq(i);
      if (String(input.val()).length < 1) {
        input.addClass("error");
      }
    }
    if (validateEmail(email_input.val()) == false) {
      email_input.addClass("error");
    }
    if (form.find(".error").length == 0) {
      return true;
    }
    return false;
  }
  $("form input").on("focus", function() {
    $(this).removeClass("error");
  });
}

function loginPage() {
  var form_login = $("form#login_form");
  var form_register = $("form#register_form");
  form_login.on("submit", function(e) {
    return checkLoginData();
  });
  form_register.on("submit", function(e) {
    return checkRegisterData();
  });
  function checkLoginData() {
    form_login.find("input").removeClass("error");
    var simple_inputs = form_login.find(
      'input[type="text"], input[type="password"]'
    );
    for (var i = 0, j = simple_inputs.length; i < j; i++) {
      var input = simple_inputs.eq(i);
      if (String(input.val()).length < 1) {
        input.addClass("error");
      }
    }
    return form_login.find(".error").length == 0;
  }
  function checkRegisterData() {
    form_register.find("input").removeClass("error");
    var input = form_register.find('input[type="email"]');
    if (input.val().length < 1 || validateEmail(input.val()) == false) {
      input.addClass("error");
      return false;
    }
    return true;
  }
  $("form input").on("focus", function() {
    $(this).removeClass("error");
  });
}

function loginPage() {
  var form_login = $("form#login_form");
  var form_register = $("form#register_form");
  form_login.on("submit", function(e) {
    return checkLoginData();
  });
  form_register.on("submit", function(e) {
    return checkRegisterData();
  });
  function checkLoginData() {
    form_login.find("input").removeClass("error");
    var simple_inputs = form_login.find(
      'input[type="text"], input[type="password"]'
    );
    for (var i = 0, j = simple_inputs.length; i < j; i++) {
      var input = simple_inputs.eq(i);
      if (String(input.val()).length < 1) {
        input.addClass("error");
      }
    }
    return form_login.find(".error").length == 0;
  }
  function checkRegisterData() {
    form_register.find("input").removeClass("error");
    var input = form_register.find('input[type="text"]');
    if (input.val().length < 1 || validateEmail(input.val()) == false) {
      input.addClass("error");
      return false;
    }
    return true;
  }
  $("form input").on("focus", function() {
    $(this).removeClass("error");
  });
}

function activatePhotosSilder(context) {
  var swiper_pop = new Swiper(".swiper-container.pop_photo_box", {
    speed: 500,
    simulateTouch: false,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    spaceBetween: 100,
    on: {
      init: function() {
        var all_photos = document.querySelectorAll(".photos_holder .photo");
        var index;
        for (var i = 0, j = all_photos.length; i < j; i++) {
          var one = all_photos[i];
          if (
            one ==
            $(context)
              .closest(".photo")
              .get(0)
          ) {
            index = i;
          }
        }
        var that = this;
        this.update();
        setTimeout(function() {
          that.slideTo(index, 1);
        }, 1);
      }
    },
    preloadImages: false,
    lazy: true
  });
  var swiper_pop_2 = new Swiper(".swiper-container.pop_photo_box_2", {
    speed: 500,
    simulateTouch: false,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    spaceBetween: 100,
    on: {
      init: function() {
        var all_photos = document.querySelectorAll(".photos_holder .photo");
        var index;
        for (var i = 0, j = all_photos.length; i < j; i++) {
          var one = all_photos[i];
          if (
            one ==
            $(context)
              .closest(".photo")
              .get(0)
          ) {
            index = i;
          }
        }
        var that = this;
        this.update();
        setTimeout(function() {
          that.slideTo(index, 1);
        }, 1);
      }
    },
    preloadImages: false,
    lazy: true
  });

  var pop_name = $(".photos_pop_head .name").eq(0);
  var pop_timestamp = $(".photos_pop_head .time_stamp").eq(0);
  var slide = $(".pop.photos .swiper-slide.swiper-slide-active");
  var name = slide.find(".name").html();
  var timestamp = slide.find(".time_stamp").html();
  var photoId = slide.attr("data-photo-id");
  var fullSizeSrc = slide.attr("data-fullsize-src");
  pop_name
    .html(name)
    .parent()
    .attr("data-photo-id", photoId)
    .attr("data-fullsize-src", fullSizeSrc);
  var base_photos_url = $(".photos_list").attr("data-photos-base-url");

  history.replaceState(null, '', base_photos_url + photoId);

  pop_timestamp.html(timestamp);
  swiper_pop.on("transitionStart", function() {
    slide = $(".pop.photos .swiper-slide.swiper-slide-active");
    name = slide.find(".name").html();
    timestamp = slide.find(".time_stamp").html();
    photoId = slide.attr("data-photo-id");
    fullSizeSrc = slide.attr("data-fullsize-src");
    pop_name
      .html(name)
      .parent()
      .attr("data-photo-id", photoId)
      .attr("data-fullsize-src", fullSizeSrc);
    pop_timestamp.html(timestamp);

    history.replaceState(null, '', base_photos_url + photoId);
  });

  var pop_name_2 = $(".photos_pop_head .name").eq(1);
  var pop_timestamp_2 = $(".photos_pop_head .time_stamp").eq(1);
  var slide_2 = $(".pop.photos_download .swiper-slide.swiper-slide-active");
  var name_2 = slide.find(".name").html();
  var timestamp_2 = slide.find(".time_stamp").html();
  var photoId_2 = slide.attr("data-photo-id");
  var fullSizeSrc_2 = slide.attr("data-fullsize-src");
  pop_name_2
    .html(name_2)
    .parent()
    .attr("data-photo-id", photoId_2)
    .attr("data-fullsize-src", fullSizeSrc_2);
  pop_timestamp_2.html(timestamp_2);
  swiper_pop_2.on("transitionStart", function() {
    slide_2 = $(".pop.photos_download .swiper-slide.swiper-slide-active");
    name_2 = slide.find(".name").html();
    timestamp_2 = slide.find(".time_stamp").html();
    photoId_2 = slide.attr("data-photo-id");
    fullSizeSrc_2 = slide.attr("data-fullsize-src");
    pop_name_2
      .html(name_2)
      .parent()
      .attr("data-photo-id", photoId_2)
      .attr("data-fullsize-src", fullSizeSrc_2);
    pop_timestamp_2.html(timestamp_2);
  });
}

function openPop(name, context) {
  if (popup_debounce) {
    return false;
  }
  popup_debounce = true;
  if (typeof eltaFront != "undefined")
    eltaFront.feedPopup(name, { context: context });
  if (typeof eltaBack != "undefined")
    eltaBack.feedPopup(name, { context: context });

  setTimeout(function() {
    popup_debounce = false;
  }, 300);
  pop_opened = name;
  var overlay = $("#overlay");
  var pop = $(".pop." + name);
  if (overlay.hasClass("show")) {
    popup_debounce = false;
    closePop();
    setTimeout(function() {
      openPop(name, context);
    }, 310);
    return false;
  }

  $("main").css({ "pointer-events": "none" });
  pop.show(0, function() {
    if (name == "photos" || name == "photos_download") {
      overlay.addClass("overflow");
      activatePhotosSilder(context);
    } else if (name == "edit_photo_photographer_upload") {
      overlay.css({ display: "flex" });
      var image = $(context)
        .closest(".file")
        .find("img");
      var name_field = $(context)
        .closest(".file")
        .find(".name")
        .text();
      var keywords_field = $(context)
        .closest(".file")
        .find(".remove")
        .attr("data-keywords");
      var date_field = $(context)
        .closest(".file")
        .find(".remove")
        .attr("data-date");
      var comment_field = $(context)
        .closest(".file")
        .find(".remove")
        .attr("data-comment");
      pop.find('input[name="name"]').val(name_field);
      pop.find('input[name="keywords"]').val(keywords_field);
      pop.find('textarea[name="comments"]').val(comment_field);
      opened_photo_edit_element = context;
      if (date_field != "") {
        pop.find('input[name="day_photo"]').prop("checked", true);
        pop.find('input[name="datepicker"]').val(date_field);
      } else {
        pop.find('input[name="day_photo"]').prop("checked", false);
      }
      if (image.length == 0) {
        pop.find(".img_holder").addClass("not_img");
        pop.find(".img_holder img").remove();
      } else {
        var img_src = image.attr("src");
        pop.find(".img_holder").removeClass("not_img");
        if (pop.find(".img_holder img").length == 0) {
          var img_html = '<img src="' + img_src + '" alt="" />';
          pop.find(".img_holder").html(img_html);
        } else {
          pop.find(".img_holder img").attr("src", img_src);
        }
      }
    } else if (name == "edit_news_photo") {
      overlay.css({ display: "flex" });
      var image = $(context)
        .closest(".photo_block")
        .find("img");
      var name_field = $(context)
        .closest(".photo_block")
        .find(".name")
        .text();
      var comment_field = $(context)
        .closest(".photo_block")
        .attr("data-comment");
      pop.find('input[name="name"]').val(name_field);
      pop.find('textarea[name="comments"]').val(comment_field);
      opened_news_edit_element = context;
      if (image.length == 0) {
        pop.find(".img_holder").addClass("not_img");
        pop.find(".img_holder img").remove();
      } else {
        var img_src = image.attr("src");
        pop.find(".img_holder").removeClass("not_img");
        if (pop.find(".img_holder img").length == 0) {
          var img_html = '<img src="' + img_src + '" alt="" />';
          pop.find(".img_holder").html(img_html);
        } else {
          pop.find(".img_holder img").attr("src", img_src);
        }
      }
    } else if (name == "one_photo" || name == "mobile_detail_filter") {
      overlay.addClass("overflow");
    } else if (name == "one_video") {
      overlay.addClass("overflow");
      var iframe = $(".pop.one_video iframe");
      var src = $(".pop.one_video .img_holder").attr("data-iframe");
      if (iframe && src) {
        iframe.attr("src", src);
      }
    } else {
      overlay.css({ display: "flex" });
    }
    if (name == "confirm_download") {
      pop
        .find(".white")
        .attr("data-fullsize-src", $(context).attr("data-fullsize-src"))
        .attr("data-photo-id", $(context).attr("data-photo-id"));
    }
    overlay.addClass("show");
    pop.addClass("show");
  });
}

function closePop() {
  if (popup_debounce) {
    return false;
  }
  popup_debounce = true;
  setTimeout(function() {
    popup_debounce = false;
  }, 300);
  pop_opened = null;
  var overlay = $("#overlay");
  var pop = $(".pop");
  $("main").css({ "pointer-events": "all" });
  pop.removeClass("show");
  overlay.removeClass("show");
  setTimeout(function() {
    if (pop.find("iframe")) {
      pop.find("iframe").attr("src", "");
    }
    $(".photo_block.checked").removeClass("checked");
    overlay.removeClass("overflow");
    overlay.css({ display: "block" });
    pop.hide();
  }, 250);
}

function globals() {
  if (
    ($(".news_page").is(":visible") ||
      $(".photos_page").is(":visible") ||
      $(".expected_page").is(":visible")) &&
    !$(".page").hasClass("no_filter")
  ) {
    $("header").addClass("mobile_filter");
  }
  $("header").addClass("opacity");
  if (!$(".title_page").is(":visible")) {
    $("header").addClass("white");
  }
  $("header .right_block .search_button").on("click", function() {
    var parent = $(this).closest(".search");
    if (parent.hasClass("open")) {
      var input = parent.find("input");
      if (input.val().length > 0) {
        // search script
      } else {
        parent.removeClass("open");
      }
    } else {
      parent.addClass("open");
    }
  });
  $(".cookies_pop .button").on("click", function() {
    var cookies_pop = $(".cookies_pop");
    cookies_pop.addClass("gone");
    setTimeout(function() {
      cookies_pop.remove();
    }, 260);
  });
  activateDropdowns();
  activateTooltips();
  $(document).on("click", function(e) {
    var target = $(e.target);
    if (
      !target.hasClass("simple_dropdown") &&
      !target.closest(".simple_dropdown").hasClass("simple_dropdown")
    ) {
      $(".simple_dropdown").removeClass("open");
    }
    if (
      target.closest(".pop").hasClass("pop") &&
      (target.hasClass("close") || target.hasClass("close_box"))
    ) {
      closePop();
    }
    if (
      !target.closest(".simple_tooltip").hasClass("simple_tooltip") &&
      !target.hasClass("simple_tooltip")
    ) {
      $(".simple_tooltip").removeClass("open");
    }
    if (
      !target.closest(".extra_tooltip").hasClass("extra_tooltip") &&
      !target.hasClass("extra_tooltip")
    ) {
      $(".extra_tooltip").removeClass("open");
    }
    if (
      !target.hasClass("mobile_person_opener") &&
      !target
        .closest(".mobile_person_opener")
        .hasClass("mobile_person_opener") &&
      !target.hasClass("mobile_toggler") &&
      !target.closest(".mobile_toggler").hasClass("mobile_toggler") &&
      !target.hasClass("burger_plus") &&
      !target.closest(".burger_plus").hasClass("burger_plus") &&
      !target.hasClass("burger") &&
      !target.closest(".burger").hasClass("burger") &&
      !target.hasClass("mobile_search") &&
      !target.closest(".mobile_search").hasClass("mobile_search") &&
      !target.hasClass("mobile_search_button") &&
      !target.closest(".mobile_search_button").hasClass("mobile_search_button")
    ) {
      if ($(".title_page").is(":visible")) {
        var top = $("main").scrollTop();
        if (top < 20) {
          $("header").removeClass("white");
          header_white = false;
        }
      }
      var body = $("body");
      body.removeClass("open");
      body.removeClass("search");
      body.removeClass("submenu");
      body.removeClass("person");
    }
    if (target.hasClass("overlay")) {
      closePop();
    }
    if (target.hasClass("option")) {
      target
        .closest(".simple_dropdown")
        .find("input")
        .removeClass("error");
    }
  });
  $(document).on(
    "focus",
    'input[type="text"], input[type="email"], input[type="password"], input[type="number"], textarea',
    function() {
      $(this).removeClass("error");
    }
  );
  if ($(".title_page").is(":visible")) {
    (function() {
      var debounce = false;
      var body = $("body");
      var header = $("header");
      var main = $("main");

      $(".burger, .burger_plus").on("click", function() {
        if (debounce) {
          return false;
        }
        debounce = true;
        setTimeout(function() {
          debounce = false;
        }, 300);
        var top = main.scrollTop();
        if (top < 20) {
          if (body.hasClass("open") || body.hasClass("submenu")) {
            header.removeClass("white");
            header_white = false;
          } else {
            header.addClass("white");
            header_white = true;
          }
        }
        if (body.hasClass("submenu")) {
          body.removeClass("submenu");
        } else {
          body.toggleClass("open");
          body.removeClass("search");
          body.removeClass("person");
        }
      });
      $(".mobile_person_opener").on("click", function() {
        if (debounce) {
          return false;
        }
        debounce = true;
        setTimeout(function() {
          debounce = false;
        }, 300);
        var top = main.scrollTop();
        if (top < 20) {
          if (body.hasClass("person")) {
            header.removeClass("white");
            header_white = false;
          } else {
            header.addClass("white");
            header_white = true;
          }
        }
        if (body.hasClass("submenu")) {
          body.removeClass("submenu");
          body.addClass("person");
        } else {
          body.toggleClass("person");
          body.removeClass("open");
          body.removeClass("search");
        }
      });
      $(".mobile_search_button").on("click", function() {
        if (debounce) {
          return false;
        }
        debounce = true;
        setTimeout(function() {
          debounce = false;
        }, 300);
        var top = main.scrollTop();
        if (top < 20) {
          if (body.hasClass("search")) {
            header.removeClass("white");
            header_white = false;
          } else {
            header.addClass("white");
            header_white = true;
          }
        }
        if (body.hasClass("submenu")) {
          body.removeClass("submenu");
          body.addClass("search");
        } else {
          body.toggleClass("search");
          body.removeClass("open");
          body.removeClass("person");
        }
      });
      $(".mobile_menu .mobile_toggler").on("click", function() {
        if (top < 20) {
          header.addClass("white");
          header_white = true;
        }
        body.removeClass("open");
        body.addClass("submenu");
      });
    })();
  } else {
    (function() {
      var debounce = false;
      var body = $("body");
      $(".burger, .burger_plus").on("click", function() {
        if (debounce) {
          return false;
        }
        debounce = true;
        setTimeout(function() {
          debounce = false;
        }, 300);
        if (body.hasClass("submenu")) {
          body.removeClass("submenu");
        } else {
          body.toggleClass("open");
          body.removeClass("search");
          body.removeClass("person");
        }
      });
      $(".mobile_search_button").on("click", function() {
        if (debounce) {
          return false;
        }
        debounce = true;
        setTimeout(function() {
          debounce = false;
        }, 300);
        if ($("header").hasClass("mobile_filter")) {
          body.removeClass("open");
          body.removeClass("person");
          body.removeClass("submenu");
          body.removeClass("search");
          openPop("mobile_detail_filter");
        } else {
          if (body.hasClass("submenu")) {
            body.removeClass("submenu");
            body.addClass("search");
          } else {
            body.toggleClass("search");
            body.removeClass("open");
            body.removeClass("person");
          }
        }
      });
      $(".mobile_person_opener").on("click", function() {
        if (debounce) {
          return false;
        }
        debounce = true;
        setTimeout(function() {
          debounce = false;
        }, 300);
        if (body.hasClass("submenu")) {
          body.removeClass("submenu");
          body.addClass("person");
        } else {
          body.toggleClass("person");
          body.removeClass("open");
          body.removeClass("search");
        }
      });
      $(".mobile_menu .mobile_toggler").on("click", function() {
        body.removeClass("open");
        body.addClass("submenu");
      });
    })();
  }
  $(".pop.confirm .button").on("click", function() {
    closePop();
  });
  $(".pop.confirm_download .button").on("click", function() {
    if ($(this).hasClass("white") && $(this).attr("data-fullsize-src")) {
      var link = document.createElement("a");
      link.href = $(this).attr("data-fullsize-src");
      link.download = $(this).attr("data-photo-id") + ".jpg";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      // reduce the counter:
      if ($(this).attr("data-reduce-counter") == 1) {
        var rest = parseInt(
          $(".photos_rest_to_download")
            .first()
            .html()
        );
        $(".photos_rest_to_download").html(rest > 0 ? rest - 1 : 0);
      }
    }
    closePop();
  });
  $(document).on("click", ".open_edit_pop", function(e) {
    if ($("#photographer_upload_form").is(":visible")) {
      openPop("edit_photo_photographer_upload", e.target);
    } else {
      openPop("edit_photo", e.target);
    }
  });
  $(document).on("click", ".open_edit_news_photo", function(e) {
    openPop("edit_news_photo", e.target);
  });
  $(".add_another").on("click", function() {
    var previous_object = $(this).prev();
    var new_element = previous_object.clone();
    var name = $(".delete")
      .eq(0)
      .text();
    if (new_element.html().indexOf("remove_another") == -1) {
      var wraped =
        '<div class="added_another">' +
        new_element[0].outerHTML +
        '<div><div class="remove_another">' +
        name +
        "</div></div></div>";
      $(wraped).insertAfter(previous_object);
    } else {
      new_element.insertAfter(previous_object);
    }
    var newly_added = $(this).prev();
    newly_added.find(".current").removeClass("current");
    newly_added.find("input").val("");
    refreshDropdowns();
  });
  $(document).on("click", ".remove_another", function() {
    $(this)
      .closest(".added_another")
      .remove();
  });
  $(".deactivate_button_holder .deactivate").on("click", function() {
    $(this).toggleClass("active_account");
  });
  $(document).on("click", ".copy_text, .copy_text_pop", function(e) {
    var button = $(e.target);
    if (typeof button.attr("data-target") != "undefined") {
      var what = button.attr("data-target");
      CopyToClipboard(what);
    }
  });
  var checkIfMobileDebounce;
  function checkIfMobile() {
    if (window.innerWidth <= 1000) {
      isMobile = true;
      console.log("mobile");
    } else {
      isMobile = false;
      console.log("desktop");
    }
  }
  checkIfMobile();
  $(window).on("resize", function() {
    clearTimeout(checkIfMobileDebounce);
    checkIfMobileDebounce = setTimeout(function() {
      checkIfMobile();
    }, 300);
  });
  $(".mobile_menu a").on("click", function() {
    $("body").removeClass("open");
  });
}

function refreshDropdowns() {
  var contents = $(".simple_dropdown .content");
  for (var i = 0, j = contents.length; i < j; i++) {
    var content = contents.eq(i);
    var content_init = document.querySelectorAll(".simple_dropdown .content")[
      i
    ];
    if (content.innerHeight() > 276) {
      content.css({ height: "276px", "border-bottom": "1px solid #efefef" });
      var ps = new SimpleBar(content_init, { autoHide: false });
    }
  }
}

function activateDropdowns() {
  var contents = $(".simple_dropdown .content");
  for (var i = 0, j = contents.length; i < j; i++) {
    var content = contents.eq(i);
    var content_init = document.querySelectorAll(".simple_dropdown .content")[
      i
    ];
    if (content.innerHeight() > 276) {
      content.css({ height: "276px", "border-bottom": "1px solid #efefef" });
      var ps = new SimpleBar(content_init, { autoHide: false });
    }
  }
  $(document).on("click", ".simple_dropdown", function(e) {
    $(".simple_dropdown")
      .not($(this))
      .removeClass("open");
    $(this).toggleClass("open");
    var target = $(e.target);
    var input = $(this).find("input");
    if (target.hasClass("option") && !target.hasClass("current")) {
      var value = target.text();
      input.val(value);
      $(this)
        .find(".option")
        .removeClass("current");
      target.addClass("current");

      if (
        window[$(this).attr("data-onchange-context")] &&
        typeof window[$(this).attr("data-onchange-context")][
          $(this).attr("data-onchange-function")
        ] == "function"
      ) {
        window[$(this).attr("data-onchange-context")][
          $(this).attr("data-onchange-function")
        ](target.attr("data-value"), $(this));
      }

      switch ($(this).attr("data-type")) {
        case "category":
          var catDropdown = $(this).find(
            'input[name="category"],input[name="category[]"],input[data-type-input="category"]'
          );
          if (
            typeof eltaSubcategories != "undefined" &&
            catDropdown.length > 0
          ) {
            var subcatDropdown,
              catDropdownName = catDropdown.attr("name");
            if (catDropdownName == "category")
              subcatDropdown = $(this)
                .parent()
                .find('input[name="sub' + catDropdownName + '"]')
                .closest(".simple_dropdown");
            else
              subcatDropdown = $(this)
                .closest(".row")
                .find('[name="sub' + catDropdownName + '"]')
                .closest(".simple_dropdown");

            if (subcatDropdown && subcatDropdown.length) {
              subcatDropdown.find("input").val("");
              var subcats = eltaSubcategories[target.attr("data-value")],
                subcatOptions = "";
              if (typeof subcats != "undefined") {
                for (var subcatId in subcats) {
                  subcatOptions +=
                    '<div class="option" data-value="' +
                    subcats[subcatId].id +
                    '">' +
                    subcats[subcatId].name +
                    "</div>";
                }
              }
              subcatDropdown.find(".content").remove();
              subcatDropdown.append(
                '<div class="content">' + subcatOptions + "</div>"
              );
              refreshDropdowns();
            }
          }
          break;
        case "department":
          fillDepartmentEmployees($(this), target.attr("data-value"));
          break;
      }
    }
  });
  $(".autocomplete_dropdown select.for_desktop").chosen("destroy");
  $(".autocomplete_dropdown select.for_desktop").chosen({
    no_results_text: "Rezultatų nėra: "
  });
  $("select.not_touched").on("change", function() {
    $(this).removeClass("not_touched");
  });
  $(document).on(
    "change",
    "div[data-type=country-filter] input[type=radio]",
    function(e) {
      if (typeof eltaFront != "undefined") eltaFront.changeCountry($(e.target));
      else if (typeof eltaBack != "undefined")
        eltaBack.changeCountry($(e.target));
    }
  );
}

function fillDepartmentEmployees(deptDropdown, selectedDept, employeeId) {
  var inputObj = deptDropdown.find('input[name="_assign_to_department"]');
  if (typeof eltaDeptEmployees != "undefined" && inputObj.length > 0) {
    var dependentDropdown = deptDropdown
      .closest(".add_manager")
      .find(".autocomplete_dropdown");
    if (dependentDropdown && dependentDropdown.length) {
      var dependentOptions = '<option value=""></option>';
      if (selectedDept) {
        var employees = eltaDeptEmployees[selectedDept].employees;
        if (typeof employees != "undefined") {
          for (var eId in employees) {
            dependentOptions +=
              "<option " +
              (employees[eId].id == employeeId ? "selected " : "") +
              'value="' +
              employees[eId].id +
              '">' +
              employees[eId].name +
              "</option>";
          }
        }
      }
      dependentDropdown.find("select").each(function() {
        $(this)
          .val("")
          .chosen("destroy")
          .html(dependentOptions)
          .chosen({ no_results_text: "Rezultatų nėra: " });
      });
    }
  }
}

function activateTooltips() {
  $(".simple_tooltip").on("click", function() {
    $(".simple_tooltip")
      .not($(this))
      .removeClass("open");
    $(this).toggleClass("open");
  });
}

function registrationPage() {
  var form = $(".registration_page form");
  form.on("submit", function(e) {
    return true; // checkData();
  });
  function checkData() {
    form.find("input").removeClass("error");
    var simple_inputs = form.find(
      'input[type="text"], input[type="number"], input[type="email"]'
    );
    var email_input = form.find('input[type="email"]');
    var rules_checkbox = form.find('input[name="agreed_to_rules"]');
    for (var i = 0, j = simple_inputs.length; i < j; i++) {
      var input = simple_inputs.eq(i);
      if (String(input.val()).length < 1) {
        input.addClass("error");
      }
    }
    if (!validateEmail(email_input.val())) {
      email_input.addClass("error");
    }
    if (!rules_checkbox.is(":checked")) {
      rules_checkbox.addClass("error");
    }
    if (form.find(".error").length == 0) {
      return true;
    } else if (window.innerWidth < 1000) {
      var top =
        form
          .find(".error")
          .eq(0)
          .offset().top +
        $("main").scrollTop() -
        100;
      $("main").animate({ scrollTop: top }, 300);
    }
    return false;
  }
  $(".registration_page form input").on("focus", function() {
    $(this).removeClass("error");
  });
  $('.registration_page form input[type="checkbox"]').on("change", function() {
    $(this).removeClass("error");
  });
  $('input[name="is_private_person"]').on("change", function() {
    $(".company_hide").slideToggle(150);
  });
}

function subscribePage() {
  var form = $(".subscribe_page form");
  form.on("submit", function(e) {
    return checkData();
  });
  function checkData() {
    form.find("input").removeClass("error");
    var simple_inputs_1 = form
      .find('input[type="text"], input[type="number"]')
      .not(
        '.keywords_hider input[type="text"], .keywords_hider input[type="number"]'
      );
    var email_input_1 = form
      .find('input[type="email"]')
      .not('.keywords_hider input[type="email"]');
    var simple_inputs_2 = form.find(
      '.keywords_hider input[type="text"], .keywords_hider input[type="number"]'
    );
    var email_input_2 = form.find('.keywords_hider input[type="email"]');

    if (
      $(".remove_another").not(".keywords_hider .remove_another").length == 0
    ) {
      for (var i = 0, j = email_input_1.length; i < j; i++) {
        var input = email_input_1.eq(i);
        if (!input.is(":visible")) {
          continue;
        }
        if (String(input.val()).length < 1) {
          if (String(simple_inputs_1.eq(i).val()).length < 1) {
            simple_inputs_1.eq(i).addClass("error");
          }
        } else {
          if (!validateEmail(input.val())) {
            input.addClass("error");
          }
          if (String(simple_inputs_1.eq(i).val()).length < 1) {
            simple_inputs_1.eq(i).addClass("error");
          }
        }
      }
    } else if (
      $(".remove_another").not(".keywords_hider .remove_another").length != 0
    ) {
      for (var i = 0, j = email_input_1.length; i < j; i++) {
        var input = email_input_1.eq(i);
        if (!input.is(":visible") || String(input.val()).length < 1) {
          continue;
        }
        if (!validateEmail(input.val())) {
          input.addClass("error");
        }
        if (String(simple_inputs_1.eq(i).val()).length < 1) {
          simple_inputs_1.eq(i).addClass("error");
        }
      }
    }
    if ($(".keywords_hider .remove_another").length == 0) {
      for (var i = 0, j = email_input_2.length; i < j; i++) {
        var input = email_input_2.eq(i);
        if (!input.is(":visible")) {
          continue;
        }
        if (String(input.val()).length < 1) {
          if (String(simple_inputs_2.eq(i).val()).length < 4) {
            simple_inputs_2.eq(i).addClass("error");
          }
        } else {
          if (!validateEmail(input.val())) {
            input.addClass("error");
          }
          if (String(simple_inputs_2.eq(i).val()).length < 4) {
            simple_inputs_2.eq(i).addClass("error");
          }
        }
      }
    } else if ($(".keywords_hider .remove_another").length != 0) {
      for (var i = 0, j = email_input_2.length; i < j; i++) {
        var input = email_input_2.eq(i);
        if (!input.is(":visible")) {
          continue;
        }
        if (String(input.val()).length < 1) {
          if (
            String(simple_inputs_2.eq(i).val()).length > 0 &&
            String(simple_inputs_2.eq(i).val()).length < 4
          ) {
            simple_inputs_2.eq(i).addClass("error");
          }
        } else {
          if (!validateEmail(input.val())) {
            input.addClass("error");
          }
          if (String(simple_inputs_2.eq(i).val()).length < 4) {
            simple_inputs_2.eq(i).addClass("error");
          }
        }
      }
    }

    if (form.find(".error").length == 0) {
      // set dropdown data
      $(".simple_dropdown").each(function() {
        var txt = $(this)
          .find('input[type="text"]')
          .val();
        var option = $(this)
          .find(".option")
          .filter(function() {
            return $(this).text() === txt;
          });

        $(this)
          .find('input[type="hidden"]')
          .val(option.length == 1 ? option.attr("data-value") : "");
      });

      return true;
    } else if (window.innerWidth < 1000) {
      var top =
        form
          .find(".error")
          .eq(0)
          .offset().top +
        $("main").scrollTop() -
        100;
      $("main").animate({ scrollTop: top }, 300);
    }
    return false;
  }
  $(document).on(
    "click",
    '.subscribe_page form input[type="text"], .subscribe_page form input[type="email"]',
    function() {
      $(this).removeClass("error");
    }
  );
  $('input[name="subscribe_by_keywords"]').on("change", function() {
    $(".keywords_hider").slideToggle(150);
  });
  form.find(".simple_dropdown").each(function() {
    var val = $(this)
      .find('input[type="hidden"]')
      .val();
    $(this)
      .find('input[type="text"]')
      .val(
        $(this)
          .find('.option[data-value="' + val + '"]')
          .html()
      );
  });
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function adjustScrollbar() {
  var dif = $("body").outerWidth() - $("#scroller").outerWidth();
  $("header").css({ width: "calc(100% - " + dif + "px)" });
  $(".cookies_pop").css({ width: "calc(100% - " + dif + "px)" });
}

$(".fb.button").on("click", function() {
  openWarning();
});

function openWarning() {
  var global_warning = $(".global_warning");
  if (global_warning.is(":visible")) {
    global_warning.slideToggle(150);
  } else {
    $("main").animate({ scrollTop: 0 }, 150, function() {
      global_warning.slideToggle(150);
    });
  }
}

function titlePage() {
  var main = $("main");
  var header = $("header");
  var counters_end = false;
  var number_holder_1 = $("section.numbers .number").eq(0);
  var number_holder_2 = $("section.numbers .number").eq(1);
  var number_holder_3 = $("section.numbers .number").eq(2);
  var max_number_1 = number_holder_1.text();
  var max_number_2 = number_holder_2.text();
  var max_number_3 = number_holder_3.text();
  number_holder_1.text("0,000");
  number_holder_2.text("0,000");
  number_holder_3.text("0,000");

  main.on("scroll", function() {
    var top = main.scrollTop();
    if (top > 20 && header_white == false) {
      header.addClass("white");
      header_white = true;
    }
    if (top < 20 && header_white) {
      header.removeClass("white");
      header_white = false;
    }
    if (top > 200 && counters_end == false) {
      countersFunc();
      counters_end = true;
    }
  });
  function activateSwipers() {
    var visuals = $(".swiper-container.visuals");
    var swiper = new Swiper(".hero .swiper-container.visuals", {
      effect: "fade",
      speed: 750,
      loop: true,
      disableOnInteraction: false,
      simulateTouch: false
    });
    var swiper_t = new Swiper(".hero .swiper-container.texts", {
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      speed: 2000,
      loop: true,
      pagination: {
        el: ".hero .swiper-pagination",
        clickable: true
      },
      disableOnInteraction: false,
      simulateTouch: false
    });
    swiper.on("slideChangeTransitionStart", function() {
      visuals.addClass("gone");
    });
    swiper.on("slideChangeTransitionEnd", function() {
      visuals.removeClass("gone");
    });
    swiper_t.on("slideChangeTransitionStart", function() {
      setTimeout(function() {
        var active = swiper_t.activeIndex;
        swiper.slideTo(active);
      }, 1300);
    });
  }
  var countersFunc = function() {
    var i = 0;
    var j = parseInt(max_number_1.split(" ")[0] + max_number_1.split(" ")[1]);
    var k = 0;
    var l = parseInt(max_number_2.split(" ")[0] + max_number_2.split(" ")[1]);
    var m = 0;
    var n = parseInt(max_number_3.split(" ")[0] + max_number_3.split(" ")[1]);
    var time = 1000;
    var duration_1 = (time / j) * 4;
    var duration_2 = (time / l) * 4;
    var duration_3 = (time / n) * 4;

    function rollFirst() {
      if (i <= j) {
        $({ to: 0 }).animate({ to: 1 }, duration_1, function() {
          number_holder_1.text(i);
          i += 163;
          rollFirst();
        });
      } else {
        number_holder_1.text(j);
      }
    }
    function rollSecond() {
      if (k <= l) {
        $({ to: 0 }).animate({ to: 1 }, duration_2, function() {
          number_holder_2.text(k);
          k += 163;
          rollSecond();
        });
      } else {
        number_holder_2.text(l);
      }
    }
    function rollThird() {
      if (m <= n) {
        $({ to: 0 }).animate({ to: 1 }, duration_3, function() {
          number_holder_3.text(m);
          m += 163;
          rollThird();
        });
      } else {
        number_holder_3.text(n);
      }
    }
    rollFirst();
    rollSecond();
    rollThird();
  };
  (function() {
    var tags = $(".hot_news .tag");
    for (var i = 0, j = tags.length; i < j; i++) {
      var tag = tags.eq(i);
      var color = tag.attr("data-color");
      tag.css({ background: color });
    }
  })();
  (function() {
    var section = document.querySelectorAll("section.hero")[0];
    var header = document.querySelectorAll("header")[0];
    var tl = new TimelineLite();
    tl.to(section, 0, { rotationZ: 0, z: 0, scale: 1 }, 0);
    tl.to(header, 0, { y: -78 }, 0);
    setTimeout(function() {
      tl.to(
        section,
        1,
        { rotationZ: 0, z: 0, scale: 1, ease: Power4.easeInOut },
        0
      );
      section.classList.add("opacity");
      tl.to(
        header,
        2,
        {
          y: 0,
          ease: Power4.easeInOut,
          onComplete: function() {
            activateSwipers();
          }
        },
        "-=1.6"
      );
    });
  })();
  $("section.photos .photo_of_the_day").on("click", function() {
    $(".pop.one_photo img").attr("src", "");
    var src = $(this).attr("data-published-src");
    if (src) {
      $(".pop.one_photo img").attr("src", src);
      openPop("one_photo");
    }
  });
  $("section.video .poster").on("click", function() {
    openPop("one_video");
  });
}

function loader(bool) {
  var loader = $("#loader");
  if (bool) {
    loader.css({ display: "flex" });
    setTimeout(function() {
      loader.addClass("show");
    });
  } else {
    loader.removeClass("show");
    setTimeout(function() {
      loader.hide();
    }, 510);
  }
}

function getDataFromIframe($iframe) {
  var inside = $iframe.contents().find("body");
  return inside.attr("data-html-output");
}
