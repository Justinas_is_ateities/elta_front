<!DOCTYPE HTML>
<html>
	<head>
		<title>Elta</title>	
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<link rel="stylesheet" type="text/css" media="screen" href="./css/main.css" />
<!-- 		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> -->
	</head>
	<body>
		
		<header>
			<a href="#" id="logo"></a>
			<div class="right_block">
				<ul class="main_menu">
					<li><a href="#">Pranešimai spaudai</a></li>
					<li><a href="#">Įvykusios konferencijos</a></li>
					<li><a href="#">fotobankas</a></li>
					<li><a href="#">apie elta</a></li>
					<li><a href="#">paslaugos</a></li>
					<li><a href="#">kontaktai</a></li>
				</ul>
				<div class="search">
					<div class="search_button"></div>
					<input type="text" name="search" placeholder="Search">
				</div>
				<a href="#" class="login button underlined">Prisijungti</a>
			</div>
			<div class="mobile_header">
				<div class="mobile_search_button"></div>
				<a href="#" class="login mobile_login"></a>
				<div class="burger">
					<div class="plank"></div>
					<div class="plank"></div>
					<div class="plank"></div>
				</div>
			</div>
		</header>
		
		<div class="mobile_menu">
			<ul>
				<li><a href="#">Pranešimai spaudai</a></li>
				<li><a href="#">Įvykusios konferencijos</a></li>
				<li><a href="#">fotobankas</a></li>
				<li><a href="#">apie elta</a></li>
				<li><a href="#">paslaugos</a></li>
				<li><a href="#">kontaktai</a></li>
			</ul>
		</div>

		<div class="mobile_search">
			<div class="search_button"></div>
			<input type="text" name="search" placeholder="Search">
		</div>
		
		<main>
			<div id="scroller">
			</div>
		</main>

 <!-- 		Auselė fronto peržiūrai - ištrinti! -->

<style type="text/css">
			#mmm {
				position: fixed;
				width: 100%;
				right: 0;
				top: 0;
				background: #0c0c0c;
				z-index: 200;
				padding: 15px 15px 15px 30px;
				top: 0;
				height: 100%;
				box-sizing: border-box;
				cursor: pointer;
				z-index: 1000;
			}
			#mmm:hover {
				right:0;
			}
			#iii {
				overflow-y: scroll;
				position: absolute;
				width: 100%;
				top: 30px;
				height: 85%;
			}
			#mmm a {
				text-decoration: none;
				color: #fff;
				font-size: 14px;
				display: block;
				color: #666;
				margin: 5px 0;
				transition: color 100ms ease-in-out;
			}
			#mmm a:hover {
				color: #fff;
			}
			#ooo {
				width: 30px;
				height: 29px;
				position: absolute;
				top: 0;
				left: -30px;
				background: #0c0c0c;
				border-bottom-left-radius: 2px;
				cursor: pointer;
			}
			#ooo:before {
				content: "";
				position: absolute;
				right: 7px;
				display: block;
				width: 16px;
				top: -1px;
				height: 0;
				box-shadow: 0 10px 0 1px #fff,0 16px 0 1px #fff,0 22px 0 1px #fff;
			}
			.subb {
				opacity: .5;
				color: #fff;
				font-weight: bold;
				margin: 18px 0;
				font-size: 70%;
			}
		</style>
		<?php
		echo '<div id="mmm"><div id="ooo"></div><div id="iii">';
		$url = 'http://'.$_SERVER['HTTP_HOST'];
		echo '<div class="subb">LANDING</div>';
		$files=scandir(getcwd().'/landing_templates');
		foreach($files as $k => $file)
		    if (strpos($file,'.php') !== false)
		    	if ($_SERVER['HTTP_HOST'] == 'localhost:82') {
		    		echo '<a href="'.$url.'/elta_front/landing_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	} else {
		    		echo '<a href="'.$url.'/landing_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	}
    	echo '<div class="subb">CLIENT</div>';
		$files=scandir(getcwd().'/client_templates');
		foreach($files as $k => $file)
		    if (strpos($file,'.php') !== false)
		    	if ($_SERVER['HTTP_HOST'] == 'localhost:82') {
		    		echo '<a href="'.$url.'/elta_front/client_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	} else {
		    		echo '<a href="'.$url.'/client_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	}
    	echo '<div class="subb">MANAGER</div>';
		$files=scandir(getcwd().'/manager_templates');
		foreach($files as $k => $file)
		    if (strpos($file,'.php') !== false)
		    	if ($_SERVER['HTTP_HOST'] == 'localhost:82') {
		    		echo '<a href="'.$url.'/elta_front/manager_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	} else {
		    		echo '<a href="'.$url.'/manager_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		    	}
		        
		// echo '<div class="subb">KLIENTAI</div>';
		// $files=scandir(getcwd().'/../client_templates');
		// foreach($files as $k => $file)
		//     if (strpos($file,'.php') !== false)
		//         echo '<a href="/igehub-fe/client_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';
		// echo '<div class="subb">ANALITIKAI</div>';
		// $files=scandir(getcwd().'/../analytic_templates');
		// foreach($files as $k => $file)
		//     if (strpos($file,'.php') !== false)
		//         echo '<a href="/igehub-fe/analytic_templates/'.$file.'">'.str_replace('.php', '', $file).'</a>';

		?>

<!-- 		Auselės pabaiga -->
			
	</body>
</html>