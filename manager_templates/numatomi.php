<?php include '../partials/head.php';?>
<?php include '../partials/header_employee.php';?>

<div class="page expected_page sidebar_layout">
	<?php include '../partials/global_warning.php';?>
	<section class="filter">
		<?php include '../partials/search_employee.php';?>
		<div class="button underlined detail_search">Detali paieška</div>
		<div class="opened_filter_buttons">
			<div class="button underlined close_search">Suskleisti paiešką</div>
			<div class="clear"></div>
			<div class="button underlined clear_filters">Išvalyti filtrą</div>
		</div>
	</section>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_employee.php';?>
			</div>
		</div>
		<div class="right layout">
			<div class="calendar">
				<div class="month">Kovas, 2018</div>
				<div class="day">
					<div class="day_controls">
						<div class="number">09</div>
						<div class="day_name">Pirmadienis</div>
						<label class="simple_checkbox smaller">
							<input type="checkbox" name="select_all">
							<span class="name">
								<span>Žymėti viską</span>
							</span>
						</label>
						<div class="button white">Eksportuoti</div>
					</div>
					<div class="day_list">
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
					</div>
				</div>
				<div class="day">
					<div class="day_controls">
						<div class="number">09</div>
						<div class="day_name">Pirmadienis</div>
						<label class="simple_checkbox smaller">
							<input type="checkbox" name="select_all">
							<span class="name">
								<span>Žymėti viską</span>
							</span>
						</label>
						<div class="button white">Eksportuoti</div>
					</div>
					<div class="day_list">
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
					</div>
				</div>
				<div class="month">Kovas, 2018</div>
				<div class="day">
					<div class="day_controls">
						<div class="number">09</div>
						<div class="day_name">Pirmadienis</div>
						<label class="simple_checkbox smaller">
							<input type="checkbox" name="select_all">
							<span class="name">
								<span>Žymėti viską</span>
							</span>
						</label>
						<div class="button white">Eksportuoti</div>
					</div>
					<div class="day_list">
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
					</div>
				</div>
				<div class="day">
					<div class="day_controls">
						<div class="number">09</div>
						<div class="day_name">Pirmadienis</div>
						<label class="simple_checkbox smaller">
							<input type="checkbox" name="select_all">
							<span class="name">
								<span>Žymėti viską</span>
							</span>
						</label>
						<div class="button white">Eksportuoti</div>
					</div>
					<div class="day_list">
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
						<div class="publication">
							<label class="simple_checkbox smaller">
								<input type="checkbox" name="select_this">
								<span class="name">
									<span></span>
								</span>
							</label>
							<div class="time">
								14:15
							</div>
							<div class="publication_name">
								<div class="expandable">
									<div class="inner">
										Po širdį draskančios šeimos dramos – dar neregėta atomazga: valstybę įpareigojo viešai atsiprašyti
									</div>
								</div>
								<div class="expand">
									<div class="on">Išskleisti</div>
									<div class="off">Suskleisti</div>
								</div>
							</div>
							<a href="#" class="edit_link">Redaguoti</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>