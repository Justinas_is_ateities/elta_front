<?php include '../partials/head.php';?>
<?php include '../partials/header_manager.php';?>

<div class="page manager_settings_page sidebar_layout no_filter">
	<?php include '../partials/global_warning.php';?>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_manager.php';?>
			</div>
		</div>
		<div class="right layout">
			<form id="manager_settings_form">
				<a href="#" class="go_back_to_list">atgal į sąrašą</a>
				<div class="deactivate_button_holder">
					<div class="deactivate active_account">
						<span class="when_active">Deaktyvuoti paskyrą</span>
						<span>Aktyvuoti paskyrą</span>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<div class="simple_input">
							<div class="label">Vardas</div>
							<input type="text" name="name" placeholder="Vardenis">
						</div>
						<div class="simple_input">
							<div class="label">Vartotojo vardas</div>
							<input type="text" name="nickname" placeholder="Slapyvardis">
						</div>
						<div class="simple_input">
							<div class="label">Slaptažodis</div>
							<input type="password" name="password" placeholder="ag7845vvn">
						</div>
						<div class="simple_input">
							<div class="label">Pareigos</div>
							<input type="text" name="position" placeholder="Žurnalistas">
						</div>
						<div class="simple_input">
							<div class="label">Kontaktinis telefonas</div>
							<input type="text" name="phone" placeholder="+370 6 88 55552">
						</div>
					</div>
					<div class="col">
						<div class="simple_input">
							<div class="label">Pavardė</div>
							<input type="text" name="surname" placeholder="Pavardenis">
						</div>
						<div class="autocomplete_dropdown">
							<div class="label">Įmonės pavadinimas</div>
							<div class="planks">
								<div class="plank"></div>
								<div class="plank"></div>
								<div class="plank"></div>
							</div>
							<select class="for_desktop" name="company_name" data-placeholder="UAB „Žurnalistai rašo“" tabindex="1">
								<option value=""></option>
								<option value="UAB „Gerą girą gera gert“">UAB „Gerą girą gera gert“</option>
								<option value="UAB „Paršiukai stumia tanką“">UAB „Paršiukai stumia tanką“</option>
								<option value="AB „Šantažas“">AB „Šantažas“</option>
							</select>
							<!-- Cia dublikatas del mobile -->
							<select name="company_name_2" class="for_mobile not_touched">
								<option value="UAB „Gerą girą gera gert“">UAB „Gerą girą gera gert“</option>
								<option value="UAB „Paršiukai stumia tanką“">UAB „Paršiukai stumia tanką“</option>
								<option value="AB „Šantažas“">AB „Šantažas“</option>
							</select>
						</div>
						<div class="simple_input">
							<div class="label">Pakartokite slaptažodį</div>
							<input type="password" name="repeat_password" placeholder="ag7845vvn">
						</div>
						<div class="simple_input">
							<div class="label">El. paštas</div>
							<input type="email" name="email" placeholder="vardenis@elpastas.lt">
						</div>
					</div>
				</div>
				<div class="simple_textarea">
					<div class="label">Pastabos ir komentarai</div>
					<textarea name="comments" placeholder="Jūsų komentarai"></textarea>
				</div>
				<div class="line"></div>
				<div class="label">Vartotojo tipas</div>
				<label class="simple_checkbox">
					<input type="checkbox" name="group_user">
					<span class="name">
						<span>Grupinis vartotojas</span>
					</span>
				</label>
				<div class="line"></div>
				<div class="label">Pranešimai spaudai</div>
				<div class="row">
					<div class="col">
						<label class="simple_checkbox">
							<input type="checkbox" name="press_releases_publish">
							<span class="name">
								<span>Pranešimų spaudai publikavimas</span>
							</span>
						</label>
					</div>
					<div class="col">
						<label class="simple_checkbox">
							<input type="checkbox" name="calaendar_publish">
							<span class="name">
								<span>Kalendoriaus publikavimas</span>
							</span>
						</label>
					</div>
				</div>
				<div class="line"></div>
				<div class="label">IP apribojimai</div>
				<div class="ips_ranges hiddeable">
					<div class="row full">
						<div class="simple_input">
							<input type="text" name="ip_range_1" placeholder="192.168.0.1-192.168.120.255">
						</div>
					</div>
					<div class="add_another_removable">Pridėti IP adresų ruožą</div>
				</div>
				<div class="line"></div>
				<div class="label">Priklauso Įmonei</div>
				<div class="manager_list">
					<div class="heading">
						<div class="col">Įmonės pavadinimas</div>
						<div class="col">Galioja iki</div>
						<div class="col"></div>
					</div>
					<div class="member">
						<div class="col">UAB "Vardenis Pavardenis"</div>
						<div class="col">2018-04-18</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>

					<!-- Nesu tikras, ką reiškia kai vartotojas yra atvaizduojamas rausvai. Bet toks scenarijus nupieštas -->

					<div class="member pink">
						<div class="col">UAB "Vardenis Pavardenis"</div>
						<div class="col">2018-04-18</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>

					<!-- ... -->
					
					<div class="member">
						<div class="col">UAB "Vardenis Pavardenis"</div>
						<div class="col">2018-04-18</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>
					<div class="member">
						<div class="col">UAB "Vardenis Pavardenis"</div>
						<div class="col">2018-04-18</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>
					<div class="member">
						<div class="col">UAB "Vardenis Pavardenis"</div>
						<div class="col">2018-04-18</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>
				</div>
				<div class="form_buttons">
					<div class="error_message">Something went wrong!</div>
					<div class="delete">Trinti</div>
					<button class="button blue" type="submit">Išsaugoti</button>
				</div>
				<a href="#" class="go_back_to_list bottom">atgal į sąrašą</a>
			</form>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>