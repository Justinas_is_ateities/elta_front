<?php include '../partials/head.php';?>
<?php include '../partials/header_employee.php';?>

<div class="page photos_page manager_view">
	<?php include '../partials/global_warning.php';?>
	<section class="filter">
		<?php include '../partials/search_client_without_checkboxes.php';?>
		<div class="button underlined detail_search">Detali paieška</div>
		<div class="opened_filter_buttons">
			<div class="button underlined close_search">Suskleisti paiešką</div>
			<div class="clear"></div>
			<div class="button underlined clear_filters">Išvalyti filtrą</div>
		</div>
		<div class="right_box">
			<div class="label">Atvaizdavimas</div>
			<div class="view current" data-view="columns"></div>
			<div class="view" data-view="info"></div>
		</div>
	</section>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_employee.php';?>
			</div>
		</div>
		<div class="right layout">
			<div class="photos_holder">
				<div class="scroller_holder">
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="buttons absolute">
							<span class="edit">Versti</span>
							<span class="edit open_edit_pop">Redaguoti</span>
							<span class="delete"></span>
						</div>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
						<div class="functional_buttons">
							<span>
								<span class="edit">Versti</span>
								<span class="edit open_edit_pop">Redaguoti</span>
								<span class="delete">ištrinti</span>
							</span>
						</div>
					</div>
					<div class="pager_holder">
						<div class="simple_pager">
							<a href="#" class="page_link">1</a>
							<a href="#" class="page_link current">2</a>
							<a href="#" class="page_link">3</a>
							<span>...</span>
							<a href="#" class="page_link">7</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>