<?php include '../partials/head.php';?>
<?php include '../partials/header_manager.php';?>

<div class="page manager_settings_page sidebar_layout no_filter">
	<?php include '../partials/global_warning.php';?>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_manager.php';?>
			</div>
		</div>
		<div class="right layout">
			<form id="manager_settings_form">
				<a href="#" class="go_back_to_list">atgal į sąrašą</a>
				<div class="deactivate_button_holder">
					<div class="deactivate active_account">
						<span class="when_active">Deaktyvuoti paskyrą</span>
						<span>Aktyvuoti paskyrą</span>
					</div>
				</div>
				<label class="simple_checkbox">
					<input type="checkbox" name="test_user">
					<span class="name">
						<span>Bandomasis vartotojas</span>
					</span>
				</label>
				<div class="row">
					<div class="col">
						<div class="simple_input">
							<div class="label">Įmonės pavadinimas</div>
							<input type="text" name="company" placeholder="UAB „Žurnalistai rašo“">
						</div>
						<div class="simple_input">
							<div class="label">Adresas</div>
							<input type="text" name="address" placeholder="Gatvių g. 0-11, Kaunas">
						</div>
					</div>
					<div class="col">
						<div class="simple_input">
							<div class="label">El. paštas</div>
							<input type="email" name="email" placeholder="vardenis@adresas.lt">
						</div>
						<div class="simple_input">
							<div class="label">Kontaktinis telefonas</div>
							<input type="text" name="phone" placeholder="+370 6 88 55552">
						</div>
					</div>
				</div>
				<div class="simple_textarea">
					<div class="label">Pastabos ir komentarai</div>
					<textarea name="comments" placeholder="Jūsų komentarai"></textarea>
				</div>
				<label class="simple_checkbox">
					<input type="checkbox" name="subscribe">
					<span class="name">
						<span>Leisti prenumeruotis naujienas el. paštu</span>
					</span>
				</label>
				<div class="line"></div>
				<div class="subscribe_hidder">
					<div class="row">
						<div class="col">
							<div class="label">Prenumerata</div>
							<div class="simple_input icon_dates">
								<input data-empty="true" type="text" name="subscription_dates" placeholder="Pasirinkite laikotarpį" value="">
							</div>
						</div>
						<div class="col">
							<div class="label">Naujienų vėlavimas</div>
							<div class="simple_dropdown bigger">
								<input type="text" name="news_lating" disabled="" placeholder="Valandos" value="">
								<div class="content">
									<div class="scroller_holder">
										<div class="option">0</div>
										<div class="option">1</div>
										<div class="option">2</div>
										<div class="option">3</div>
										<div class="option">4</div>
										<div class="option">5</div>
										<div class="option">6</div>
										<div class="option">7</div>
										<div class="option">8</div>
										<div class="option">9</div>
										<div class="option">10</div>
										<div class="option">11</div>
										<div class="option">12</div>
										<div class="option">13</div>
										<div class="option">14</div>
										<div class="option">15</div>
										<div class="option">16</div>
										<div class="option">17</div>
										<div class="option">18</div>
										<div class="option">19</div>
										<div class="option">20</div>
										<div class="option">21</div>
										<div class="option">22</div>
										<div class="option">23</div>
										<div class="option">24</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
				</div>
				<div class="label">Priskirta ELTA naujienų kategorija</div>
				<div class="row">
					<div class="col multicheckboxes_holder">
						<div class="small_label">Lietuvių kalba</div>
						<div class="multirow">
							<div class="multicolumn heading_1"></div>
							<div class="multicolumn heading_1">Lietuvos</div>
							<div class="multicolumn heading_1">Užsienio</div>
						</div>
						<div class="multirow">
							<div class="multicolumn heading_2"></div>
							<div class="multicolumn heading_2"><span>Naujienos</span><span>Vaizdai</span></div>
							<div class="multicolumn heading_2"><span>Naujienos</span><span>Vaizdai</span></div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Politika</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_local_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_local_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_foreign_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_foreign_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Ūkis</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_local_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_local_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_foreign_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_foreign_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Teisėtvarka</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_local_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_local_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_foreign_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_foreign_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Kultūra</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_local_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_local_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_foreign_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_foreign_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Sportas</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_local_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_local_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_foreign_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_foreign_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Susisiekimas</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_local_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_local_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_foreign_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_foreign_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Eltos gidas</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_local_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_local_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_foreign_news_lt">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_foreign_views_lt">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
					</div>
					<div class="col multicheckboxes_holder">
						<div class="small_label">Anglų kalba</div>
						<div class="multirow">
							<div class="multicolumn heading_1"></div>
							<div class="multicolumn heading_1">Lietuvos</div>
							<div class="multicolumn heading_1">Užsienio</div>
						</div>
						<div class="multirow">
							<div class="multicolumn heading_2"></div>
							<div class="multicolumn heading_2"><span>Naujienos</span><span>Vaizdai</span></div>
							<div class="multicolumn heading_2"><span>Naujienos</span><span>Vaizdai</span></div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Politika</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_local_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_local_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_foreign_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_foreign_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Ūkis</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_local_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_local_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_foreign_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_foreign_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Teisėtvarka</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_local_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_local_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_foreign_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_foreign_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Kultūra</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_local_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_local_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_foreign_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_foreign_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Sportas</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_local_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_local_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_foreign_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_foreign_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Susisiekimas</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_local_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_local_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_foreign_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_foreign_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Eltos gidas</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_local_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_local_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_foreign_news_en">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_foreign_views_en">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col multicheckboxes_holder">
						<div class="small_label">Rusų kalba</div>
						<div class="multirow">
							<div class="multicolumn heading_1"></div>
							<div class="multicolumn heading_1">Lietuvos</div>
							<div class="multicolumn heading_1">Užsienio</div>
						</div>
						<div class="multirow">
							<div class="multicolumn heading_2"></div>
							<div class="multicolumn heading_2"><span>Naujienos</span><span>Vaizdai</span></div>
							<div class="multicolumn heading_2"><span>Naujienos</span><span>Vaizdai</span></div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Politika</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_local_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_local_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_foreign_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="politics_foreign_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Ūkis</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_local_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_local_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_foreign_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="economy_foreign_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Teisėtvarka</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_local_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_local_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_foreign_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="law_foreign_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Kultūra</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_local_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_local_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_foreign_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="culture_foreign_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Sportas</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_local_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_local_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_foreign_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="sport_foreign_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Susisiekimas</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_local_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_local_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_foreign_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="transport_foreign_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">Eltos gidas</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_local_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_local_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_foreign_news_ru">
										<span class="name"></span>
									</label>
								</span>
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="elta_guide_foreign_views_ru">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
					</div>
					<div class="col multicheckboxes_holder">
						<div class="small_label">Užsienio agentūrų naujienos</div>
						<div class="multirow">
							<div class="multicolumn">AFP</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="foreign_agencies_afp">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">DPA</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="foreign_agencies_dpa">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="multirow">
							<div class="multicolumn">TASS</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="foreign_agencies_tass">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
						<div class="small_label">Užsienio agentūrų nuotraukos</div>
						<div class="multirow">
							<div class="multicolumn">EPA</div>
							<div class="multicolumn">
								<span>
									<label class="simple_checkbox smaller">
										<input type="checkbox" name="foreign_agencies_epa">
										<span class="name"></span>
									</label>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="label">Pranešimai spaudai</div>
				<div class="row">
					<div class="col">
						<label class="simple_checkbox">
							<input type="checkbox" name="press_release_publication">
							<span class="name">Pranešimų spaudai publikavimas</span>
						</label>
						<label class="simple_checkbox">
							<input type="checkbox" name="calendar_publication">
							<span class="name">Kalendoriaus publikavimas</span>
						</label>
						<div class="simple_input">
							<div class="label">Pranešimų spaudai limitas (per metus)</div>
							<input type="number" name="press_release_limit" placeholder="100">
						</div>
					</div>
					<div class="col">
						<label class="simple_checkbox">
							<input type="checkbox" name="show_press_releases_in_main_list">
							<span class="name">Rodyti pranešimus spaudai bendrame naujienų sraute</span>
						</label>
						<div class="show_press_releases_in_main_list_hiddeable">
							<div class="simple_dropdown bigger">
								<input type="text" name="press_release_theme" disabled="" placeholder="Pasirinkite temą" value="">
								<div class="content">
									<div class="scroller_holder">
										<div class="option">Visos naujienos</div>
										<div class="option">Politika</div>
										<div class="option">Ekonomika</div>
										<div class="option">Teisėtvarka</div>
										<div class="option">Soprtas</div>
									</div>
								</div>
							</div>
							<div class="simple_dropdown bigger">
								<input type="text" name="press_release_subtheme" disabled="" placeholder="Pasirinkite potemę" value="">
								<div class="content">
									<div class="scroller_holder">
										<div class="option">Visos naujienos</div>
										<div class="option">Politika</div>
										<div class="option">Ekonomika</div>
										<div class="option">Teisėtvarka</div>
										<div class="option">Soprtas</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="label">Archyvas (dienos)</div>
				<div class="row">
					<div class="col">
						<div class="simple_input">
							<input type="number" name="archive_times" placeholder="365">
						</div>
						<div class="simple_input">
							<div class="label">Nuotraukų limitas (per mėnesį)</div>
							<input type="number" name="photos_limit" placeholder="50">
							<div class="example">Nuotraukų likutis parsisiuntimui: <strong>30</strong></div>
						</div>
					</div>
					<div class="col">
						<div class="simple_input icon_dates">
							<input type="text" data-empty="true" name="archive_dates" placeholder="Pasirinkite laikotarpį" value="">
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="label naming">Naujienų eksportavimas</div>
				<label class="simple_checkbox">
					<input type="checkbox" name="export_news">
					<span class="name">
						<span>Naujienos</span>
					</span>
				</label>
				<div class="export_hiddeable">
					<div class="label"></div>
					<div class="row">
						<div class="col">
							<div class="simple_dropdown bigger">
								<input type="text" name="export_type" disabled="" placeholder="" value="FTP">
								<div class="content export_type">
									<div class="scroller_holder">
										<div class="option current" data-selection="ftp">FTP</div>
										<div class="option" data-selection="email">El. paštas</div>
									</div>
								</div>
							</div>
							<div class="ftp_hiddeable">
								<div class="simple_input">
									<input type="text" name="ftp_server_address" placeholder="FTP serverio adresas">
								</div>
								<div class="simple_input">
									<input type="text" name="user_name" placeholder="Vartotojo vardas">
								</div>
							</div>
							<div class="simple_dropdown bigger format_code_hiddeable">
								<input type="text" name="export_format_email_code" disabled="" placeholder="Pasirinkite siunčiamos informacijos koduotę" value="">
								<div class="content">
									<div class="scroller_holder">
										<div class="option">UTF-8</div>
										<div class="option">Sena koduotė</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="simple_dropdown bigger email_hiddeable">
								<input type="text" name="export_format_email" disabled="" placeholder="Pasirinkite siunčiamos informacijos formatą" value="">
								<div class="content export_format_email">
									<div class="scroller_holder">
										<div class="option" data-selection="email_xml">XML (UTF-8)</div>
										<div class="option" data-selection="email_html">HTML (UTF-8)</div>
										<div class="option" data-selection="email_plain">PLAIN TEXT</div>
									</div>
								</div>
							</div>
							<div class="simple_dropdown bigger ftp_hiddeable">
								<input type="text" name="export_format_ftp" disabled="" placeholder="Pasirinkite siunčiamos informacijos formatą" value="">
								<div class="content export_format">
									<div class="scroller_holder">
										<div class="option" data-selection="xml">XML (UTF-8)</div>
										<div class="option" data-selection="plain">PLAIN TEXT (UTF-8)</div>
									</div>
								</div>
							</div>
							<div class="ftp_hiddeable">
								<div class="simple_input">
									<input type="text" name="ftp_server_port" placeholder="Prievadas 21">
								</div>
								<div class="simple_input">
									<input type="text" name="user_password" placeholder="Slaptažodis">
								</div>
							</div>
						</div>
					</div>
					<div class="label">Turinys</div>
					<div>
						<div class="row mb20">
							<div class="col">
								<div class="simple_dropdown bigger">
									<input type="text" name="content_category" disabled="" placeholder="Pasirinkite temą" value="">
									<div class="content">
										<div class="scroller_holder">
											<div class="option">Politika</div>
											<div class="option">Verslas</div>
											<div class="option">Teisėtvarka</div>
											<div class="option">Kultūra</div>
											<div class="option">Sportas</div>
											<div class="option">Sveikata</div>
										</div>
									</div>
								</div>
								<div class="simple_dropdown bigger">
									<input type="text" name="content_country" disabled="" placeholder="Pasirinkite šalį" value="">
									<div class="content">
										<div class="scroller_holder">
											<div class="option">Lietuva</div>
											<div class="option">Užsienis</div>
										</div>
									</div>
								</div>
								<div class="simple_dropdown bigger">
									<input type="text" name="foreign_agencies" disabled="" placeholder="Užsienio agentūros" value="">
									<div class="content">
										<div class="scroller_holder">
											<div class="option">Politika</div>
											<div class="option">Verslas</div>
											<div class="option">Teisėtvarka</div>
											<div class="option">Kultūra</div>
											<div class="option">Sportas</div>
											<div class="option">Sveikata</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="simple_dropdown bigger">
									<input type="text" name="content_subcategory" disabled="" placeholder="Pasirinkite potemę" value="">
									<div class="content">
										<div class="option">Option 1</div>
										<div class="option">Option 2</div>
										<div class="option">Option 3</div>
										<div class="option">Option 4</div>
										<div class="option">Option 5</div>
									</div>
								</div>
								<div class="simple_dropdown bigger">
									<input type="text" name="content_language" disabled="" placeholder="Pasirinkite kalbą" value="">
									<div class="content">
										<div class="option">Lietuvių</div>
										<div class="option">Anglų</div>
										<div class="option">Rusų</div>
									</div>
								</div>
								<div class="simple_input email_hiddeable">
									<input type="text" name="emails" placeholder="El. paštas">
									<div class="example">Pvz. labas@elta.lt, info@elta.lt, epastas@elta.lt</div>
								</div>
							</div>
						</div>
						<div class="add_another">Pridėti</div>
					</div>
				</div>
				<div class="clear"></div>
				<label class="simple_checkbox">
					<input type="checkbox" name="export_photos">
					<span class="name">
						<span>Nuotraukos</span>
					</span>
				</label>
				<div class="export_photos_hiddeable">
					<div class="label"></div>
					<div class="row">
						<div class="col">
							<div class="simple_input">
								<input type="text" name="photos_ftp_server_address" placeholder="FTP serverio adresas">
							</div>
							<div class="simple_input">
								<input type="text" name="photos_user_name" placeholder="Vartotojo vardas">
							</div>
						</div>
						<div class="col">
							<div class="ftp_hiddeable">
								<div class="simple_input">
									<input type="text" name="photos_ftp_server_port" placeholder="Prievadas 21">
								</div>
								<div class="simple_input">
									<input type="text" name="photos_user_password" placeholder="Slaptažodis">
								</div>
							</div>
						</div>
					</div>
					<div class="label">Turinys</div>
					<div>
						<div class="row mb20">
							<div class="col">
								<div class="simple_dropdown bigger">
									<input type="text" name="photos_content_category" disabled="" placeholder="Pasirinkite temą" value="">
									<div class="content">
										<div class="option">Politika</div>
										<div class="option">Verslas</div>
										<div class="option">Teisėtvarka</div>
										<div class="option">Kultūra</div>
										<div class="option">Sportas</div>
										<div class="option">Sveikata</div>
									</div>
								</div>
								<div class="simple_dropdown bigger">
									<input type="text" name="photos_content_country" disabled="" placeholder="Pasirinkite šalį" value="">
									<div class="content">
										<div class="option">Lietuva</div>
										<div class="option">Užsienis</div>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="simple_dropdown bigger">
									<input type="text" name="photos_content_subcategory" disabled="" placeholder="Pasirinkite potemę" value="">
									<div class="content">
										<div class="option">Option 1</div>
										<div class="option">Option 2</div>
										<div class="option">Option 3</div>
										<div class="option">Option 4</div>
										<div class="option">Option 5</div>
									</div>
								</div>
								<div class="simple_dropdown bigger">
									<input type="text" name="photos_content_language" disabled="" placeholder="Pasirinkite kalbą" value="">
									<div class="content">
										<div class="option">Lietuvių</div>
										<div class="option">Anglų</div>
										<div class="option">Rusų</div>
									</div>
								</div>
							</div>
						</div>
						<div class="add_another minus_margin">Pridėti</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="label">Pakartotinis naujienų eksportavimas</div>
				<div class="row">
					<div class="col">
						<div class="simple_input icon_dates">
							<input type="text" name="export_dates" placeholder="Pasirinkite laikotarpį" value="">
						</div>
					</div>
					<div class="col">
						<div class="button white export_button">Eksportuoti</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="label">Įmonei priskirti vartotojai</div>
				<div class="manager_list">
					<div class="heading">
						<div class="col">Vardas, Pavardė</div>
						<div class="col">El. paštas</div>
						<div class="col"></div>
					</div>
					<div class="member">
						<div class="col">Vardenis Pavardenis</div>
						<div class="col">vardenis@adresas.lt</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>

					<!-- Nesu tikras, ką reiškia kai vartotojas yra atvaizduojamas rausvai. Bet toks scenarijus nupieštas -->

					<div class="member pink">
						<div class="col">Vardenis Pavardenis</div>
						<div class="col">vardenis@adresas.lt</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>

					<!-- ... -->
					
					<div class="member">
						<div class="col">Vardenis Pavardenis</div>
						<div class="col">vardenis@adresas.lt</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>
					<div class="member">
						<div class="col">Vardenis Pavardenis</div>
						<div class="col">vardenis@adresas.lt</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>
					<div class="member">
						<div class="col">Vardenis Pavardenis</div>
						<div class="col">vardenis@adresas.lt</div>
						<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
					</div>
				</div>
				<div class="form_buttons">
					<div class="error_message">Something went wrong!</div>
					<div class="delete">Trinti</div>
					<button class="button blue" type="submit">Išsaugoti</button>
				</div>
				<a href="#" class="go_back_to_list bottom">atgal į sąrašą</a>
			</form>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>