<?php include '../partials/head.php';?>
<?php include '../partials/header_employee.php';?>

<div class="page redactor_page sidebar_layout no_filter no_sidebar">
	<?php include '../partials/global_warning.php';?>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_employee.php';?>
			</div>
		</div>
		<div class="right layout">
			<form id="redactor_form">
				<a href="#" class="go_back_to_list">atgal į sąrašą</a>
				<div class="subheader">
					<div class="button history underlined">Veiksmų istorija</div>
					<div class="add_manager_holder">
						<div class="button blue add_manager">Priskirti darbuotojui</div>
						<div class="added_manager">Priskirta: <span>Vardenis Pavardenis</span></div>	
					</div>
				</div>
				<div class="simple_input">
					<div class="label">Pavadinimas</div>
					<input type="text" spellcheck="true" name="name">
				</div>
				<div class="line"></div>
				<div class="label">Data ir laikas</div>
				<label class="simple_checkbox inline postpone">
					<input type="checkbox" name="postpone">
					<span class="name">
						<span>Atidėti publikavimą</span>
					</span>
				</label>
				<div class="time_pickers">
					<div>
						<div class="simple_input inline icon_dates">
							<input class="datepicker" type="text" name="postpone_date">
						</div>
						<div class="simple_input inline hours timer">
							<input type="number" name="postpone_hours" value="12" min="0" max="24">
							<span>val.</span>
						</div>
						<div class="simple_input inline minutes timer">
							<input type="number" name="postpone_minutes" value="00" min="0" max="59">
							<span>min.</span>
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="row">
					<div class="checkboxes_box">
						<div class="label">Šalis:</div>
						<label class="simple_radio">
							<input type="radio" name="country" checked>
							<span class="name">
								<span>Lietuva</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="country">
							<span class="name">
								<span>Užsienis</span>
							</span>
						</label>
					</div>
					<div class="checkboxes_box">
						<div class="label">Kalba:</div>
						<label class="simple_radio">
							<input type="radio" name="language" checked>
							<span class="name">
								<span>Lietuvių</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="language">
							<span class="name">
								<span>Anglų</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="language">
							<span class="name">
								<span>Rusų</span>
							</span>
						</label>
					</div>
				</div>
				<div class="line"></div>
				<div class="row">
					<div class="col">
						<div class="label">Tema</div>
						<div class="simple_dropdown bigger">
							<input type="text" name="topic" disabled="" placeholder="Pasirinkite temą" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="label">Potemė</div>
						<div class="simple_dropdown bigger">
							<input type="text" name="subtopic" disabled="" placeholder="Pasirinkite potemę" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="add_another">Pridėti</div>
				<div class="line"></div>
				<div class="row full">
					<div class="label">Būsena</div>
					<label class="simple_checkbox">
						<input type="checkbox" name="supplemented">
						<span class="name">
							<span>Papildyta</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="important">
						<span class="name">
							<span>Svarbu</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="revised">
						<span class="name">
							<span>Patikslinta</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="interview">
						<span class="name">
							<span>Interviu</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="renewed">
						<span class="name">
							<span>Atnaujinta</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="comment">
						<span class="name">
							<span>Komentaras</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="topic">
						<span class="name">
							<span>Tema</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="edited">
						<span class="name">
							<span>Taisyta</span>
						</span>
					</label>
				</div>
				<div class="line"></div>
				<div class="button_space">
					<div class="add_more_news">Pridėti naujienas</div>
				</div>
				<div class="editor_iframe">
					<input type="text" name="editor" value="Testinis tekstas" data-texts='["Vienas tekstas", "Antras tekstas", "Trečias tekstas"]' data-oversize="Paveikslėlio dydis negali viršyti 2Mb" />
					<iframe tabindex="0" src="http://localhost:82/elta_front/editor/"></iframe>
				</div>
				<div class="line"></div>
				<div class="editor_iframe">
					<input type="text" name="editor" value="Testinis tekstas antras" data-texts='["Vienas tekstas", "Antras tekstas", "Trečias tekstas"]' data-oversize="Paveikslėlio dydis negali viršyti 2Mb" />
					<iframe src="http://localhost:82/elta_front/editor/"></iframe>
				</div>
				<div class="editor_iframe">
					<input type="text" name="editor" value="Testinis tekstas trecias" data-texts='["Vienas tekstas", "Antras tekstas", "Trečias tekstas"]' data-oversize="Paveikslėlio dydis negali viršyti 2Mb" />
					<iframe src="http://localhost:82/elta_front/editor/"></iframe>
				</div>
				<div class="line"></div>
				<div class="editor_iframe">
					<input type="text" name="editor" value="Testinis tekstas ketvirtas" data-texts='["Vienas tekstas", "Antras tekstas", "Trečias tekstas"]' data-oversize="Paveikslėlio dydis negali viršyti 2Mb" />
					<iframe src="http://localhost:82/elta_front/editor/"></iframe>
				</div>
				<div class="editor_iframe">
					<input type="text" name="editor" value="Testinis tekstas penktas" data-texts='["Vienas tekstas", "Antras tekstas", "Trečias tekstas"]' data-oversize="Paveikslėlio dydis negali viršyti 2Mb" />
					<iframe src="http://localhost:82/elta_front/editor/"></iframe>
				</div>
				<div class="line"></div>
				<div class="editor_iframe">
					<input type="text" name="editor" value="" data-texts='["Vienas tekstas", "Antras tekstas", "Trečias tekstas"]' data-oversize="Paveikslėlio dydis negali viršyti 2Mb" />
					<iframe src="http://localhost:82/elta_front/editor/"></iframe>
				</div>
				<div class="line"></div>
				<div class="simple_input">
					<div class="label">Raktažodžiai</div>
					<input type="text" spellcheck="true" name="keywords">
				</div>
				<div class="line"></div>
				<div class="label">Autorius</div>
				<div class="row">
					<div class="col">
						<div class="autocomplete_dropdown bigger">
							<div class="planks">
								<div class="plank"></div>
								<div class="plank"></div>
								<div class="plank"></div>
							</div>
							<select class="for_desktop" name="author" data-placeholder="Pasirinkite autorių" tabindex="1">
								<option value=""></option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
							</select>
							<!-- Cia dublikatas del mobile -->
							<select name="author_2" class="for_mobile not_touched">
								<option value="" disabled selected>Pasirinkite autorių</option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
							</select>
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="label">Priskirti nuotrauką</div>
				<div class="added_photos">

					<!-- Šitas pirmas .photo_block iskviečia popupą, tai jis turi būti visada, o kiti .photo_block yra šiaip suhardkodinti dėl vaizdom kad matytųsi kaip turi išsivest, bet aš nerašiau javascripto jų sukūrimo pasirinkus, nes ten popupe yra pageris, tai vistiek reikės ajaxo tam -->

					<div class="photo_block">
						<div class="photo_holder add_photo">
							<span>Pasirinkite nuotrauką</span>
						</div>
					</div>
					<div class="photo_block" data-comment="">
						<div class="photo_holder">
							<div class="buttons">
								<div class="info extra_tooltip">
									<div class="tooltip">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
									</div>
								</div>
								<span class="edit open_edit_news_photo">Redaguoti</span>
							</div>
							<div class="remove"></div>
							<div class="img_holder">
								<img src="../media/images/thumb_photo_1.jpg" alt="">
							</div>
						</div>
						<div class="name">S. Skvernelis / Irmanto Gelūno nuotr.</div>
					</div>
					<div class="photo_block" data-comment="">
						<div class="photo_holder">
							<div class="buttons">
								<div class="info extra_tooltip">
									<div class="tooltip">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
									</div>
								</div>
								<span class="edit open_edit_news_photo">Redaguoti</span>
							</div>
							<div class="remove"></div>
							<div class="img_holder">
								<img src="../media/images/thumb_photo_2.jpg" alt="">
							</div>
						</div>
						<div class="name">R. Karbauskis / Irmanto Gelūno nuotr.</div>
					</div>
					<div class="photo_block" data-comment="">
						<div class="photo_holder">
							<div class="buttons">
								<div class="info extra_tooltip">
									<div class="tooltip">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
									</div>
								</div>
								<span class="edit open_edit_news_photo">Redaguoti</span>
							</div>
							<div class="remove"></div>
							<div class="img_holder">
								<img src="../media/images/thumb_photo_1.jpg" alt="">
							</div>
						</div>
						<div class="name">S. Skvernelis / Irmanto Gelūno nuotr.</div>
					</div>
					<div class="photo_block" data-comment="">
						<div class="photo_holder">
							<div class="buttons">
								<div class="info extra_tooltip">
									<div class="tooltip">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
									</div>
								</div>
								<span class="edit open_edit_news_photo">Redaguoti</span>
							</div>
							<div class="remove"></div>
							<div class="img_holder">
								<img src="../media/images/thumb_photo_2.jpg" alt="">
							</div>
						</div>
						<div class="name">R. Karbauskis / Irmanto Gelūno nuotr.</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="form_buttons">
					<div class="error_message">Something went wrong!</div>
					<div class="delete">Trinti</div>
					<button class="button white big" type="submit">Išsaugoti tarp ruošiamų</button>
					<button class="button blue" type="submit">Išsaugoti ir publikuoti</button>
				</div>
				<a href="#" class="go_back_to_list bottom">atgal į sąrašą</a>
			</form>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>