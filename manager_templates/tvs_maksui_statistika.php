<?php include '../partials/head.php';?>
<?php include '../partials/header_employee.php';?>

<div class="page cms_page news_page no_filter">
	<?php include '../partials/global_warning.php';?>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<div class="toggler">
					<div class="toggler_head active" data-sector="all">
						<span>
							<span>Statistika</span>
						</span>
					</div>
					<div class="toggler_head" data-sector="all">
						<span>
							<span>Viskas</span>
						</span>
					</div>
					<div class="toggler_head" data-sector="my_messages">
						<span>
							<span>Priskirta man</span>

							<!-- Šitą tik tuo atveju jeigu yra žinučių "priskirta man" -->
							<span class="has_messages">2</span>
							<!-- ... -->

						</span>
						<div class="opener"><span></span></div>
					</div>
					<div class="toggler_body">
						<div class="subsector" data-subsector="all_politics">
							<span>
								<span>VISOS POLITIKOS NAUJIENOS</span>
							</span>
						</div>
						<div class="subsector" data-subsector="foreign_politics">
							<span>
								<span>Užsienio politika</span>
							</span>
						</div>
						<div class="subsector" data-subsector="lithuanian_politics">
							<span>
								<span>Lietuvos politika</span>
							</span>
						</div>
					</div>
					<div class="toggler_head" data-sector="businness">
						<span>
							<span>Politika</span>
						</span>
						<div class="opener"><span></span></div>
					</div>
					<div class="toggler_body">
						<div class="subsector" data-subsector="all_politics">
							<span>
								<span>VISOS POLITIKOS NAUJIENOS</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
						<div class="subsector" data-subsector="foreign_politics">
							<span>
								<span>Užsienio politika</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
						<div class="subsector" data-subsector="lithuanian_politics">
							<span>
								<span>Lietuvos politika</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
					</div>
					<div class="toggler_head" data-sector="law">
						<span>
							<span>Politika</span>
						</span>
						<div class="opener"><span></span></div>
					</div>
					<div class="toggler_body">
						<div class="subsector" data-subsector="all_politics">
							<span>
								<span>VISOS POLITIKOS NAUJIENOS</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
						<div class="subsector" data-subsector="foreign_politics">
							<span>
								<span>Užsienio politika</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
						<div class="subsector" data-subsector="lithuanian_politics">
							<span>
								<span>Lietuvos politika</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="right layout info">
			<div class="photos_holder">
				<div class="scroller_holder">
					<div class="stats_holder">
						<h1>Statistika</h1>
						<div class="simple_table">
							<table>
								<tr>
									<th>Darbuotojas</th>
									<th>Išsaugota tarp ruošiamų</th>
									<th>Išpublikuota</th>
								</tr>
								<tr>
									<td>Redaktorius</td>
									<td>1</td>
									<td>15</td>
								</tr>
								<tr>
									<td>Vertėjas</td>
									<td>1</td>
									<td>15</td>
								</tr>
								<tr>
									<td>Fotoredaktorius</td>
									<td>5</td>
									<td>8</td>
								</tr>
								<tr>
									<td>Redaktorius</td>
									<td>1</td>
									<td>15</td>
								</tr>
								<tr>
									<td>Vertėjas</td>
									<td>1</td>
									<td>15</td>
								</tr>
								<tr>
									<td>Fotoredaktorius</td>
									<td>5</td>
									<td>8</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>