<?php include '../partials/head.php';?>
<?php include '../partials/header_employee.php';?>

<div class="page translator_new_page sidebar_layout no_filter no_sidebar">
	<?php include '../partials/global_warning.php';?>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_employee.php';?>
			</div>	
		</div>
		<div class="right layout">
			<form id="translator_new_form">
				<a href="#" class="go_back_to_list">atgal į sąrašą</a>
				<div class="subheader">
					<div class="button history underlined">Veiksmų istorija</div>
					<div class="add_manager_holder">
						<div class="button blue add_manager">Priskirti darbuotojui</div>
						<div class="added_manager">Priskirta: <span>Vardenis Pavardenis</span></div>	
					</div>
				</div>
				<div class="simple_input">
					<div class="label">Pavadinimas</div>
					<input type="text" name="name_read" readonly value="First Saudi cinemas in decades to reopen">
					<input type="text" spellcheck="true" name="name">
				</div>
				<div class="line"></div>
				<div class="label">Data ir laikas</div>
				<label class="simple_checkbox inline postpone">
					<input type="checkbox" name="postpone">
					<span class="name">
						<span>Atidėti publikavimą</span>
					</span>
				</label>
				<div class="time_pickers">
					<div>
						<div class="simple_input inline icon_dates">
							<input class="datepicker" type="text" name="postpone_date">
						</div>
						<div class="simple_input inline hours timer">
							<input type="number" name="postpone_hours" value="12" min="0" max="24">
							<span>val.</span>
						</div>
						<div class="simple_input inline minutes timer">
							<input type="number" name="postpone_minutes" value="00" min="0" max="59">
							<span>min.</span>
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="row">
					<div class="checkboxes_box">
						<div class="label">Šalis:</div>
						<label class="simple_radio">
							<input type="radio" name="country" checked>
							<span class="name">
								<span>Lietuva</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="country">
							<span class="name">
								<span>Užsienis</span>
							</span>
						</label>
					</div>
					<div class="checkboxes_box">
						<div class="label">Kalba:</div>
						<label class="simple_radio">
							<input type="radio" name="language" checked>
							<span class="name">
								<span>Lietuvių</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="language">
							<span class="name">
								<span>Anglų</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="language">
							<span class="name">
								<span>Rusų</span>
							</span>
						</label>
					</div>
				</div>
				<div class="line"></div>
				<div class="row">
					<div class="col">
						<div class="label">Tema</div>
						<div class="simple_dropdown bigger">
							<input type="text" name="topic" disabled="" placeholder="Pasirinkite temą" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="label">Potemė</div>
						<div class="simple_dropdown bigger">
							<input type="text" name="subtopic" disabled="" placeholder="Pasirinkite potemę" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="add_another">Pridėti</div>
				<div class="line"></div>
				<div class="row">
					<div class="checkboxes_box">
						<div class="label">Naujienos tipas</div>
						<label class="simple_radio">
							<input type="radio" name="new_type" checked>
							<span class="name">
								<span>Naujiena</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="new_type">
							<span class="name">
								<span>Naujienų rink.</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="new_type">
							<span class="name">
								<span>Biuletenis</span>
							</span>
						</label>
					</div>
					<div class="col aligned">
						<div class="label">Būsena</div>
						<label class="simple_checkbox">
							<input type="checkbox" name="supplemented">
							<span class="name">
								<span>Papildyta</span>
							</span>
						</label>
						<label class="simple_checkbox">
							<input type="checkbox" name="important">
							<span class="name">
								<span>Svarbu</span>
							</span>
						</label>
						<label class="simple_checkbox">
							<input type="checkbox" name="revised">
							<span class="name">
								<span>Patikslinta</span>
							</span>
						</label>
						<label class="simple_checkbox">
							<input type="checkbox" name="interview">
							<span class="name">
								<span>Interviu</span>
							</span>
						</label>
						<label class="simple_checkbox">
							<input type="checkbox" name="renewed">
							<span class="name">
								<span>Atnaujinta</span>
							</span>
						</label>
						<label class="simple_checkbox">
							<input type="checkbox" name="comment">
							<span class="name">
								<span>Komentaras</span>
							</span>
						</label>
						<label class="simple_checkbox">
							<input type="checkbox" name="topic">
							<span class="name">
								<span>Tema</span>
							</span>
						</label>
						<label class="simple_checkbox">
							<input type="checkbox" name="edited">
							<span class="name">
								<span>Taisyta</span>
							</span>
						</label>
					</div>
				</div>
				<div class="line"></div>
				<div class="simple_textarea">
					<div class="label">Juodraštis</div>
					<textarea name="draft" placeholder="Jūsų užrašai"></textarea>
				</div>
				<div class="line"></div>
				<div class="button_space">
					<div class="copy_text" data-target="for_copy">Kopijuoti tekstą</div>
				</div>
				<div class="scrollable_container" id="for_copy">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br/><br/>
					Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi<br/><br/>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br/><br/>
					Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi
				</div>
				<div class="editor"></div>
				<div class="line"></div>
				<div class="simple_input">
					<div class="label">Raktažodžiai</div>
					<input type="text" name="keywords_read" readonly value="First Saudi cinemas in decades to reopen">
					<input type="text" spellcheck="true" name="keywords" placeholder="Pvz. elta, seimas, vyriausybe, skvernelis, karbauskis">
				</div>
				<div class="line"></div>
				<div class="form_buttons">
					<label class="simple_checkbox inline">
						<input type="checkbox" name="not_finished" checked>
						<span class="name">
							<span>Nebaigtas versti</span>
						</span>
					</label>
					<div class="delete">Trinti</div>
					<button class="button white big" type="submit">Išsaugoti tarp ruošiamų</button>
					<button class="button blue" type="submit">Išsaugoti ir publikuoti</button>
				</div>
				<a href="#" class="go_back_to_list bottom">atgal į sąrašą</a>
			</form>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>