<?php include '../partials/head.php';?>
<?php include '../partials/header_manager.php';?>

<div class="page manager_page sidebar_layout">
	<?php include '../partials/global_warning.php';?>
	<section class="filter">
		<?php include '../partials/search_manager.php';?>
		<div class="button underlined detail_search">Detali paieška</div>
		<div class="opened_filter_buttons">
			<div class="button underlined close_search">Suskleisti paiešką</div>
			<div class="clear"></div>
			<div class="button underlined clear_filters">Išvalyti filtrą</div>
		</div>
	</section>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_manager.php';?>
			</div>	
		</div>
		<div class="right layout">
			<div class="manager_list">
				<div class="heading">
					<div class="col">Vardas, Pavardė</div>
					<div class="col">Įmonė</div>
					<div class="col">Narystė galioja iki</div>
					<div class="col"></div>
				</div>
				<div class="member">
					<div class="col">Vardenis Pavardenis</div>
					<div class="col">UAB "Žinių srautas"</div>
					<div class="col">2018-09-01</div>
					<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
				</div>

				<!-- Nesu tikras, ką reiškia kai vartotojas yra atvaizduojamas rausvai. Bet toks scenarijus nupieštas -->

				<div class="member pink">
					<div class="col">Vardenis Pavardenis</div>
					<div class="col">UAB "Žinių srautas"</div>
					<div class="col">2018-09-01</div>
					<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
				</div>

				<!-- ... -->
				
				<div class="member">
					<div class="col">Vardenis Pavardenis</div>
					<div class="col">UAB "Žinių srautas"</div>
					<div class="col">2018-09-01</div>
					<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
				</div>
				<div class="member">
					<div class="col">Vardenis Pavardenis</div>
					<div class="col">UAB "Žinių srautas"</div>
					<div class="col">2018-09-01</div>
					<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
				</div>
				<div class="member">
					<div class="col">Vardenis Pavardenis</div>
					<div class="col">UAB "Žinių srautas"</div>
					<div class="col">2018-09-01</div>
					<div class="col"><a href="#" class="edit_link">Redaguoti</a></div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>