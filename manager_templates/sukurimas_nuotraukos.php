<?php include '../partials/head.php';?>
<?php include '../partials/header_employee.php';?>

<div class="page photographer_upload_page sidebar_layout no_filter no_sidebar">
	<?php include '../partials/global_warning.php';?>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_employee.php';?>
			</div>
		</div>
		<div class="right layout">
			<form id="photographer_upload_form">
				<a href="#" class="go_back_to_list">atgal į sąrašą</a>
				<div class="subheader">
					<div class="button history underlined">Veiksmų istorija</div>
					<div class="add_manager_holder">
						<div class="button blue add_manager">Priskirti darbuotojui</div>
						<div class="added_manager">Priskirta: <span>Vardenis Pavardenis</span></div>	
					</div>
				</div>
				<div class="simple_input">
					<div class="label">Pavadinimas</div>
					<input type="text" spellcheck="true" name="name">
				</div>
				<div class="line"></div>
				<div class="row">
					<div class="checkboxes_box">
						<div class="label">Šalis:</div>
						<label class="simple_radio">
							<input type="radio" name="country" checked>
							<span class="name">
								<span>Lietuva</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="country">
							<span class="name">
								<span>Užsienis</span>
							</span>
						</label>
					</div>
					<div class="checkboxes_box">
						<div class="label">Kalba:</div>
						<label class="simple_radio">
							<input type="radio" name="language" checked>
							<span class="name">
								<span>Lietuvių</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="language">
							<span class="name">
								<span>Anglų</span>
							</span>
						</label>
						<label class="simple_radio">
							<input type="radio" name="language">
							<span class="name">
								<span>Rusų</span>
							</span>
						</label>
					</div>
				</div>
				<div class="line"></div>
				<div class="row">
					<div class="col">
						<div class="label">Tema</div>
						<div class="simple_dropdown bigger">
							<input type="text" name="topic" disabled="" placeholder="Pasirinkite temą" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col">
						<div class="label">Potemė</div>
						<div class="simple_dropdown bigger">
							<input type="text" name="subtopic" disabled="" placeholder="Pasirinkite potemę" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Visos naujienos</div>
									<div class="option">Politika</div>
									<div class="option">Ekonomika</div>
									<div class="option">Teisėtvarka</div>
									<div class="option">Soprtas</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="add_another">Pridėti</div>
				<div class="line"></div>
				<div class="row">
					<div class="col">
						<div class="label">Autorius</div>
						<div class="autocomplete_dropdown">
							<div class="planks">
								<div class="plank"></div>
								<div class="plank"></div>
								<div class="plank"></div>
							</div>
							<select class="for_desktop" name="author" data-placeholder="Pasirinkite autorių" tabindex="1">
								<option value=""></option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
							</select>
							<!-- Cia dublikatas del mobile -->
							<select name="author_2" class="for_mobile not_touched">
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
								<option value="Visos naujienos">Visos naujienos</option>
								<option value="Politika">Politika</option>
								<option value="Soprtas">Soprtas</option>
							</select>
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="simple_input">
					<div class="label">Raktažodžiai</div>
					<input type="text" spellcheck="true" name="keywords" placeholder="Pvz. elta, seimas, vyriausybe, skvernelis, karbauskis">
				</div>
				<div class="line"></div>
				<div class="drag_and_drop">
					<label class="area">
						<input multiple name="file" type="file" accept=".gif, .svg, .jpg, .png, .doc, .docx, .xls, .xlsx, .pptx, .ppt, .pdf |audio/*|video/*|image/*|media_type">
						<span class="texts">
							<span class="big">Drag&Drop</span>
							<span class="small">Norėdami įkelti failus tempkite juos pele į šią vietą arba <span class="blue">įkelkite</span> juos tiesiai iš kompiuterio</span>
						</span>
					</label>
					<div class="uploaded_area"></div>
				</div>
				<div class="line"></div>
				<div class="form_buttons">
					<div class="error_message">Something went wrong!</div>
					<div class="delete">Trinti</div>
					<button class="button white big" type="submit">Išsaugoti tarp ruošiamų</button>
					<button class="button blue" type="submit">Išsaugoti ir publikuoti</button>
				</div>
				<a href="#" class="go_back_to_list bottom">atgal į sąrašą</a>
			</form>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>