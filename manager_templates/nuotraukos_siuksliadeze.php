<?php include '../partials/head.php';?>
<?php include '../partials/header_employee.php';?>

<div class="page photos_page manager_view">
	<?php include '../partials/global_warning.php';?>
	<section class="filter">
		<?php include '../partials/search_client_without_checkboxes.php';?>
		<div class="button underlined detail_search">Detali paieška</div>
		<div class="opened_filter_buttons">
			<div class="button underlined close_search">Suskleisti paiešką</div>
			<div class="clear"></div>
			<div class="button underlined clear_filters">Išvalyti filtrą</div>
		</div>
		<div class="right_box">
			<div class="label">Atvaizdavimas</div>
			<div class="view current" data-view="columns"></div>
			<div class="view" data-view="info"></div>
		</div>
	</section>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_employee.php';?>
			</div>
		</div>
		<div class="right layout recycle_bin">
			<div class="control_area">
				<label class="simple_checkbox">
					<input type="checkbox" name="check_all">
					<span class="name">
						<span>Žymėti viską</span>
					</span>
				</label>
				<div class="right_buttons not_active">
					<div class="delete">Ištrinti</div>
					<div class="restore">Atstatyti</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="photos_holder">
				<div class="scroller_holder">
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<label class="simple_checkbox">
							<input type="checkbox" name="select_new">
							<span class="name">
								<span></span>
							</span>
						</label>
						<div class="img">
							<img src="../media/images/news.jpg" alt="">
						</div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="show_more_button">Rodyti daugiau</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>