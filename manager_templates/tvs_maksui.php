<?php include '../partials/head.php';?>
<?php include '../partials/header_employee.php';?>

<div class="page cms_page news_page no_filter">
	<?php include '../partials/global_warning.php';?>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<div class="toggler">
					<div class="toggler_head active" data-sector="all">
						<span>
							<span>Viskas</span>
						</span>
					</div>
					<div class="toggler_head" data-sector="my_messages">
						<span>
							<span>Priskirta man</span>

							<!-- Šitą tik tuo atveju jeigu yra žinučių "priskirta man" -->
							<span class="has_messages">2</span>
							<!-- ... -->

						</span>
						<div class="opener"><span></span></div>
					</div>
					<div class="toggler_body">
						<div class="subsector" data-subsector="all_politics">
							<span>
								<span>VISOS POLITIKOS NAUJIENOS</span>
							</span>
						</div>
						<div class="subsector" data-subsector="foreign_politics">
							<span>
								<span>Užsienio politika</span>
							</span>
						</div>
						<div class="subsector" data-subsector="lithuanian_politics">
							<span>
								<span>Lietuvos politika</span>
							</span>
						</div>
					</div>
					<div class="toggler_head" data-sector="businness">
						<span>
							<span>Politika</span>
						</span>
						<div class="opener"><span></span></div>
					</div>
					<div class="toggler_body">
						<div class="subsector" data-subsector="all_politics">
							<span>
								<span>VISOS POLITIKOS NAUJIENOS</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
						<div class="subsector" data-subsector="foreign_politics">
							<span>
								<span>Užsienio politika</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
						<div class="subsector" data-subsector="lithuanian_politics">
							<span>
								<span>Lietuvos politika</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
					</div>
					<div class="toggler_head" data-sector="law">
						<span>
							<span>Politika</span>
						</span>
						<div class="opener"><span></span></div>
					</div>
					<div class="toggler_body">
						<div class="subsector" data-subsector="all_politics">
							<span>
								<span>VISOS POLITIKOS NAUJIENOS</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
						<div class="subsector" data-subsector="foreign_politics">
							<span>
								<span>Užsienio politika</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
						<div class="subsector" data-subsector="lithuanian_politics">
							<span>
								<span>Lietuvos politika</span>
							</span>
							<div class="sub_opener"><span></span></div>
						</div>
						<div class="toggler_sub_body">
							<div class="sub_subsector" data-sub-subsector="category_1">
								Sub sub kategorija 1
							</div>
							<div class="sub_subsector" data-sub-subsector="category_2">
								Sub sub kategorija 2
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="right layout info columns">
			<div class="photos_holder">
				<div class="scroller_holder">
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
								<span class="has_photos">(4)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Papildyta</span>
									<span class="badge" data-color="#03ae50">Svarbu</span>
									<span class="badge" data-color="#e49600">Patikslinta</span>
									<span class="badge" data-color="#7b00e4">Interviu</span>
									<span class="badge" data-color="#e40056">Atnaujinta</span>
									<span class="badge" data-color="#0066ff">Komentaras</span>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
								<span class="has_photos">(8)</span>
								<div class="badges">
									<span class="badge" data-color="#3f9ee7">Verčiamas: V. Pavardenis</span>
								</div>
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="photo">
						<div class="img" style="background-image: url('../media/images/news.jpg');"></div>
						<div class="name">
							<div>
								J. Valančiūnui
							</div>
						</div>
						<div class="tags">
							<span>
								<a class="tag" href="#">#Puigdemont</a>
								<a class="tag" href="#">#Katalonija</a>
								<a class="tag" href="#">#Ispanija</a>
							</span>
						</div>
						<div class="time_stamp">
							<span>
								<span class="date">2017-03-21</span>
								<span class="time">21:45</span>
							</span>
						</div>
					</div>
					<div class="show_more_button">Rodyti daugiau</div>
				</div>
			</div>
		</div>
		<div class="side_pop">
			<div class="news_high scroller_holder">
				<h2>Lietuvos ledo ritulininkės iš Latvijos parsivežė unikalią patirtį</h2>
				<div class="time_stamp">
					<div class="date">2017-08-07</div>
					<div class="time">11:41</div>
				</div>
				<div class="heading">Ryga, kovo 12 d. (ELTA)</div>
				<div class="simple_text">
					Savaitgalį dienomis Tukumse vyko trečiasis Latvijos moterų ledo ritulio čempionato turas. Paskutiniąsias ketverias čempionato rungtynes jame sužaidė ir lietuvių „Hockey Girls“ ekipa
				</div>
				<form>
					<div class="simple_input">
						<div class="label">Pavadinimas</div>
						<input type="text" spellcheck="true" name="name" placeholder="Įveskite pavadinimą">
					</div>
					<div class="simple_textarea">
						<div class="label">Pastabos ir komentarai</div>
						<textarea name="comments" placeholder="Jūsų komentarai"></textarea>
					</div>
					<div class="label">Text Editor</div>
					<div class="editor"></div>
					<div class="label">Šalis:</div>
					<label class="simple_radio">
						<input type="radio" name="country" checked>
						<span class="name">
							<span>Lietuva</span>
						</span>
					</label>
					<label class="simple_radio">
						<input type="radio" name="country">
						<span class="name">
							<span>Užsienis</span>
						</span>
					</label>
					<label class="simple_radio">
						<input type="radio" name="country">
						<span class="name">
							<span>Lietuva</span>
						</span>
					</label>
					<label class="simple_radio">
						<input type="radio" name="country">
						<span class="name">
							<span>Užsienis</span>
						</span>
					</label>
					<div class="label">Dalykai:</div>
					<label class="simple_checkbox">
						<input type="checkbox" name="vienas">
						<span class="name">
							<span>Lietuva</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="du">
						<span class="name">
							<span>Užsienis</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="trys">
						<span class="name">
							<span>Lietuva</span>
						</span>
					</label>
					<label class="simple_checkbox">
						<input type="checkbox" name="keturi">
						<span class="name">
							<span>Užsienis</span>
						</span>
					</label>
					<div class="label">Dropdownas</div>
					<div class="simple_dropdown bigger">
						<input type="text" name="topic" disabled="" placeholder="Pasirinkite temą" value="">
						<div class="content">
							<div class="scroller_holder">
								<div class="option">Visos naujienos</div>
								<div class="option">Politika</div>
								<div class="option">Ekonomika</div>
								<div class="option">Teisėtvarka</div>
								<div class="option">Soprtas</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="added_photos">

						<!-- Šitas pirmas .photo_block iskviečia popupą, tai jis turi būti visada, o kiti .photo_block yra šiaip suhardkodinti dėl vaizdom kad matytųsi kaip turi išsivest, bet aš nerašiau javascripto jų sukūrimo pasirinkus, nes ten popupe yra pageris, tai vistiek reikės ajaxo tam -->

						<div class="photo_block">
							<div class="photo_holder add_photo">
								<span>Pasirinkite nuotrauką</span>
							</div>
						</div>
						<div class="photo_block">
							<div class="photo_holder">
								<div class="buttons">
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
									<span class="edit open_edit_pop">Redaguoti</span>
								</div>
								<div class="remove"></div>
								<div class="img_holder">
									<img src="../media/images/thumb_photo_1.jpg" alt="">
								</div>
							</div>
							<div class="name">S. Skvernelis / Irmanto Gelūno nuotr.</div>
						</div>
						<div class="photo_block">
							<div class="photo_holder">
								<div class="buttons">
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
									<span class="edit open_edit_pop">Redaguoti</span>
								</div>
								<div class="remove"></div>
								<div class="img_holder">
									<img src="../media/images/thumb_photo_2.jpg" alt="">
								</div>
							</div>
							<div class="name">R. Karbauskis / Irmanto Gelūno nuotr.</div>
						</div>
						<div class="photo_block">
							<div class="photo_holder">
								<div class="buttons">
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
									<span class="edit open_edit_pop">Redaguoti</span>
								</div>
								<div class="remove"></div>
								<div class="img_holder">
									<img src="../media/images/thumb_photo_1.jpg" alt="">
								</div>
							</div>
							<div class="name">S. Skvernelis / Irmanto Gelūno nuotr.</div>
						</div>
						<div class="photo_block">
							<div class="photo_holder">
								<div class="buttons">
									<div class="info extra_tooltip">
										<div class="tooltip">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitr.
										</div>
									</div>
									<span class="edit open_edit_pop">Redaguoti</span>
								</div>
								<div class="remove"></div>
								<div class="img_holder">
									<img src="../media/images/thumb_photo_2.jpg" alt="">
								</div>
							</div>
							<div class="name">R. Karbauskis / Irmanto Gelūno nuotr.</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="form_buttons">
						<div class="error_message">Something went wrong!</div>
						<div class="delete">Trinti</div>
						<button class="button white big" type="submit">Išsaugoti tarp ruošiamų</button>
						<button class="button blue" type="submit">Išsaugoti ir publikuoti</button>
					</div>
				</form>			
			</div>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>