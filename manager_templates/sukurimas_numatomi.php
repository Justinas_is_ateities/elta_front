<?php include '../partials/head.php';?>
<?php include '../partials/header_employee.php';?>

<div class="page predefined_page sidebar_layout no_filter no_sidebar">
	<?php include '../partials/global_warning.php';?>
	<section class="photos">
		<div class="mobile_filter_toggler">
			<span class="icon">
				<span class="plank"></span>
				<span class="plank"></span>
				<span class="plank"></span>
			</span>
			Nuotraukų filtras
		</div>
		<div class="left">
			<div class="scroller_holder">
				<?php include '../partials/sidebar_employee.php';?>
			</div>
		</div>
		<div class="right layout">
			<form id="predefined_form">
				<a href="#" class="go_back_to_list">atgal į sąrašą</a>
				<div class="subheader">
					<div class="button history underlined">Veiksmų istorija</div>
					<div class="add_manager_holder">
						<div class="button blue add_manager">Priskirti darbuotojui</div>
						<div class="added_manager">Priskirta: <span>Vardenis Pavardenis</span></div>	
					</div>
				</div>
				<div class="simple_input">
					<div class="label">Pavadinimas</div>
					<input type="text" spellcheck="true" name="name">
				</div>
				<div class="line"></div>
				<div class="label">Data ir laikas</div>
				<div class="time_pickers">
					<div>
						<div class="simple_input inline icon_dates">
							<input class="datepicker" type="text" name="postpone_date">
						</div>
						<div class="simple_input inline hours timer">
							<input type="number" name="postpone_hours" value="12" min="0" max="24">
							<span>val.</span>
						</div>
						<div class="simple_input inline minutes timer">
							<input type="number" name="postpone_minutes" value="00" min="0" max="59">
							<span>min.</span>
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="label">Tema</div>
				<div class="row">
					<div class="col">
						<div class="simple_dropdown bigger">
							<input type="text" name="theme" disabled="" placeholder="Pasirinkite temą" value="">
							<div class="content">
								<div class="scroller_holder">
									<div class="option">Prezidentūra</div>
									<div class="option">Seimas</div>
									<div class="option">Vyriausybė</div>
									<div class="option">Ministerijos</div>
									<div class="option">Savivalda</div>
									<div class="option">Vyriausioji rinkimų komisija (vrk)</div>
									<div class="option">Vyriausioji tarnybinės etikos komisija (vtek)</div>
									<div class="option">Gynyba</div>
									<div class="option">Diplomatija</div>
									<div class="option">Teisėsauga</div>
									<div class="option">Organizacijos</div>
									<div class="option">Minėjimas</div>
									<div class="option">Mitingas</div>
									<div class="option">Akcija</div>
									<div class="option">Ūkis</div>
									<div class="option">Sveikatos apsauga</div>
									<div class="option">Bažnyčia</div>
									<div class="option">Mokslas</div>
									<div class="option">Švietimas</div>
									<div class="option">Kultūra</div>
									<div class="option">Pramogos</div>
									<div class="option">Sportas</div>
									<div class="option">Spaudos konferencijos</div>
									<div class="option">Seime</div>
									<div class="option">Kitos</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="line"></div>
				<div class="editor"></div>
				<div class="line"></div>
				<div class="form_buttons">
					<div class="error_message">Something went wrong!</div>
					<div class="delete">Trinti</div>
					<button class="button blue" type="submit">Išsaugoti ir publikuoti</button>
				</div>
				<a href="#" class="go_back_to_list bottom">atgal į sąrašą</a>
			</form>
		</div>
	</section>
</div>

<?php include '../partials/footer.php';?>