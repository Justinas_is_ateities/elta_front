import Html from "slate-html-serializer";
import React, { PureComponent } from "react";
import ReactDOM from "react-dom";
import "../styles/global/main.scss";
import { Editor, getEventTransfer, getEventRange } from "slate-react";
import { Block, Value, Selection } from "slate";
import ReactQuill from "react-quill";
import $ from "jquery";
import DeepTable from "slate-deep-table";
import isUrl from "is-url";
import Icon from "react-icons-kit";
import Plain from "slate-plain-serializer";

import { browser } from "react-icons-kit/oct/browser";
import { arrow_left } from "react-icons-kit/ikons/arrow_left";
import { arrow_right } from "react-icons-kit/ikons/arrow_right";
import { text_left } from "react-icons-kit/ikons/text_left";
import { text_right } from "react-icons-kit/ikons/text_right";
import { text_center } from "react-icons-kit/ikons/text_center";
import { text_justify } from "react-icons-kit/ikons/text_justify";
import { picture } from "react-icons-kit/ikons/picture";
import { table2 } from "react-icons-kit/icomoon/table2";
import { timesRectangleO } from "react-icons-kit/fa/timesRectangleO";
import { down } from "react-icons-kit/iconic/down";
import { up } from "react-icons-kit/iconic/up";
import { ic_format_bold } from "react-icons-kit/md/ic_format_bold";
import { ic_format_italic } from "react-icons-kit/md/ic_format_italic";
import { ic_format_underlined } from "react-icons-kit/md/ic_format_underlined";
import { ic_strikethrough_s } from "react-icons-kit/md/ic_strikethrough_s";
import { ic_format_quote } from "react-icons-kit/md/ic_format_quote";
import { ic_format_list_bulleted } from "react-icons-kit/md/ic_format_list_bulleted";
import { ic_format_list_numbered } from "react-icons-kit/md/ic_format_list_numbered";
import { clearFormatting } from "react-icons-kit/icomoon/clearFormatting";
import { ic_format_size } from "react-icons-kit/md/ic_format_size";
import { u1F1ED } from "react-icons-kit/noto_emoji_regular/u1F1ED";
import { ic_code } from "react-icons-kit/md/ic_code";
import { cross } from "react-icons-kit/icomoon/cross";
import { checkmark } from "react-icons-kit/icomoon/checkmark";
import { ic_border_clear } from "react-icons-kit/md/ic_border_clear";
import { copywriting } from "react-icons-kit/iconic/copywriting";
import { quoteRight } from "react-icons-kit/fa/quoteRight";
import { textHeight } from "react-icons-kit/icomoon/textHeight";

const initialValue = Value.fromJSON({
  document: {
    nodes: [
      {
        object: "block",
        type: "block",
        nodes: [
          {
            object: "text",
            leaves: [
              {
                text: "",
              },
            ],
          },
        ],
      },
    ],
  },
});

const plugins = [DeepTable()];

String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, "g"), replacement);
};

const Image = ({ src, selected, className, width }) => {
  let style_value;
  if (typeof className == "undefined") {
    style_value = "left";
  } else if (className == "left_img") {
    style_value = "left";
  } else if (className == "right_img") {
    style_value = "right";
  } else if (className == "center_img") {
    style_value = "center";
  }
  return (
    <div
      className={"image_holder " + (selected ? "selected " : "")}
      style={{ textAlign: style_value }}
    >
      <img
        src={src}
        style={{
          width: width,
          display: "inline-block",
          maxWidth: "100%",
          margin: "20px 0",
        }}
        data-width={width}
        data-className={className}
        alt=""
      />
    </div>
  );
};

const Image_text = ({ src, selected, className, width, status }) => {
  let style_value;
  let margin = "20px 20px 20px 0";
  if (typeof className == "undefined") {
    style_value = "left";
  } else if (className == "left_img") {
    style_value = "left";
  } else if (className == "right_img") {
    style_value = "right";
  } else if (className == "center_img") {
    style_value = "center";
  }
  if (typeof status == "undefined") {
    status = "left";
  }
  if (status == "right") {
    margin = "20px 0 20px 20px";
  }
  if (status == "block") {
    margin = "0";
  }
  return (
    <img
      className={"free_image " + (selected ? "selected " : "")}
      tabIndex="0"
      src={src}
      style={{
        float: status,
        width: width,
        display: "inline-block",
        maxWidth: "100%",
        margin: margin,
      }}
      data-width={width}
      data-className={className}
      data-status={status}
      alt=""
    />
  );
};

const Left = (props) => (
  <div style={{ textAlign: "left" }}>{props.children}</div>
);

const Right = (props) => (
  <div style={{ textAlign: "right" }}>{props.children}</div>
);

const Center = (props) => (
  <div style={{ textAlign: "center" }}>{props.children}</div>
);

const uppercase = (props) => (
  <span style={{ textTransform: "uppercase" }}>{props.children}</span>
);

const Justify = (props) => (
  <div style={{ textAlign: "justify", whiteSpace: "pre-line" }}>
    {props.children}
  </div>
);

function insertImage(editor, src, target) {
  if (target) {
    editor.select(target);
  }

  editor.insertBlock({
    type: "image",
    data: { src },
    className: "left_img",
    status: "block",
  });
}

function insertIframe(editor, src, target) {
  if (target) {
    editor.select(target);
  }

  editor.insertBlock({
    type: "iframe",
    data: { src },
    status: "block",
  });
}

const BLOCK_TAGS = {
  p: "div",
  li: "list-item",
  ul: "bulleted-list",
  ol: "numbered-list",
  blockquote: "quote",
  pre: "code",
  img: "image",
  iframe: "iframe",
  div: "div",
  table: "table",
  tr: "table_row",
  td: "table_cell",
};

const MARK_TAGS = {
  strong: "bold",
  em: "italic",
  u: "underline",
  strike: "strike",
  s: "strike",
  code: "code",
  h1: "heading-one",
  h2: "heading-two",
  h3: "heading-three",
  h4: "heading-four",
  h5: "heading-five",
  h6: "heading-six",
};

const DEFAULT_NODE = "block";

const RULES = [
  {
    deserialize(el, next) {
      const mark = MARK_TAGS[el.tagName.toLowerCase()];
      const element = $(el);
      if (typeof element.attr("style") != "undefined" && mark) {
        if (
          mark &&
          (element.attr("style").indexOf("text-align: center") != -1 ||
            element.attr("style").indexOf("text-align:center") != -1)
        ) {
          if (mark == "heading-one") {
            return {
              object: "mark",
              type: "h1_center",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-two") {
            return {
              object: "mark",
              type: "h2_center",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-three") {
            return {
              object: "mark",
              type: "h3_center",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-four") {
            return {
              object: "mark",
              type: "h4_center",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-five") {
            return {
              object: "mark",
              type: "h5_center",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-six") {
            return {
              object: "mark",
              type: "h6_center",
              nodes: next(el.childNodes),
            };
          }
        } else if (
          mark &&
          (element.attr("style").indexOf("text-transform: uppercase") != -1 ||
            element.attr("style").indexOf("text-transform:uppercase") != -1)
        ) {
          return {
            object: "mark",
            type: "uppercase",
            nodes: next(el.childNodes),
          };
        } else if (
          mark &&
          (element.attr("style").indexOf("text-align: right") != -1 ||
            element.attr("style").indexOf("text-align:right") != -1)
        ) {
          if (mark == "heading-one") {
            return {
              object: "mark",
              type: "h1_right",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-two") {
            return {
              object: "mark",
              type: "h2_right",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-three") {
            return {
              object: "mark",
              type: "h3_right",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-four") {
            return {
              object: "mark",
              type: "h4_right",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-five") {
            return {
              object: "mark",
              type: "h5_right",
              nodes: next(el.childNodes),
            };
          } else if (mark == "heading-six") {
            return {
              object: "mark",
              type: "h6_right",
              nodes: next(el.childNodes),
            };
          }
        }
      } else if (mark && element.hasClass("ql-align-right")) {
        if (mark == "heading-one") {
          return {
            object: "mark",
            type: "h1_right",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-two") {
          return {
            object: "mark",
            type: "h2_right",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-three") {
          return {
            object: "mark",
            type: "h3_right",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-four") {
          return {
            object: "mark",
            type: "h4_right",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-five") {
          return {
            object: "mark",
            type: "h5_right",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-six") {
          return {
            object: "mark",
            type: "h6_right",
            nodes: next(el.childNodes),
          };
        }
      } else if (mark && element.hasClass("ql-align-center")) {
        if (mark == "heading-one") {
          return {
            object: "mark",
            type: "h1_center",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-two") {
          return {
            object: "mark",
            type: "h2_center",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-three") {
          return {
            object: "mark",
            type: "h3_center",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-four") {
          return {
            object: "mark",
            type: "h4_center",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-five") {
          return {
            object: "mark",
            type: "h5_center",
            nodes: next(el.childNodes),
          };
        } else if (mark == "heading-six") {
          return {
            object: "mark",
            type: "h6_center",
            nodes: next(el.childNodes),
          };
        }
      } else if (element.hasClass("ql-align-right")) {
        return {
          object: "mark",
          type: "right",
          nodes: next(el.childNodes),
        };
      } else if (element.hasClass("ql-align-center")) {
        return {
          object: "mark",
          type: "center",
          nodes: next(el.childNodes),
        };
      } else if (mark) {
        return {
          object: "mark",
          type: mark,
          nodes: next(el.childNodes),
        };
      } else if (typeof element.attr("style") != "undefined") {
        if (
          String(element.html()).indexOf("img") == -1 &&
          String(element.html()).indexOf("iframe") == -1
        ) {
          if (
            element.attr("style").indexOf("text-transform: uppercase") != -1 ||
            element.attr("style").indexOf("text-transform:uppercase") != -1
          ) {
            return {
              object: "mark",
              type: "uppercase",
              nodes: next(el.childNodes),
            };
          }
          if (
            element.attr("style").indexOf("text-align: center") != -1 ||
            element.attr("style").indexOf("text-align:center") != -1
          ) {
            return {
              object: "mark",
              type: "center",
              nodes: next(el.childNodes),
            };
          } else if (
            element.attr("style").indexOf("text-align: left") != -1 ||
            element.attr("style").indexOf("text-align:left") != -1
          ) {
            return {
              object: "mark",
              type: "left",
              nodes: next(el.childNodes),
            };
          } else if (
            element.attr("style").indexOf("text-align: right") != -1 ||
            element.attr("style").indexOf("text-align:right") != -1
          ) {
            return {
              object: "mark",
              type: "right",
              nodes: next(el.childNodes),
            };
          } else if (
            element.attr("style").indexOf("text-align: justify") != -1 ||
            element.attr("style").indexOf("text-align:justify") != -1
          ) {
            return {
              object: "mark",
              type: "justify",
              nodes: next(el.childNodes),
            };
          } else if (
            element.attr("style").indexOf("font-size: 14px") != -1 ||
            element.attr("style").indexOf("font-size:14px") != -1
          ) {
            return {
              object: "mark",
              type: "small",
              nodes: next(el.childNodes),
            };
          } else if (
            element.attr("style").indexOf("font-size: 18px") != -1 ||
            element.attr("style").indexOf("font-size:18px") != -1
          ) {
            return {
              object: "mark",
              type: "medium",
              nodes: next(el.childNodes),
            };
          } else if (
            element.attr("style").indexOf("font-size: 16px") != -1 ||
            element.attr("style").indexOf("font-size:16px") != -1
          ) {
            return {
              object: "mark",
              type: "default",
              nodes: next(el.childNodes),
            };
          } else if (
            element.attr("style").indexOf("font-size: 20px") != -1 ||
            element.attr("style").indexOf("font-size:20px") != -1
          ) {
            return {
              object: "mark",
              type: "big",
              nodes: next(el.childNodes),
            };
          }
        }
      }
    },
  },
  {
    deserialize(el, next) {
      const element = $(el);
      if (element.hasClass("iframe_holder")) {
        return {
          object: "block",
          type: "iframe",
          nodes: next(el.childNodes),
          data: {
            src: element.find("iframe").attr("src"),
          },
        };
      } else if (el.tagName.toLowerCase() == "table") {
        const block = BLOCK_TAGS[el.tagName.toLowerCase()];
        if (block) {
          let opacity = false;
          if (
            $(el).attr("style").indexOf("border-width: 0px") != -1 ||
            $(el).attr("style").indexOf("border-width: 0") != -1 ||
            $(el).attr("style").indexOf("border-width:0px") != -1 ||
            $(el).attr("style").indexOf("border-width:0") != -1
          ) {
            opacity = true;
          }
          return {
            object: "block",
            type: block,
            nodes: next(el.childNodes),
            data: {
              opacity: opacity,
            },
          };
        }
      } else if (el.tagName.toLowerCase() != "img") {
        const block = BLOCK_TAGS[el.tagName.toLowerCase()];
        if (block) {
          return {
            object: "block",
            type: block,
            nodes: next(el.childNodes),
          };
        }
      } else {
        const element = $(el);
        let className = element.attr("data-className");
        if (typeof className == "undefined") {
          className = "left_img";
        }
        let style = element.attr("style");
        let width = "auto";
        let status = "block";
        if (typeof style != "undefined") {
          style = style.split(";");
          for (let i = 0, j = style.length; i < j; i++) {
            let value = style[i];
            if (
              value.indexOf("width") != -1 &&
              value.indexOf("max-width") == -1
            ) {
              width = value.split(":")[1];
            }
            if (value.indexOf("float") != -1) {
              status = value.split(":")[1];
            }
          }
        }
        if (status != "block") {
          return {
            object: "block",
            type: "image_text",
            nodes: next(el.childNodes),
            data: {
              src: el.getAttribute("src"),
              width: width,
              className: className,
              status: status,
            },
          };
        } else {
          return {
            object: "block",
            type: "image",
            nodes: next(el.childNodes),
            data: {
              src: el.getAttribute("src"),
              width: width,
              className: className,
              status: status,
            },
          };
        }
      }
    },
  },
  {
    serialize(obj, children) {
      if (obj.object == "block") {
        switch (obj.type) {
          case "code":
            return (
              <pre>
                <code>{children}</code>
              </pre>
            );
          case "paragraph":
            return <div className={obj.data.get("className")}>{children}</div>;
          case "iframe":
            return (
              <div
                className="iframe_holder"
                style={{
                  position: "relative",
                  width: "100%",
                  paddingTop: "56.25%",
                }}
              >
                <iframe
                  frameBorder="0"
                  allowFullScreen="1"
                  src={obj.data.get("src")}
                  style={{
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    top: "0",
                    left: "0",
                  }}
                ></iframe>
              </div>
            );
          case "quote":
            return <blockquote>{children}</blockquote>;
          case "break":
            return <div>{children}</div>;
          case "div":
            return <div>{children}</div>;
          case "block":
            return <div>{children}</div>;
          case "quote":
            return <blockquote>{children}</blockquote>;
          case "code":
            return (
              <pre>
                <code>{children}</code>
              </pre>
            );
          case "bulleted-list":
            return <ul>{children}</ul>;
          case "heading-one":
            return <h1>{children}</h1>;
          case "heading-two":
            return <h2>{children}</h2>;
          case "heading-three":
            return <h3>{children}</h3>;
          case "heading-four":
            return <h4>{children}</h4>;
          case "heading-five":
            return <h5>{children}</h5>;
          case "uppercase":
            return (
              <span style={{ textTransform: "uppercase" }}>{children}</span>
            );
          case "heading-six":
            return <h6>{children}</h6>;
          case "list-item":
            return <li>{children}</li>;
          case "numbered-list":
            return <ol>{children}</ol>;
          case "table_cell":
            return (
              <td
                style={{
                  borderColor: "black",
                  borderStyle: "solid",
                  borderWidth: "inherit",
                  padding: ".5em",
                }}
              >
                {children}
              </td>
            );
          case "table_row":
            return (
              <tr
                style={{
                  borderColor: "black",
                  borderStyle: "solid",
                  borderWidth: "inherit",
                }}
              >
                {children}
              </tr>
            );
          case "table":
            let opacity;
            if (typeof obj.data.get("opacity") == "undefined") {
              opacity = "1px";
            } else if (obj.data.get("opacity")) {
              opacity = "0px";
            } else {
              opacity = "1px";
            }
            return (
              <table
                style={{
                  borderColor: "black",
                  borderStyle: "solid",
                  borderWidth: opacity,
                  borderCollapse: "collapse",
                  width: "100%",
                }}
              >
                <tbody
                  style={{
                    borderColor: "black",
                    borderStyle: "solid",
                    borderWidth: "inherit",
                  }}
                >
                  {children}
                </tbody>
              </table>
            );
          case "image_text": {
            const src = obj.data.get("src");
            let className = obj.data.get("className");
            let width = obj.data.get("width");
            let status = obj.data.get("status");

            if (typeof className == "undefined") {
              className = "left_img";
            }
            if (typeof width == "undefined") {
              width = "auto";
            }
            if (typeof status == "undefined") {
              status = "left";
            }
            return (
              <Image_text
                src={src}
                className={className}
                width={width}
                status={status}
              />
            );
          }
          case "image":
            const src = obj.data.get("src");
            let className = obj.data.get("className");
            let width = obj.data.get("width");
            if (typeof className == "undefined") {
              className = "left_img";
            }
            if (typeof width == "undefined") {
              width = "auto";
            }
            return <Image src={src} className={className} width={width} />;
        }
      }
    },
  },
  {
    serialize(obj, children) {
      if (obj.object == "mark") {
        switch (obj.type) {
          case "bold":
            return <strong>{children}</strong>;
          case "italic":
            return <em>{children}</em>;
          case "underline":
            return <u>{children}</u>;
          case "uppercase":
            return (
              <span style={{ textTransform: "uppercase" }}>{children}</span>
            );
          case "center":
            return <div style={{ textAlign: "center" }}>{children}</div>;
          case "justify":
            return (
              <div style={{ textAlign: "justify", whiteSpace: "pre-line" }}>
                {children}
              </div>
            );
          case "left":
            return <div style={{ textAlign: "left" }}>{children}</div>;
          case "right":
            return <div style={{ textAlign: "right" }}>{children}</div>;
          case "bold":
            return <strong>{children}</strong>;
          case "strike":
            return <strike>{children}</strike>;
          case "code":
            return <code>{children}</code>;
          case "italic":
            return <em>{children}</em>;
          case "quote":
            return <blockquote>{children}</blockquote>;
          case "underline":
            return <u>{children}</u>;
          case "heading-one":
            return <h1>{children}</h1>;
          case "heading-two":
            return <h2>{children}</h2>;
          case "heading-three":
            return <h3>{children}</h3>;
          case "heading-four":
            return <h4>{children}</h4>;
          case "h1_right":
            return <h1 style={{ textAlign: "right" }}>{children}</h1>;
          case "h2_right":
            return <h2 style={{ textAlign: "right" }}>{children}</h2>;
          case "h3_right":
            return <h3 style={{ textAlign: "right" }}>{children}</h3>;
          case "h4_right":
            return <h4 style={{ textAlign: "right" }}>{children}</h4>;
          case "h1_center":
            return <h1 style={{ textAlign: "center" }}>{children}</h1>;
          case "h2_center":
            return <h2 style={{ textAlign: "center" }}>{children}</h2>;
          case "h3_center":
            return <h3 style={{ textAlign: "center" }}>{children}</h3>;
          case "h4_center":
            return <h4 style={{ textAlign: "center" }}>{children}</h4>;
          case "small":
            return <div style={{ fontSize: "14px" }}>{children}</div>;
          case "medium":
            return <div style={{ fontSize: "18px" }}>{children}</div>;
          case "default":
            return <div style={{ fontSize: "16px" }}>{children}</div>;
          case "big":
            return <div style={{ fontSize: "20px" }}>{children}</div>;
        }
      }
    },
  },
  {
    deserialize(el, next) {
      if (el.tagName.toLowerCase() == "pre") {
        const code = el.childNodes[0];
        const childNodes =
          code && code.tagName.toLowerCase() == "code"
            ? code.childNodes
            : el.childNodes;

        return {
          object: "block",
          type: "code",
          nodes: next(childNodes),
        };
      }
    },
  },
];

const schema = {
  document: {
    last: { type: "block" },
    normalize: (editor, { code, node, child }) => {
      switch (code) {
        case "last_child_type_invalid": {
          const paragraph = Block.create("block");
          return editor.insertNodeByKey(node.key, node.nodes.size, paragraph);
        }
      }
    },
  },
  blocks: {
    image: {
      isVoid: true,
    },
    iframe: {
      isVoid: true,
    },
    image_text: {
      isVoid: true,
    },
  },
};

const serializer = new Html({ rules: RULES });
const previous = [];
const next = [];
const body = $("body");
let heightDebounce;
let table_debounce = false;
let data_debounce = false;
let jumping_flag = false;

class App extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: initialValue,
      undo: false,
      redo: false,
      ctrlDown: false,
      text: "",
      quillExist: false,
      imgData: "",
      left: false,
      right: false,
      center: false,
      justify: false,
      bold: false,
      italic: false,
      underline: false,
      strike: false,
      link_text: "",
      link_checkbox: true,
      link_pop: false,
      link_selected: false,
      qoutes_selected: false,
      bullets_selected: false,
      numbers_selected: false,
      heading_one: false,
      heading_two: false,
      heading_three: false,
      heading_four: false,
      small: false,
      medium: false,
      default: false,
      big: false,
      table: false,
      html_mode: false,
      image_pop: false,
      error_text: "",
      explain: [],
      selected_table: "",
      border_button: false,
      uppercase: false,
      frame_pop: false,
      frame_text: "",
    };

    this.undo = this.undo.bind(this);
    this.redo = this.redo.bind(this);
    this.prevText;
    this.undoDeb = true;
    this.redoDeb = true;
  }

  componentDidMount() {
    this.refs.editor.focus();
    this.refs.editor.blur();
    var that = this;
    $(document).on("click", function (e) {
      var target = $(e.target);
      if (target.hasClass("free_image")) {
        var src = target.attr("src");
        var width = target.attr("data-width");
        var className = target.attr("data-className");
        var status = target.attr("data-status");
        if (typeof status == "undefined") {
          status = "block";
        }
        that.setState({
          imgData: {
            src: src,
            width: width,
            className: className,
            status: status,
          },
        });
      } else if (
        !target.hasClass("image_holder") &&
        !target.closest(".image_holder").hasClass("image_holder") &&
        !target.hasClass("button") &&
        !target.closest(".button").hasClass("button")
      ) {
        that.setState({ imgData: "" });
      } else if (target.hasClass("image_holder")) {
        var src = target.find("img").attr("src");
        var width = target.find("img").attr("data-width");
        var className = target.find("img").attr("data-className");
        var status = target.find("img").attr("data-status");
        if (typeof status == "undefined") {
          status = "block";
        }
        that.setState({
          imgData: {
            src: src,
            width: width,
            className: className,
            status: status,
          },
        });
      } else if (target.closest(".image_holder").hasClass("image_holder")) {
        var src = target.attr("src");
        var width = target.attr("data-width");
        var className = target.attr("data-className");
        var status = target.attr("data-status");
        if (typeof status == "undefined") {
          status = "block";
        }
        that.setState({
          imgData: {
            src: src,
            width: width,
            className: className,
            status: status,
          },
        });
      }
      that.setState({ image_pop: false });
    });
    window.getData = function () {
      that.getData();
    };
  }

  getData() {
    if (data_debounce) {
      return false;
    }
    data_debounce = true;
    setTimeout(() => {
      data_debounce = false;
    }, 300);
    let inHTML = body.attr("data-html-input");
    inHTML = inHTML.replaceAll("<div><br/></div>", "<div></div>");
    inHTML = inHTML.replaceAll("<br/>", "");
    inHTML = inHTML.replaceAll("<br>", "");
    inHTML = inHTML.replaceAll("</br>", "");
    if (inHTML.startsWith("<div> </div>")) {
      inHTML = inHTML.substring(12);
    }
    const oversize = body.attr("data-oversize");
    let explain = body.attr("data-texts");
    if (typeof inHTML != "undefined" && inHTML.length != 0) {
      const newValue = Value.fromJSON({
        document: {
          nodes: [
            {
              object: "block",
              type: "block",
              nodes: [
                {
                  object: "text",
                  leaves: [
                    {
                      text: " ",
                    },
                  ],
                },
              ],
            },
          ],
        },
      });
      this.setState({ value: newValue }, () => {
        setTimeout(() => {
          const editor = this.refs.editor;
          $("#html_holder").html(inHTML);
          const html = $("#html_holder").html();
          const { document } = serializer.deserialize(html);
          editor.insertFragment(document);
          setTimeout(() => {
            this.html();
            setTimeout(() => {
              this.html();
            });
          });
        });
      });
    }
    if (typeof oversize != "undefined" && oversize.length != 0) {
      this.setState({ error_text: oversize });
    }
    if (explain != "") {
      explain = JSON.parse(body.attr("data-texts"));
      if (typeof explain != "undefined" && explain.length != 0) {
        this.setState({ explain: [...explain] });
      }
    }
  }

  hasMark(type) {
    const { value } = this.state;
    return value.activeMarks.some((mark) => mark.type == type);
  }

  hasBlock(type) {
    const { value } = this.state;
    let isActive = value.blocks.some((node) => node.type == type);

    if (["numbered-list", "bulleted-list"].includes(type)) {
      const {
        value: { document, blocks },
      } = this.state;

      if (blocks.size > 0) {
        const parent = document.getParent(blocks.first().key);
        isActive = this.hasBlock("list-item") && parent && parent.type === type;
      }
    }
    return isActive;
  }

  onChange({ value }) {
    this.setState({ value });
    if (this.state.skipHistory || value == this.prevText) {
      this.setState({ skipHistory: false });
      return;
    }
    this.prevText = value;
    previous.push(value);
    this.undoDeb = true;
    this.redoDeb = true;
    this.checkInactiveStates();
    this.checkHeightChanges(value);
  }

  checkHeightChanges(value) {
    clearTimeout(heightDebounce);
    heightDebounce = setTimeout(() => {
      if (!this.state.html_mode) {
        let string = serializer.serialize(value);
        string.replace(/"/g, "&quot;");
        string = string.replaceAll("<div></div>", "<div><br/></div>");
        body.attr("data-html-output", string);
      }
    }, 1000);
  }

  checkInactiveStates() {
    if (previous.length === 0) {
      this.setState({ undo: false });
    } else {
      this.setState({ undo: true });
    }
    if (next.length === 0) {
      this.setState({ redo: false });
    } else {
      this.setState({ redo: true });
    }
    if (this.hasMark("left")) {
      this.setState({ left: true });
    } else if (this.state.left) {
      this.setState({ left: false });
    }
    if (this.hasMark("right")) {
      this.setState({ right: true });
    } else if (this.state.right) {
      this.setState({ right: false });
    }
    if (this.hasMark("center")) {
      this.setState({ center: true });
    } else if (this.state.center) {
      this.setState({ center: false });
    }
    if (this.hasMark("uppercase")) {
      this.setState({ uppercase: true });
    } else if (this.state.uppercase) {
      this.setState({ uppercase: false });
    }
    if (this.hasMark("justify")) {
      this.setState({ justify: true });
    } else if (this.state.justify) {
      this.setState({ justify: false });
    }
    if (this.hasMark("bold")) {
      this.setState({ bold: true });
    } else if (this.state.bold) {
      this.setState({ bold: false });
    }
    if (this.hasMark("italic")) {
      this.setState({ italic: true });
    } else if (this.state.italic) {
      this.setState({ italic: false });
    }
    if (this.hasMark("underline")) {
      this.setState({ underline: true });
    } else if (this.state.underline) {
      this.setState({ underline: false });
    }
    if (this.hasMark("strike")) {
      this.setState({ strike: true });
    } else if (this.state.strike) {
      this.setState({ strike: false });
    }
    if (this.hasMark("quote")) {
      this.setState({ qoutes_selected: true });
    } else if (this.state.qoutes_selected) {
      this.setState({ qoutes_selected: false });
    }
    if (this.hasBlock("bulleted-list")) {
      this.setState({ bullets_selected: true });
    } else if (this.state.bullets_selected) {
      this.setState({ bullets_selected: false });
    }
    if (this.hasBlock("numbered-list")) {
      this.setState({ numbers_selected: true });
    } else if (this.state.numbers_selected) {
      this.setState({ numbers_selected: false });
    }
    if (this.hasMark("heading-one")) {
      this.setState({ heading_one: true });
    } else if (this.state.heading_one) {
      this.setState({ heading_one: false });
    }
    if (this.hasMark("heading-two")) {
      this.setState({ heading_two: true });
    } else if (this.state.heading_two) {
      this.setState({ heading_two: false });
    }
    if (this.hasMark("heading-three")) {
      this.setState({ heading_three: true });
    } else if (this.state.heading_three) {
      this.setState({ heading_three: false });
    }
    if (this.hasMark("heading-four")) {
      this.setState({ heading_four: true });
    } else if (this.state.heading_four) {
      this.setState({ heading_four: false });
    }
    if (this.hasMark("small")) {
      this.setState({ small: true });
    } else if (this.state.small) {
      this.setState({ small: false });
    }
    if (this.hasMark("medium")) {
      this.setState({ medium: true });
    } else if (this.state.medium) {
      this.setState({ medium: false });
    }
    if (this.hasMark("default")) {
      this.setState({ default: true });
    } else if (this.state.default) {
      this.setState({ default: false });
    }
    if (this.hasMark("big")) {
      this.setState({ big: true });
    } else if (this.state.big) {
      this.setState({ big: false });
    }
    if (this.hasMark("h1_right")) {
      this.setState({ heading_one: true, right: true });
    }
    if (this.hasMark("h2_right")) {
      this.setState({ heading_two: true, right: true });
    }
    if (this.hasMark("h3_right")) {
      this.setState({ heading_three: true, right: true });
    }
    if (this.hasMark("h4_right")) {
      this.setState({ heading_four: true, right: true });
    }
    if (this.hasMark("h1_center")) {
      this.setState({ heading_one: true, center: true });
    }
    if (this.hasMark("h2_center")) {
      this.setState({ heading_two: true, center: true });
    }
    if (this.hasMark("h3_center")) {
      this.setState({ heading_three: true, center: true });
    }
    if (this.hasMark("h4_center")) {
      this.setState({ heading_four: true, center: true });
    }
    if (this.refs.editor.isSelectionInTable()) {
      this.setState({ table: true });
    } else if (this.state.table) {
      this.setState({ table: false });
    }
  }

  undo() {
    if (previous.length === 0) {
      return;
    }
    const n = previous.pop();
    next.push(n);
    this.setState({ value: n.merge({ isNative: false }), skipHistory: true });
    if (this.undoDeb) {
      this.undoDeb = false;
      this.redoDeb = true;
      this.undo();
      this.undo();
    }
    this.checkInactiveStates();
  }

  onDrop(event, editor, next) {
    const target = getEventRange(event, editor);
    if (!target && event.type === "drop") return next();

    const transfer = getEventTransfer(event);
    const { type, text, files } = transfer;

    if (type === "files") {
      for (const file of files) {
        const reader = new FileReader();
        const [mime] = file.type.split("/");
        if (mime !== "image") continue;
        if (file.size > 2097152) {
          this.setState({ image_pop: true });
          continue;
        }

        reader.addEventListener("load", () => {
          editor.command(insertImage, reader.result, target);
        });

        reader.readAsDataURL(file);
      }
      return;
    }

    if (type === "text") {
      if (!isUrl(text)) return next();
      if (!isImage(text)) return next();
      editor.command(insertImage, text, target);
      return;
    }

    next();
  }

  uploadImage(event) {
    const editor = this.refs.editor;
    const target = getEventRange(event, editor);
    const file = event.target.files[0];

    const reader = new FileReader();
    const [mime] = file.type.split("/");
    if (mime !== "image") return;
    if (file.size > 2097152) {
      this.setState({ image_pop: true });
      return;
    }

    reader.addEventListener("load", () => {
      editor.command(insertImage, reader.result, target);
    });

    reader.readAsDataURL(file);
  }

  redo() {
    if (next.length === 0) {
      return;
    }
    const n = next.pop();
    previous.push(n);
    this.setState({ value: n.merge({ isNative: false }), skipHistory: true });
    if (this.redoDeb) {
      this.redoDeb = false;
      this.undoDeb = true;
      this.redo();
    }
    this.checkInactiveStates();
  }

  onPaste(event, editor, next) {
    const transfer = getEventTransfer(event);
    if (transfer.type == "html") {
      this.handleQuillChange(transfer.html, editor);
    } else {
      return next();
    }
  }

  renderMark(props, editor, next) {
    const { children, mark, attributes } = props;

    switch (mark.type) {
      case "uppercase":
        return (
          <span style={{ textTransform: "uppercase" }} {...attributes}>
            {children}
          </span>
        );
      case "center":
        return <Center {...attributes}>{children}</Center>;
      case "justify":
        return <Justify {...attributes}>{children}</Justify>;
      case "left":
        return <Left {...attributes}>{children}</Left>;
      case "right":
        return <Right {...attributes}>{children}</Right>;
      case "bold":
        return <strong {...attributes}>{children}</strong>;
      case "strike":
        return <strike {...attributes}>{children}</strike>;
      case "code":
        return <code {...attributes}>{children}</code>;
      case "italic":
        return <em {...attributes}>{children}</em>;
      case "quote":
        return <blockquote {...attributes}>{children}</blockquote>;
      case "underline":
        return <u {...attributes}>{children}</u>;
      case "heading-one":
        return <h1 {...attributes}>{children}</h1>;
      case "heading-two":
        return <h2 {...attributes}>{children}</h2>;
      case "heading-three":
        return <h3 {...attributes}>{children}</h3>;
      case "heading-four":
        return <h4 {...attributes}>{children}</h4>;
      case "h1_right":
        return (
          <h1 style={{ textAlign: "right" }} {...attributes}>
            {children}
          </h1>
        );
      case "h2_right":
        return (
          <h2 style={{ textAlign: "right" }} {...attributes}>
            {children}
          </h2>
        );
      case "h3_right":
        return (
          <h3 style={{ textAlign: "right" }} {...attributes}>
            {children}
          </h3>
        );
      case "h4_right":
        return (
          <h4 style={{ textAlign: "right" }} {...attributes}>
            {children}
          </h4>
        );
      case "h1_center":
        return (
          <h1 style={{ textAlign: "center" }} {...attributes}>
            {children}
          </h1>
        );
      case "h2_center":
        return (
          <h2 style={{ textAlign: "center" }} {...attributes}>
            {children}
          </h2>
        );
      case "h3_center":
        return (
          <h3 style={{ textAlign: "center" }} {...attributes}>
            {children}
          </h3>
        );
      case "h4_center":
        return (
          <h4 style={{ textAlign: "center" }} {...attributes}>
            {children}
          </h4>
        );
      case "small":
        return (
          <div style={{ fontSize: "14px" }} {...attributes}>
            {children}
          </div>
        );
      case "medium":
        return (
          <div style={{ fontSize: "18px" }} {...attributes}>
            {children}
          </div>
        );
      case "default":
        return (
          <div style={{ fontSize: "16px" }} {...attributes}>
            {children}
          </div>
        );
      case "big":
        return (
          <div style={{ fontSize: "20px" }} {...attributes}>
            {children}
          </div>
        );
      default:
        return next();
    }
  }

  renderNode(props, editor, next) {
    const { attributes, children, node, isFocused } = props;
    switch (node.type) {
      case "quote":
        return <blockquote {...attributes}>{children}</blockquote>;
      case "div":
        return <div {...attributes}>{children}</div>;
      case "code":
        return (
          <pre>
            <code {...attributes}>{children}</code>
          </pre>
        );
      case "uppercase":
        return <span style={{ textTransform: "uppercase" }}>{children}</span>;
      case "bulleted-list":
        return <ul {...attributes}>{children}</ul>;
      case "heading-one":
        return <h1 {...attributes}>{children}</h1>;
      case "iframe":
        return (
          <div
            className="iframe_holder"
            style={{
              position: "relative",
              width: "100%",
              paddingTop: "56.25%",
            }}
            {...attributes}
          >
            <iframe
              src={node.data.get("src")}
              frameBorder="0"
              allowFullScreen="1"
              {...attributes}
              style={{
                position: "absolute",
                width: "100%",
                height: "100%",
                top: "0",
                left: "0",
              }}
            ></iframe>
          </div>
        );
      case "table_cell":
        return (
          <td
            {...attributes}
            style={{
              borderColor: "black",
              borderStyle: "solid",
              borderWidth: "inherit",
              padding: ".5em",
            }}
          >
            {children}
          </td>
        );
      case "table_row":
        return (
          <tr
            {...attributes}
            style={{
              borderColor: "black",
              borderStyle: "solid",
              borderWidth: "inherit",
            }}
          >
            {children}
          </tr>
        );
      case "table":
        const { data } = node;
        let opacity;
        if (typeof data == "undefined") {
          opacity = "1px";
        } else if (typeof data.get("opacity") == "undefined") {
          opacity = "1px";
        } else if (data.get("opacity")) {
          opacity = "0px";
        } else {
          opacity = "1px";
        }
        return (
          <table
            onClick={() => {
              this.selectTable(props.node.key);
            }}
            {...attributes}
            style={{
              borderColor: "black",
              borderStyle: "solid",
              borderWidth: opacity,
              borderCollapse: "collapse",
              width: "100%",
            }}
          >
            <tbody
              style={{
                borderColor: "black",
                borderStyle: "solid",
                borderWidth: "inherit",
              }}
            >
              {children}
            </tbody>
          </table>
        );
      case "heading-two":
        return <h2 {...attributes}>{children}</h2>;
      case "heading-three":
        return <h3 {...attributes}>{children}</h3>;
      case "heading-four":
        return <h4 {...attributes}>{children}</h4>;
      case "heading-five":
        return <h5 {...attributes}>{children}</h5>;
      case "heading-six":
        return <h6 {...attributes}>{children}</h6>;
      case "break":
        return <div {...attributes}>{children}</div>;
      case "list-item":
        return <li {...attributes}>{children}</li>;
      case "numbered-list":
        return <ol {...attributes}>{children}</ol>;
      case "image": {
        const src = node.data.get("src");
        let className = node.data.get("className");
        let width = node.data.get("width");

        if (typeof className == "undefined") {
          className = "left_img";
        }
        if (typeof width == "undefined") {
          width = "auto";
        }
        return (
          <Image
            src={src}
            selected={isFocused}
            className={className}
            width={width}
            status={"block"}
          />
        );
      }
      case "image_text": {
        const src = node.data.get("src");
        let className = node.data.get("className");
        let width = node.data.get("width");
        let status = node.data.get("status");

        if (typeof className == "undefined") {
          className = "left_img";
        }
        if (typeof width == "undefined") {
          width = "auto";
        }
        if (typeof status == "undefined") {
          status = "left";
        }
        return (
          <Image_text
            src={src}
            selected={isFocused}
            className={className}
            width={width}
            status={status}
          />
        );
      }

      default: {
        return next();
      }
    }
  }

  selectTable(key) {
    if (table_debounce) {
      return;
    }
    table_debounce = true;
    setTimeout(() => {
      table_debounce = false;
    }, 200);
    this.setState({ selected_table: key });
    const table = $('table[data-key="' + key + '"]');
    if (
      table.attr("style").indexOf("border-width: 0px") != -1 ||
      table.attr("style").indexOf("border-width: 0") != -1 ||
      table.attr("style").indexOf("border-width:0px") != -1 ||
      table.attr("style").indexOf("border-width:0") != -1
    ) {
      this.setState({ border_button: true });
    } else {
      this.setState({ border_button: false });
    }
  }

  handleQuillChange(value, editor) {
    this.setState({ quillExist: true }, () => {
      this.setState({ text: value }, () => {
        const quill = $(".ql-editor");
        const cleared = quill.html();
        const { document } = serializer.deserialize(cleared);
        editor.insertFragment(document);
        setTimeout(() => {
          this.setState({ quillExist: false });
        });
      });
    });
  }

  left() {
    const editor = this.refs.editor;
    if (this.state.imgData != "") {
      if (this.state.imgData.status == "block") {
        editor.delete().insertBlock({
          type: "image",
          data: {
            src: this.state.imgData.src,
            className: "left_img",
            width: this.state.imgData.width,
            status: this.state.imgData.status,
          },
        });
      } else {
        editor.delete().insertBlock({
          type: "image_text",
          data: {
            src: this.state.imgData.src,
            className: "left_img",
            width: this.state.imgData.width,
            status: this.state.imgData.status,
          },
        });
      }
    } else {
      if (this.hasMark("h1_right")) {
        editor.removeMark("h1_right");
        editor.addMark("heading-one");
      }
      if (this.hasMark("h2_right")) {
        editor.removeMark("h2_right");
        editor.addMark("heading-two");
      }
      if (this.hasMark("h3_right")) {
        editor.removeMark("h3_right");
        editor.addMark("heading-three");
      }
      if (this.hasMark("h4_right")) {
        editor.removeMark("h4_right");
        editor.addMark("heading-four");
      }
      if (this.hasMark("h1_center")) {
        editor.removeMark("h1_center");
        editor.addMark("heading-one");
      }
      if (this.hasMark("h2_center")) {
        editor.removeMark("h2_center");
        editor.addMark("heading-two");
      }
      if (this.hasMark("h3_center")) {
        editor.removeMark("h3_center");
        editor.addMark("heading-three");
      }
      if (this.hasMark("h4_center")) {
        editor.removeMark("h4_center");
        editor.addMark("heading-four");
      }
      editor.removeMark("left");
      editor.removeMark("right");
      editor.removeMark("center");
      editor.removeMark("justify");
      editor.addMark("left");
    }
  }

  right() {
    const editor = this.refs.editor;
    if (this.state.imgData != "") {
      if (this.state.imgData.status == "block") {
        editor.delete().insertBlock({
          type: "image",
          data: {
            src: this.state.imgData.src,
            className: "right_img",
            width: this.state.imgData.width,
            status: this.state.imgData.status,
          },
        });
      } else {
        editor.delete().insertBlock({
          type: "image_text",
          data: {
            src: this.state.imgData.src,
            className: "right_img",
            width: this.state.imgData.width,
            status: this.state.imgData.status,
          },
        });
      }
    } else {
      if (this.hasMark("h1_right")) {
        editor.removeMark("h1_right");
        editor.addMark("heading-one");
      }
      if (this.hasMark("h2_right")) {
        editor.removeMark("h2_right");
        editor.addMark("heading-two");
      }
      if (this.hasMark("h3_right")) {
        editor.removeMark("h3_right");
        editor.addMark("heading-three");
      }
      if (this.hasMark("h4_right")) {
        editor.removeMark("h4_right");
        editor.addMark("heading-four");
      }
      if (this.hasMark("h1_center")) {
        editor.removeMark("h1_center");
        editor.addMark("heading-one");
      }
      if (this.hasMark("h2_center")) {
        editor.removeMark("h2_center");
        editor.addMark("heading-two");
      }
      if (this.hasMark("h3_center")) {
        editor.removeMark("h3_center");
        editor.addMark("heading-three");
      }
      if (this.hasMark("h4_center")) {
        editor.removeMark("h4_center");
        editor.addMark("heading-four");
      }
      editor.removeMark("left");
      editor.removeMark("right");
      editor.removeMark("center");
      editor.removeMark("justify");
      editor.addMark("right");
    }
  }

  center() {
    const editor = this.refs.editor;
    if (this.state.imgData != "") {
      if (this.state.imgData.status == "block") {
        editor.delete().insertBlock({
          type: "image",
          data: {
            src: this.state.imgData.src,
            className: "center_img",
            width: this.state.imgData.width,
            status: this.state.imgData.status,
          },
        });
      } else {
        editor.delete().insertBlock({
          type: "image_text",
          data: {
            src: this.state.imgData.src,
            className: "center_img",
            width: this.state.imgData.width,
            status: this.state.imgData.status,
          },
        });
      }
    } else {
      if (this.hasMark("h1_right")) {
        editor.removeMark("h1_right");
        editor.addMark("heading-one");
      }
      if (this.hasMark("h2_right")) {
        editor.removeMark("h2_right");
        editor.addMark("heading-two");
      }
      if (this.hasMark("h3_right")) {
        editor.removeMark("h3_right");
        editor.addMark("heading-three");
      }
      if (this.hasMark("h4_right")) {
        editor.removeMark("h4_right");
        editor.addMark("heading-four");
      }
      if (this.hasMark("h1_center")) {
        editor.removeMark("h1_center");
        editor.addMark("heading-one");
      }
      if (this.hasMark("h2_center")) {
        editor.removeMark("h2_center");
        editor.addMark("heading-two");
      }
      if (this.hasMark("h3_center")) {
        editor.removeMark("h3_center");
        editor.addMark("heading-three");
      }
      if (this.hasMark("h4_center")) {
        editor.removeMark("h4_center");
        editor.addMark("heading-four");
      }
      editor.removeMark("left");
      editor.removeMark("right");
      editor.removeMark("center");
      editor.removeMark("justify");
      editor.addMark("center");
    }
  }

  justify() {
    const editor = this.refs.editor;
    if (this.state.imgData == "") {
      editor.removeMark("left");
      editor.removeMark("right");
      editor.removeMark("center");
      editor.removeMark("justify");
      editor.addMark("justify");
    }
  }

  imageFull() {
    const editor = this.refs.editor;
    if (this.state.imgData.status == "block") {
      editor.delete().insertBlock({
        type: "image",
        data: {
          src: this.state.imgData.src,
          className: this.state.imgData.className,
          width: "100%",
          status: this.state.imgData.status,
        },
      });
    } else {
      editor.delete().insertBlock({
        type: "image_text",
        data: {
          src: this.state.imgData.src,
          className: this.state.imgData.className,
          width: "100%",
          status: this.state.imgData.status,
        },
      });
    }
  }

  imagesmall() {
    const editor = this.refs.editor;
    if (this.state.imgData.status == "block") {
      editor.delete().insertBlock({
        type: "image",
        data: {
          src: this.state.imgData.src,
          className: this.state.imgData.className,
          width: "20%",
          status: this.state.imgData.status,
        },
      });
    } else {
      editor.delete().insertBlock({
        type: "image_text",
        data: {
          src: this.state.imgData.src,
          className: this.state.imgData.className,
          width: "20%",
          status: this.state.imgData.status,
        },
      });
    }
  }

  imageThird() {
    const editor = this.refs.editor;
    if (this.state.imgData.status == "block") {
      editor.delete().insertBlock({
        type: "image",
        data: {
          src: this.state.imgData.src,
          className: this.state.imgData.className,
          width: "33.33%",
          status: this.state.imgData.status,
        },
      });
    } else {
      editor.delete().insertBlock({
        type: "image_text",
        data: {
          src: this.state.imgData.src,
          className: this.state.imgData.className,
          width: "33.33%",
          status: this.state.imgData.status,
        },
      });
    }
  }

  imageHalf() {
    const editor = this.refs.editor;
    if (this.state.imgData.status == "block") {
      editor.delete().insertBlock({
        type: "image",
        data: {
          src: this.state.imgData.src,
          className: this.state.imgData.className,
          width: "50%",
          status: this.state.imgData.status,
        },
      });
    } else {
      editor.delete().insertBlock({
        type: "image_text",
        data: {
          src: this.state.imgData.src,
          className: this.state.imgData.className,
          width: "50%",
          status: this.state.imgData.status,
        },
      });
    }
  }

  float_left() {
    const editor = this.refs.editor;
    editor.delete().insertBlock({
      type: "image_text",
      data: {
        src: this.state.imgData.src,
        className: this.state.imgData.className,
        width: this.state.imgData.width,
        status: "left",
      },
    });
  }

  float_right() {
    const editor = this.refs.editor;
    editor.delete().insertBlock({
      type: "image_text",
      data: {
        src: this.state.imgData.src,
        className: this.state.imgData.className,
        width: this.state.imgData.width,
        status: "right",
      },
    });
  }

  block() {
    const editor = this.refs.editor;
    editor.delete().insertBlock({
      type: "image",
      data: {
        src: this.state.imgData.src,
        className: this.state.imgData.className,
        width: this.state.imgData.width,
        status: "block",
      },
    });
  }

  onInsertTable() {
    const editor = this.refs.editor;
    editor.insertTable();
  }

  onBorderClear() {
    const editor = this.refs.editor;
    const table = $('table[data-key="' + this.state.selected_table + '"]');
    let width = "0px";
    let border_button;
    if (
      table.attr("style").indexOf("border-width: 0px") != -1 ||
      table.attr("style").indexOf("border-width: 0") != -1 ||
      table.attr("style").indexOf("border-width:0px") != -1 ||
      table.attr("style").indexOf("border-width:0") != -1
    ) {
      width = "1px";
      editor.setNodeByKey(this.state.selected_table, {
        data: { opacity: false },
      });
      border_button = false;
    } else {
      editor.setNodeByKey(this.state.selected_table, {
        data: { opacity: true },
      });
      border_button = true;
    }
    table.css({ "border-width": width });
    setTimeout(() => {
      this.setState({ border_button });
    });
  }

  onInsertColumn() {
    const editor = this.refs.editor;
    editor.insertColumn();
  }

  onInsertRow() {
    const editor = this.refs.editor;
    editor.insertRow();
  }

  onRemoveColumn() {
    const editor = this.refs.editor;
    editor.removeColumn();
  }

  onRemoveRow() {
    const editor = this.refs.editor;
    editor.removeRow();
  }

  onRemoveTable() {
    const editor = this.refs.editor;
    editor.removeTable();
  }

  bold() {
    const editor = this.refs.editor;
    editor.toggleMark("bold");
  }

  uppercase() {
    const editor = this.refs.editor;
    editor.toggleMark("uppercase");
  }

  italic() {
    const editor = this.refs.editor;
    editor.toggleMark("italic");
  }

  underline() {
    const editor = this.refs.editor;
    editor.toggleMark("underline");
  }

  strike() {
    const editor = this.refs.editor;
    editor.toggleMark("strike");
  }

  quote() {
    const editor = this.refs.editor;
    editor.toggleMark("quote");
  }

  headingOne() {
    const editor = this.refs.editor;
    editor.removeMark("small");
    editor.removeMark("medium");
    editor.removeMark("default");
    editor.removeMark("big");
    if (this.hasMark("h1_right")) {
      editor.removeMark("h1_right");
      editor.addMark("right");
      return;
    }
    if (this.hasMark("h2_right")) {
      editor.removeMark("h2_right");
      editor.addMark("right");
    }
    if (this.hasMark("h3_right")) {
      editor.removeMark("h3_right");
      editor.addMark("right");
    }
    if (this.hasMark("h4_right")) {
      editor.removeMark("h4_right");
      editor.addMark("right");
    }
    if (this.hasMark("h1_center")) {
      editor.removeMark("h1_center");
      editor.addMark("center");
      return;
    }
    if (this.hasMark("h2_center")) {
      editor.removeMark("h2_center");
      editor.addMark("center");
    }
    if (this.hasMark("h3_center")) {
      editor.removeMark("h3_center");
      editor.addMark("center");
    }
    if (this.hasMark("h4_center")) {
      editor.removeMark("h4_center");
      editor.addMark("center");
    }
    if (this.hasMark("heading-one")) {
      editor.removeMark("heading-one");
      return;
    }
    editor.removeMark("heading-one");
    editor.removeMark("heading-two");
    editor.removeMark("heading-three");
    editor.removeMark("heading-four");
    editor.addMark("heading-one");
  }

  headingTwo() {
    const editor = this.refs.editor;
    editor.removeMark("small");
    editor.removeMark("medium");
    editor.removeMark("default");
    editor.removeMark("big");
    if (this.hasMark("h1_right")) {
      editor.removeMark("h1_right");
      editor.addMark("right");
    }
    if (this.hasMark("h2_right")) {
      editor.removeMark("h2_right");
      editor.addMark("right");
      return;
    }
    if (this.hasMark("h3_right")) {
      editor.removeMark("h3_right");
      editor.addMark("right");
    }
    if (this.hasMark("h4_right")) {
      editor.removeMark("h4_right");
      editor.addMark("right");
    }
    if (this.hasMark("h1_center")) {
      editor.removeMark("h1_center");
      editor.addMark("center");
    }
    if (this.hasMark("h2_center")) {
      editor.removeMark("h2_center");
      editor.addMark("center");
      return;
    }
    if (this.hasMark("h3_center")) {
      editor.removeMark("h3_center");
      editor.addMark("center");
    }
    if (this.hasMark("h4_center")) {
      editor.removeMark("h4_center");
      editor.addMark("center");
    }
    if (this.hasMark("heading-two")) {
      editor.removeMark("heading-two");
      return;
    }
    editor.removeMark("heading-one");
    editor.removeMark("heading-two");
    editor.removeMark("heading-three");
    editor.removeMark("heading-four");
    editor.addMark("heading-two");
  }

  headingThree() {
    const editor = this.refs.editor;
    editor.removeMark("small");
    editor.removeMark("medium");
    editor.removeMark("default");
    editor.removeMark("big");
    if (this.hasMark("h1_right")) {
      editor.removeMark("h1_right");
      editor.addMark("right");
    }
    if (this.hasMark("h2_right")) {
      editor.removeMark("h2_right");
      editor.addMark("right");
    }
    if (this.hasMark("h3_right")) {
      editor.removeMark("h3_right");
      editor.addMark("right");
      return;
    }
    if (this.hasMark("h4_right")) {
      editor.removeMark("h4_right");
      editor.addMark("right");
    }
    if (this.hasMark("h1_center")) {
      editor.removeMark("h1_center");
      editor.addMark("center");
    }
    if (this.hasMark("h2_center")) {
      editor.removeMark("h2_center");
      editor.addMark("center");
    }
    if (this.hasMark("h3_center")) {
      editor.removeMark("h3_center");
      editor.addMark("center");
      return;
    }
    if (this.hasMark("h4_center")) {
      editor.removeMark("h4_center");
      editor.addMark("center");
    }
    if (this.hasMark("heading-three")) {
      editor.removeMark("heading-three");
      return;
    }
    editor.removeMark("heading-one");
    editor.removeMark("heading-two");
    editor.removeMark("heading-three");
    editor.removeMark("heading-four");
    editor.addMark("heading-three");
  }

  headingFour() {
    const editor = this.refs.editor;
    editor.removeMark("small");
    editor.removeMark("medium");
    editor.removeMark("default");
    editor.removeMark("big");
    if (this.hasMark("h1_right")) {
      editor.removeMark("h1_right");
      editor.addMark("right");
    }
    if (this.hasMark("h2_right")) {
      editor.removeMark("h2_right");
      editor.addMark("right");
    }
    if (this.hasMark("h3_right")) {
      editor.removeMark("h3_right");
      editor.addMark("right");
    }
    if (this.hasMark("h4_right")) {
      editor.removeMark("h4_right");
      editor.addMark("right");
      return;
    }
    if (this.hasMark("h1_center")) {
      editor.removeMark("h1_center");
      editor.addMark("center");
    }
    if (this.hasMark("h2_center")) {
      editor.removeMark("h2_center");
      editor.addMark("center");
    }
    if (this.hasMark("h3_center")) {
      editor.removeMark("h3_center");
      editor.addMark("center");
    }
    if (this.hasMark("h4_center")) {
      editor.removeMark("h4_center");
      editor.addMark("center");
      return;
    }
    if (this.hasMark("heading-four")) {
      editor.removeMark("heading-four");
      return;
    }
    editor.removeMark("heading-one");
    editor.removeMark("heading-two");
    editor.removeMark("heading-three");
    editor.removeMark("heading-four");
    editor.addMark("heading-four");
  }

  small() {
    const editor = this.refs.editor;
    if (this.hasMark("h1_right")) {
      editor.removeMark("h1_right");
      editor.addMark("right");
    }
    if (this.hasMark("h2_right")) {
      editor.removeMark("h2_right");
      editor.addMark("right");
    }
    if (this.hasMark("h3_right")) {
      editor.removeMark("h3_right");
      editor.addMark("right");
    }
    if (this.hasMark("h4_right")) {
      editor.removeMark("h4_right");
      editor.addMark("right");
    }
    if (this.hasMark("h1_center")) {
      editor.removeMark("h1_center");
      editor.addMark("center");
    }
    if (this.hasMark("h2_center")) {
      editor.removeMark("h2_center");
      editor.addMark("center");
    }
    if (this.hasMark("h3_center")) {
      editor.removeMark("h3_center");
      editor.addMark("center");
    }
    if (this.hasMark("h4_center")) {
      editor.removeMark("h4_center");
      editor.addMark("center");
    }
    if (this.hasMark("small")) {
      editor.removeMark("small");
      return;
    }
    editor.removeMark("small");
    editor.removeMark("medium");
    editor.removeMark("big");
    editor.removeMark("default");
    editor.addMark("small");
  }

  default() {
    const editor = this.refs.editor;
    if (this.hasMark("h1_right")) {
      editor.removeMark("h1_right");
      editor.addMark("right");
    }
    if (this.hasMark("h2_right")) {
      editor.removeMark("h2_right");
      editor.addMark("right");
    }
    if (this.hasMark("h3_right")) {
      editor.removeMark("h3_right");
      editor.addMark("right");
    }
    if (this.hasMark("h4_right")) {
      editor.removeMark("h4_right");
      editor.addMark("right");
    }
    if (this.hasMark("h1_center")) {
      editor.removeMark("h1_center");
      editor.addMark("center");
    }
    if (this.hasMark("h2_center")) {
      editor.removeMark("h2_center");
      editor.addMark("center");
    }
    if (this.hasMark("h3_center")) {
      editor.removeMark("h3_center");
      editor.addMark("center");
    }
    if (this.hasMark("h4_center")) {
      editor.removeMark("h4_center");
      editor.addMark("center");
    }
    if (this.hasMark("default")) {
      editor.removeMark("default");
      return;
    }
    editor.removeMark("small");
    editor.removeMark("medium");
    editor.removeMark("big");
    editor.removeMark("default");
    editor.addMark("default");
  }

  medium() {
    const editor = this.refs.editor;
    if (this.hasMark("h1_right")) {
      editor.removeMark("h1_right");
      editor.addMark("right");
    }
    if (this.hasMark("h2_right")) {
      editor.removeMark("h2_right");
      editor.addMark("right");
    }
    if (this.hasMark("h3_right")) {
      editor.removeMark("h3_right");
      editor.addMark("right");
    }
    if (this.hasMark("h4_right")) {
      editor.removeMark("h4_right");
      editor.addMark("right");
    }
    if (this.hasMark("h1_center")) {
      editor.removeMark("h1_center");
      editor.addMark("center");
    }
    if (this.hasMark("h2_center")) {
      editor.removeMark("h2_center");
      editor.addMark("center");
    }
    if (this.hasMark("h3_center")) {
      editor.removeMark("h3_center");
      editor.addMark("center");
    }
    if (this.hasMark("h4_center")) {
      editor.removeMark("h4_center");
      editor.addMark("center");
    }
    if (this.hasMark("medium")) {
      editor.removeMark("medium");
      return;
    }
    editor.removeMark("small");
    editor.removeMark("medium");
    editor.removeMark("big");
    editor.removeMark("default");
    editor.addMark("medium");
  }

  big() {
    const editor = this.refs.editor;
    if (this.hasMark("h1_right")) {
      editor.removeMark("h1_right");
      editor.addMark("right");
    }
    if (this.hasMark("h2_right")) {
      editor.removeMark("h2_right");
      editor.addMark("right");
    }
    if (this.hasMark("h3_right")) {
      editor.removeMark("h3_right");
      editor.addMark("right");
    }
    if (this.hasMark("h4_right")) {
      editor.removeMark("h4_right");
      editor.addMark("right");
    }
    if (this.hasMark("h1_center")) {
      editor.removeMark("h1_center");
      editor.addMark("center");
    }
    if (this.hasMark("h2_center")) {
      editor.removeMark("h2_center");
      editor.addMark("center");
    }
    if (this.hasMark("h3_center")) {
      editor.removeMark("h3_center");
      editor.addMark("center");
    }
    if (this.hasMark("h4_center")) {
      editor.removeMark("h4_center");
      editor.addMark("center");
    }
    if (this.hasMark("big")) {
      editor.removeMark("big");
      return;
    }
    editor.removeMark("small");
    editor.removeMark("medium");
    editor.removeMark("big");
    editor.removeMark("default");
    editor.addMark("big");
  }

  onClickBlock(event, type) {
    const editor = this.refs.editor;
    const { value } = this.state;
    const { document } = value;

    if (type != "bulleted-list" && type != "numbered-list") {
      const isActive = this.hasBlock(type);
      const isList = this.hasBlock("list-item");

      if (isList) {
        editor
          .setBlocks(isActive ? DEFAULT_NODE : type)
          .unwrapBlock("bulleted-list")
          .unwrapBlock("numbered-list");
      } else {
        editor.setBlocks(isActive ? DEFAULT_NODE : type);
      }
    } else {
      const isList = this.hasBlock("list-item");
      const isType = value.blocks.some((block) => {
        return !!document.getClosest(
          block.key,
          (parent) => parent.type == type
        );
      });

      if (isList && isType) {
        editor
          .setBlocks(DEFAULT_NODE)
          .unwrapBlock("bulleted-list")
          .unwrapBlock("numbered-list");
      } else if (isList) {
        editor
          .unwrapBlock(
            type == "bulleted-list" ? "numbered-list" : "bulleted-list"
          )
          .wrapBlock(type);
      } else {
        editor.setBlocks("list-item").wrapBlock(type);
      }
    }
  }

  removeStyle() {
    const editor = this.refs.editor;
    editor
      .setBlocks(DEFAULT_NODE)
      .unwrapBlock("bulleted-list")
      .unwrapBlock("numbered-list")
      .removeMark("left")
      .removeMark("right")
      .removeMark("center")
      .removeMark("small")
      .removeMark("medium")
      .removeMark("default")
      .removeMark("justify")
      .removeMark("big")
      .removeMark("heading-one")
      .removeMark("heading-two")
      .removeMark("heading-three")
      .removeMark("heading-four")
      .removeMark("bold")
      .removeMark("italic")
      .removeMark("underline")
      .removeMark("strike")
      .removeMark("quote")
      .removeMark("h1_center")
      .removeMark("h2_center")
      .removeMark("h3_center")
      .removeMark("h4_center")
      .removeMark("h1_right")
      .removeMark("h2_right")
      .removeMark("h3_right")
      .removeMark("h4_right")
      .removeMark("uppercase");
  }

  iframe() {
    this.setState({
      frame_text: "",
      frame_pop: true,
    });
  }

  handleFrame(bool, event) {
    if (bool) {
      const href = this.state.frame_text;
      const editor = this.refs.editor;

      if (href == "") {
        this.setState({
          frame_pop: false,
        });
        return;
      }

      this.setState(
        {
          frame_pop: false,
        },
        () => {
          const target = getEventRange(event, editor);
          editor.command(insertIframe, href, target);
        }
      );
    } else {
      this.setState({
        frame_pop: false,
      });
    }
  }

  html() {
    const value = this.state.value;
    const editor = this.refs.editor;

    if (this.state.html_mode) {
      const string = Plain.serialize(value);
      const newValue = Value.fromJSON({
        document: {
          nodes: [
            {
              object: "block",
              type: "block",
              nodes: [
                {
                  object: "text",
                  leaves: [
                    {
                      text: " ",
                    },
                  ],
                },
              ],
            },
          ],
        },
      });
      this.setState({ value: newValue, html_mode: false }, () => {
        $("#html_holder").html(string);
        let html = $("#html_holder").html();
        console.log("html", html);
        const { document } = serializer.deserialize(html);
        editor.insertFragment(document);
      });
    } else {
      const string = serializer.serialize(value);
      const newValue = Value.fromJSON({
        document: {
          nodes: [
            {
              object: "block",
              type: "block",
              nodes: [
                {
                  object: "text",
                  leaves: [
                    {
                      text: string,
                    },
                  ],
                },
              ],
            },
          ],
        },
      });
      this.setState({ value: newValue, html_mode: true });
    }
  }

  trimRows() {
    const value = this.state.value;
    const editor = this.refs.editor;
    let string = serializer.serialize(value);

    const recursor = (prev) => {
      const i = string.indexOf("<div></div>");
      if (i != -1) {
        if (prev) {
          if (prev + 11 == i) {
            string = string.replace("<div></div>", "<-remover->");
          } else {
            string = string.replace("<div></div>", "<-replace->");
          }
        } else {
          string = string.replace("<div></div>", "<-replace->");
        }
        recursor(i);
      }
    };
    recursor();

    string = string.replaceAll("<-replace->", "<div></div>");
    string = string.replaceAll("<-remover->", "");

    const newValue = Value.fromJSON({
      document: {
        nodes: [
          {
            object: "block",
            type: "block",
            nodes: [
              {
                object: "text",
                leaves: [
                  {
                    text: " ",
                  },
                ],
              },
            ],
          },
        ],
      },
    });

    this.setState({ value: newValue }, () => {
      $("#html_holder").html(string);
      const html = $("#html_holder").html();
      const { document } = serializer.deserialize(html);
      editor.insertFragment(document);
    });
  }

  quotes() {
    const value = this.state.value;
    const editor = this.refs.editor;
    let string = serializer.serialize(value);
    let open = false;

    const recursor = (prev) => {
      const i = string.indexOf("&quot;");
      if (i != -1) {
        if (open) {
          string = string.replace("&quot;", "“");
          open = false;
        } else {
          string = string.replace("&quot;", "„");
          open = true;
        }

        recursor(i);
      }
    };
    recursor();

    const newValue = Value.fromJSON({
      document: {
        nodes: [
          {
            object: "block",
            type: "block",
            nodes: [
              {
                object: "text",
                leaves: [
                  {
                    text: " ",
                  },
                ],
              },
            ],
          },
        ],
      },
    });

    this.setState({ value: newValue }, () => {
      $("#html_holder").html(string);
      const html = $("#html_holder").html();
      const { document } = serializer.deserialize(html);
      editor.insertFragment(document);
    });
  }

  explain(i) {
    if (typeof this.state.explain[i] != "undefined") {
      return this.state.explain[i];
    } else {
      return "";
    }
  }

  onKeyUp(event) {
    if (event.which == 13) {
      // if (!event.shiftKey && !this.hasBlock('bulleted-list') && !this.hasBlock('numbered-list')) {
      //   this.refs.editor.insertBlock({
      //     type: 'break'
      //   })
      // }
      const node = $(window.getSelection().anchorNode).closest("div");
      const top = node.offset().top;
      const win_height = window.innerHeight;
      const road = top - win_height / 2;
      window.scrollTo(0, road);
    } else if (event.which == 32) {
      this.refs.editor.blur();
      setTimeout(() => {
        this.refs.editor.focus();
      });
    }
  }

  preventJumping(event) {
    if (jumping_flag) {
      setTimeout(() => {
        parent.startJump();
        jumping_flag = false;
      });
    } else {
      jumping_flag = true;
      event.preventDefault();
      parent.stopJump();
      this.refs.upload_input.click();
    }
  }

  render() {
    return (
      <div className="main_editor" onKeyUp={this.onKeyUp.bind(this)}>
        <div className="toolbar">
          <div
            className={
              "buttons_block" + (this.state.html_mode ? " inactive" : "")
            }
          >
            <button
              onClick={this.undo}
              className={"button undo" + (this.state.undo ? "" : " inactive")}
            >
              <Icon icon={arrow_left} />
              <div className="explain_pop">{this.explain(0)}</div>
            </button>
            <button
              onClick={this.redo}
              className={"button redo" + (this.state.redo ? "" : " inactive")}
            >
              <Icon icon={arrow_right} />
              <div className="explain_pop">{this.explain(1)}</div>
            </button>
          </div>
          <div
            className={
              "buttons_block" + (this.state.html_mode ? " inactive" : "")
            }
          >
            <button
              onClick={this.left.bind(this)}
              className={"button left" + (this.state.left ? " active" : "")}
            >
              <Icon icon={text_left} />
              <div className="explain_pop">{this.explain(2)}</div>
            </button>
            <button
              onClick={this.center.bind(this)}
              className={"button center" + (this.state.center ? " active" : "")}
            >
              <Icon icon={text_center} />
              <div className="explain_pop">{this.explain(3)}</div>
            </button>
            <button
              onClick={this.justify.bind(this)}
              className={
                "button justify" + (this.state.justify ? " active" : "")
              }
            >
              <Icon icon={text_justify} />
              <div className="explain_pop">{this.explain(4)}</div>
            </button>
            <button
              onClick={this.right.bind(this)}
              className={"button right" + (this.state.right ? " active" : "")}
            >
              <Icon icon={text_right} />
              <div className="explain_pop">{this.explain(5)}</div>
            </button>
          </div>
          <div
            className={
              "buttons_block" + (this.state.html_mode ? " inactive" : "")
            }
          >
            <button
              onClick={this.bold.bind(this)}
              className={"button bold" + (this.state.bold ? " active" : "")}
            >
              <Icon icon={ic_format_bold} />
              <div className="explain_pop">{this.explain(6)}</div>
            </button>
            <button
              onClick={this.italic.bind(this)}
              className={"button italic" + (this.state.italic ? " active" : "")}
            >
              <Icon icon={ic_format_italic} />
              <div className="explain_pop">{this.explain(7)}</div>
            </button>
            <button
              onClick={this.underline.bind(this)}
              className={
                "button underline" + (this.state.underline ? " active" : "")
              }
            >
              <Icon icon={ic_format_underlined} />
              <div className="explain_pop">{this.explain(8)}</div>
            </button>
            <button
              onClick={this.strike.bind(this)}
              className={"button strike" + (this.state.strike ? " active" : "")}
            >
              <Icon icon={ic_strikethrough_s} />
              <div className="explain_pop">{this.explain(9)}</div>
            </button>
            <button
              onClick={this.uppercase.bind(this)}
              className={
                "button uppercase" + (this.state.uppercase ? " active" : "")
              }
            >
              <Icon icon={textHeight} />
              <div className="explain_pop">{this.explain(41)}</div>
            </button>
          </div>
          <div
            className={
              "buttons_block" + (this.state.html_mode ? " inactive" : "")
            }
          >
            <button
              onClick={this.quote.bind(this)}
              className={
                "button quote" + (this.state.qoutes_selected ? " active" : "")
              }
            >
              <Icon icon={ic_format_quote} />
              <div className="explain_pop">{this.explain(11)}</div>
            </button>
            <button
              onClick={(event) => this.onClickBlock(event, "bulleted-list")}
              className={
                "button bulleted" +
                (this.state.bullets_selected ? " active" : "")
              }
            >
              <Icon icon={ic_format_list_bulleted} />
              <div className="explain_pop">{this.explain(12)}</div>
            </button>
            <button
              onClick={(event) => this.onClickBlock(event, "numbered-list")}
              className={
                "button numbered" +
                (this.state.numbers_selected ? " active" : "")
              }
            >
              <Icon icon={ic_format_list_numbered} />
              <div className="explain_pop">{this.explain(13)}</div>
            </button>
          </div>
          <div
            className={
              "buttons_block" + (this.state.html_mode ? " inactive" : "")
            }
          >
            <label
              htmlFor="filer"
              className="button"
              onClick={(event) => this.preventJumping(event)}
            >
              <input
                type="file"
                id="filer"
                ref="upload_input"
                onChange={this.uploadImage.bind(this)}
              />
              <Icon icon={picture} />
            </label>
            <div className="explain_pop">{this.explain(14)}</div>
            <div
              className={"image_pop" + (this.state.image_pop ? " open" : "")}
            >
              <div className="just_text_holder">{this.state.error_text}</div>
              <div className="buttons_block right">
                <button
                  onClick={() => {
                    this.setState({ image_pop: false });
                  }}
                  className="button agree"
                >
                  <Icon icon={cross} />
                </button>
              </div>
            </div>
            <button
              onClick={this.imagesmall.bind(this)}
              className={
                "button imagesmall" +
                (this.state.imgData == ""
                  ? " inactive"
                  : this.state.imgData.width == "20%"
                  ? " active"
                  : "")
              }
            >
              <div className="icon">
                <svg
                  id="Layer_141894948"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 1414 1184"
                >
                  <path d="M0,0v1184h1414V0H0z M163.3,1038.2V145.8h1087.3v892.5H163.3z" />
                  <rect x="253.7" y="507.5" width="240.3" height="169.5" />
                </svg>
              </div>
              <div className="explain_pop">{this.explain(15)}</div>
            </button>
            <button
              onClick={this.imageThird.bind(this)}
              className={
                "button imageThird" +
                (this.state.imgData == ""
                  ? " inactive"
                  : this.state.imgData.width == "33.33%"
                  ? " active"
                  : "")
              }
            >
              <div className="icon">
                <svg
                  id="Layer_1xfg"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 1414 1184"
                >
                  <path d="M0,0v1184h1414V0H0z M163.3,1038.2V145.8h1087.3v892.5H163.3z" />
                  <rect x="239.7" y="483.5" width="348.1" height="245.5" />
                </svg>
              </div>
              <div className="explain_pop">{this.explain(16)}</div>
            </button>
            <button
              onClick={this.imageHalf.bind(this)}
              className={
                "button imageHalf" +
                (this.state.imgData == ""
                  ? " inactive"
                  : this.state.imgData.width == "50%"
                  ? " active"
                  : "")
              }
            >
              <div className="icon">
                <svg
                  id="Layer_g841"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 1414 1184"
                >
                  <path d="M0,0v1184h1414V0H0z M163.3,1038.2V145.8h1087.3v892.5H163.3z" />
                  <rect x="239.7" y="381.5" width="598.3" height="422" />
                </svg>
              </div>
              <div className="explain_pop">{this.explain(17)}</div>
            </button>
            <button
              onClick={this.imageFull.bind(this)}
              className={
                "button imageFull" +
                (this.state.imgData == ""
                  ? " inactive"
                  : this.state.imgData.width == "100%"
                  ? " active"
                  : "")
              }
            >
              <div className="icon">
                <svg
                  id="Layer_1sfsdf"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 1414 1184"
                >
                  <path d="M0,0v1184h1414V0H0z M163.3,1038.2V145.8h1087.3v892.5H163.3z" />
                  <rect x="243.7" y="265.5" width="926.6" height="653.5" />
                </svg>
              </div>
              <div className="explain_pop">{this.explain(18)}</div>
            </button>
            <button
              onClick={this.float_left.bind(this)}
              className={
                "button float_left" +
                (this.state.imgData == ""
                  ? " inactive"
                  : this.state.imgData.status == "left"
                  ? " active"
                  : "")
              }
            >
              <div className="icon">
                <svg
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                >
                  <path d="M501.333,298.667H10.667C4.771,298.667,0,303.438,0,309.333v21.333c0,5.896,4.771,10.667,10.667,10.667h490.667 c5.896,0,10.667-4.771,10.667-10.667v-21.333C512,303.438,507.229,298.667,501.333,298.667z" />
                  <path d="M501.333,192h-192c-5.896,0-10.667,4.771-10.667,10.667V224c0,5.896,4.771,10.667,10.667,10.667h192 c5.896,0,10.667-4.771,10.667-10.667v-21.333C512,196.771,507.229,192,501.333,192z" />
                  <path d="M501.333,85.333h-192c-5.896,0-10.667,4.771-10.667,10.667v21.333c0,5.896,4.771,10.667,10.667,10.667h192 c5.896,0,10.667-4.771,10.667-10.667V96C512,90.104,507.229,85.333,501.333,85.333z" />
                  <path d="M330.667,405.333h-320C4.771,405.333,0,410.104,0,416v21.333C0,443.229,4.771,448,10.667,448h320 c5.896,0,10.667-4.771,10.667-10.667V416C341.333,410.104,336.563,405.333,330.667,405.333z" />
                  <path d="M10.667,256h74.667h160c5.896,0,10.667-4.771,10.667-10.667v-32V74.667C256,68.771,251.229,64,245.333,64H10.667 C4.771,64,0,68.771,0,74.667v170.667C0,251.229,4.771,256,10.667,256z M21.333,85.333h213.333v102.25l-56.458-56.458 c-4.167-4.167-10.917-4.167-15.083,0l-33.833,33.833c-4.167,4.167-4.167,10.917,0,15.083l10.667,10.667 c2.01,2.021,3.125,4.698,3.125,7.542s-1.115,5.521-3.125,7.542c-4.042,4.021-11.042,4.021-15.083,0l-10.667-10.667l-32-32 c-4.167-4.167-10.917-4.167-15.083,0l-45.792,45.792V85.333z" />
                  <circle cx="106.667" cy="128" r="21.333" />
                </svg>
              </div>
              <div className="explain_pop">{this.explain(19)}</div>
            </button>
            <button
              onClick={this.float_right.bind(this)}
              className={
                "button float_right" +
                (this.state.imgData == ""
                  ? " inactive"
                  : this.state.imgData.status == "right"
                  ? " active"
                  : "")
              }
            >
              <div className="icon">
                <svg
                  id="Layer_2"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                >
                  <path d="M501.333,298.667H10.667C4.771,298.667,0,303.438,0,309.333v21.333c0,5.896,4.771,10.667,10.667,10.667h490.667 c5.896,0,10.667-4.771,10.667-10.667v-21.333C512,303.438,507.229,298.667,501.333,298.667z" />
                  <path d="M10.667,234.667h192c5.896,0,10.667-4.771,10.667-10.667v-21.333c0-5.896-4.771-10.667-10.667-10.667h-192 C4.771,192,0,196.771,0,202.667V224C0,229.896,4.771,234.667,10.667,234.667z" />
                  <path d="M10.667,128h192c5.896,0,10.667-4.771,10.667-10.667V96c0-5.896-4.771-10.667-10.667-10.667h-192 C4.771,85.333,0,90.104,0,96v21.333C0,123.229,4.771,128,10.667,128z" />
                  <path d="M330.667,405.333h-320C4.771,405.333,0,410.104,0,416v21.333C0,443.229,4.771,448,10.667,448h320 c5.896,0,10.667-4.771,10.667-10.667V416C341.333,410.104,336.563,405.333,330.667,405.333z" />
                  <path d="M501.333,64H266.667C260.771,64,256,68.771,256,74.667v170.667c0,5.896,4.771,10.667,10.667,10.667h74.667h160 c5.896,0,10.667-4.771,10.667-10.667v-32V74.667C512,68.771,507.229,64,501.333,64z M490.667,187.583l-56.458-56.458 c-4.167-4.167-10.917-4.167-15.083,0l-33.833,33.833c-4.167,4.167-4.167,10.917,0,15.083l10.667,10.667 c2.021,2.021,3.125,4.698,3.125,7.542s-1.104,5.521-3.125,7.542c-4.042,4.021-11.042,4.021-15.083,0l-10.667-10.667l-32-32 c-4.167-4.167-10.917-4.167-15.083,0l-45.792,45.792V85.333h213.333V187.583z" />
                  <circle cx="362.667" cy="128" r="21.333" />
                </svg>
              </div>
              <div className="explain_pop">{this.explain(20)}</div>
            </button>
            <button
              onClick={this.block.bind(this)}
              className={
                "button block" +
                (this.state.imgData == ""
                  ? " inactive"
                  : this.state.imgData.status == "block"
                  ? " active"
                  : "")
              }
            >
              <div className="icon">
                <svg
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                >
                  <path d="M501.3,298.7H10.7c-5.9,0-10.7,4.8-10.7,10.7v21.3c0,5.9,4.8,10.7,10.7,10.7h490.7c5.9,0,10.7-4.8,10.7-10.7v-21.3 C512,303.4,507.2,298.7,501.3,298.7z" />
                  <path d="M330.7,405.3h-320C4.8,405.3,0,410.1,0,416v21.3c0,5.9,4.8,10.7,10.7,10.7h320c5.9,0,10.7-4.8,10.7-10.7V416 C341.3,410.1,336.6,405.3,330.7,405.3z" />
                  <path d="M10.7,256h74.7h160c5.9,0,10.7-4.8,10.7-10.7v-32V74.7c0-5.9-4.8-10.7-10.7-10.7H10.7C4.8,64,0,68.8,0,74.7v170.7 C0,251.2,4.8,256,10.7,256z M21.3,85.3h213.3v102.3l-56.5-56.5c-4.2-4.2-10.9-4.2-15.1,0L129.3,165c-4.2,4.2-4.2,10.9,0,15.1 l10.7,10.7c2,2,3.1,4.7,3.1,7.5s-1.1,5.5-3.1,7.5c-4,4-11,4-15.1,0l-10.7-10.7l-32-32c-4.2-4.2-10.9-4.2-15.1,0l-45.8,45.8 L21.3,85.3L21.3,85.3z" />
                  <circle cx="106.7" cy="128" r="21.3" />
                </svg>
              </div>
              <div className="explain_pop">{this.explain(21)}</div>
            </button>
          </div>
          <div
            className={
              "buttons_block" + (this.state.html_mode ? " inactive" : "")
            }
          >
            <button
              onClick={this.headingOne.bind(this)}
              className={
                "button headingOne" + (this.state.heading_one ? " active" : "")
              }
            >
              <Icon icon={u1F1ED} />
              <div className="explain_pop">{this.explain(22)}</div>
            </button>
            <button
              onClick={this.headingTwo.bind(this)}
              className={
                "button headingTwo" + (this.state.heading_two ? " active" : "")
              }
            >
              <Icon icon={u1F1ED} />
              <div className="explain_pop">{this.explain(23)}</div>
            </button>
            <button
              onClick={this.headingThree.bind(this)}
              className={
                "button headingThree" +
                (this.state.heading_three ? " active" : "")
              }
            >
              <Icon icon={u1F1ED} />
              <div className="explain_pop">{this.explain(24)}</div>
            </button>
            <button
              onClick={this.headingFour.bind(this)}
              className={
                "button headingFour" +
                (this.state.heading_four ? " active" : "")
              }
            >
              <Icon icon={u1F1ED} />
              <div className="explain_pop">{this.explain(25)}</div>
            </button>
            <button
              onClick={this.big.bind(this)}
              className={"button big" + (this.state.big ? " active" : "")}
            >
              <Icon icon={ic_format_size} />
              <div className="explain_pop">{this.explain(26)}</div>
            </button>
            <button
              onClick={this.medium.bind(this)}
              className={"button medium" + (this.state.medium ? " active" : "")}
            >
              <Icon icon={ic_format_size} />
              <div className="explain_pop">{this.explain(27)}</div>
            </button>
            <button
              onClick={this.default.bind(this)}
              className={
                "button default" + (this.state.default ? " active" : "")
              }
            >
              <Icon icon={ic_format_size} />
              <div className="explain_pop">{this.explain(28)}</div>
            </button>
            <button
              onClick={this.small.bind(this)}
              className={"button small" + (this.state.small ? " active" : "")}
            >
              <Icon icon={ic_format_size} />
              <div className="explain_pop">{this.explain(29)}</div>
            </button>
          </div>
          <div
            className={
              "buttons_block" + (this.state.html_mode ? " inactive" : "")
            }
          >
            <button
              onClick={this.onInsertTable.bind(this)}
              className="button onInsertTable"
            >
              <Icon icon={table2} />
              <div className="explain_pop">{this.explain(30)}</div>
            </button>
            <button
              onClick={this.onBorderClear.bind(this)}
              className={
                "button onBorderClear" +
                (this.state.border_button ? " active" : "") +
                (this.state.table ? "" : " inactive")
              }
            >
              <Icon icon={ic_border_clear} />
              <div className="explain_pop">{this.explain(31)}</div>
            </button>
            <button
              onClick={this.onInsertColumn.bind(this)}
              className={
                "button onInsertColumn" + (this.state.table ? "" : " inactive")
              }
            >
              <Icon icon={up} />
              <div className="explain_pop">{this.explain(32)}</div>
            </button>
            <button
              onClick={this.onRemoveColumn.bind(this)}
              className={
                "button onRemoveColumn" + (this.state.table ? "" : " inactive")
              }
            >
              <Icon icon={down} />
              <div className="explain_pop">{this.explain(33)}</div>
            </button>
            <button
              onClick={this.onInsertRow.bind(this)}
              className={
                "button onInsertRow" + (this.state.table ? "" : " inactive")
              }
            >
              <Icon icon={down} />
              <div className="explain_pop">{this.explain(34)}</div>
            </button>
            <button
              onClick={this.onRemoveRow.bind(this)}
              className={
                "button onRemoveRow" + (this.state.table ? "" : " inactive")
              }
            >
              <Icon icon={up} />
              <div className="explain_pop">{this.explain(35)}</div>
            </button>
            <button
              onClick={this.onRemoveTable.bind(this)}
              className={
                "button onRemoveTable" + (this.state.table ? "" : " inactive")
              }
            >
              <Icon icon={timesRectangleO} />
              <div className="explain_pop">{this.explain(36)}</div>
            </button>
          </div>
          <div
            className={
              "buttons_block" + (this.state.html_mode ? " inactive" : "")
            }
          >
            <button
              onClick={this.removeStyle.bind(this)}
              className="button removeStyle"
            >
              <Icon icon={clearFormatting} />
              <div className="explain_pop">{this.explain(37)}</div>
            </button>
            <button
              onClick={this.trimRows.bind(this)}
              className="button trimRows"
            >
              <Icon icon={copywriting} />
              <div className="explain_pop">{this.explain(39)}</div>
            </button>
            <button onClick={this.quotes.bind(this)} className="button quotes">
              <Icon icon={quoteRight} />
              <div className="explain_pop">{this.explain(40)}</div>
            </button>
          </div>
          <div className="buttons_block">
            <button onClick={this.html.bind(this)} className="button html">
              <Icon icon={ic_code} />
              <div className="explain_pop">{this.explain(38)}</div>
            </button>
          </div>
          <div className="buttons_block">
            <button onClick={this.iframe.bind(this)} className="button iframe">
              <Icon icon={browser} />
              <div className="explain_pop">{this.explain(42)}</div>
            </button>
            <div className={"link_pop" + (this.state.frame_pop ? " open" : "")}>
              <input
                type="text"
                value={this.state.frame_text}
                onChange={(event) => {
                  this.setState({ frame_text: event.target.value });
                }}
              />
              <div className="buttons_block right">
                <button
                  onClick={() => {
                    this.handleFrame(false);
                  }}
                  className="button agree"
                >
                  <Icon icon={cross} />
                </button>
                <button
                  onClick={(event) => {
                    this.handleFrame(true, event);
                  }}
                  className="button disagree"
                >
                  <Icon icon={checkmark} />
                </button>
              </div>
            </div>
          </div>
        </div>
        <Editor
          spellCheck={true}
          value={this.state.value}
          onChange={this.onChange.bind(this)}
          onPaste={this.onPaste.bind(this)}
          renderNode={this.renderNode.bind(this)}
          schema={schema}
          renderMark={this.renderMark.bind(this)}
          onDrop={this.onDrop.bind(this)}
          ref="editor"
          plugins={plugins}
          tabIndex={0}
        />
        {this.state.quillExist && (
          <div className="hider">
            <ReactQuill value={this.state.text} />
          </div>
        )}
        <div id="html_holder"></div>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("app"));
