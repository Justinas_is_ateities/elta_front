
var gulp 			= require( 'gulp' );
var sass 			= require( 'gulp-sass' );
var uglify 			= require( 'gulp-uglify' );
var sourcemaps 		= require( 'gulp-sourcemaps' );
var autoprefixer 	= require( 'gulp-autoprefixer' );
var iconfont 		= require( 'gulp-iconfont' );
var spritesmith 	= require( 'gulp-spritesmith' );
var livereload 		= require( 'gulp-livereload' );
var consolidate 	= require( 'gulp-consolidate' );
var imageop 		= require( 'gulp-image-optimization' );
var lodash  		= require( 'lodash' );
var watch 			= require( 'gulp-watch' );
var concat 			= require( 'gulp-concat' );

var appBase =  './';

var config = {
	scssPath: appBase + 'styles/global/',
	cssPath: appBase + 'styles/global/',
	jsPath: appBase + 'js/',
	distributionPath: appBase + 'assets/dist/',
	imagesPath: appBase + 'media/images/',
	spritePath: appBase + 'media/images/sprites/',
	fontsPath: './assets/fonts/',
	uglify: {
		filename: 'all.min.js', // failo pavadinimas i kuri bus sujungti visi failai
		manifest: 'manifest.json' // turi guleti jsPath kataloge
	}
};

gulp.task('images', function(cb) {
	gulp.src([
		config.imagesPath + '**/*.png',
		config.imagesPath + '**/*.jpg',
		config.imagesPath + '**/*.gif',
		config.imagesPath + '**/*.jpeg'
	])
	.pipe(imageop({
		optimizationLevel: 5,
		progressive: true,
		interlaced: true
	}))
	.pipe(gulp.dest( config.imagesPath ))
	.on('end', cb)
	.on('error', cb);
});

gulp.task('compress', function() {
	
	var fs = require( 'fs' );
	
	var manifestFile = config.jsPath + config.uglify.manifest;
	
	if ( !fs.existsSync(manifestFile) ) {
		
		console.log('\x1b[31m', 'No manifest.json file found in ' + config.jsPath ,'\x1b[0m');
		return false;
	}
	
	var manifest = JSON.parse(fs.readFileSync(manifestFile, 'utf8'));
	
	gulp.src( manifest )
		.pipe(sourcemaps.init())
		.pipe(concat( config.uglify.filename ))
		.pipe(uglify({
			mangle: false
		}))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest( config.distributionPath ));
	
	console.log('');
	console.log('\x1b[32m', 'All js files were successfully minified, manifest: ' ,'\x1b[0m');
	console.log('\x1b[32m', manifest ,'\x1b[0m');
	console.log('');
});

gulp.task('sass', function () {
	gulp.src( config.scssPath + '**/*.scss' )
		.pipe(sourcemaps.init())
		.pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
		.pipe(autoprefixer('last 3 versions'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest( config.cssPath ))
		.pipe(livereload());
});

gulp.task('default', function () {
	
	watch(config.scssPath + '**/*.scss', function() {
		
		gulp.start( 'sass' );
	});
	
	// gulp.src( appBase + '*.html', { read: false })
	// 	.pipe(watch( appBase + '*.html' ))
	// 	.pipe(livereload());
	
	livereload.listen();
});

gulp.task('iconfont', function(){
	return gulp.src([ config.fontsPath + '/svg/**/*.svg'])
		.pipe(
			iconfont({
				fontName: 'icons',
				appendUnicode: false,
				normalize: true
			})
		)
		.on('glyphs', function(glyphs, options) {
			
			var unicodeGlyphs = [];
			
			for (var i = 0; i < glyphs.length; i++) {
				unicodeGlyphs.push({
					name: glyphs[i].name,
					unicode: glyphs[i].unicode[0].charCodeAt(0).toString(16).toUpperCase()
				});
			}
			
			gulp.src( config.scssPath + '/templates/_icons.scss' )
			.pipe(consolidate('lodash', {
				glyphs: unicodeGlyphs,
				fontName: 'icons',
				fontPath: '../assets/fonts/',
				className: 'icon'
			}))
			.pipe(gulp.dest( config.scssPath ));
		})
		.pipe(gulp.dest( config.fontsPath ));
});
